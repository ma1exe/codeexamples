<?php 
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
    use app\models\CatalogCategories;
    use app\models\CatalogManufacturers;
    use app\models\CatalogCurrencies;
?>

    <?php $form = ActiveForm::begin([
        'id' => 'catalog-category',
    ]); ?>

    <?= $form->field($model, 'name'); ?>

    <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>