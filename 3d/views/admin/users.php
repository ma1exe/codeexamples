<?

    $sql = "SELECT * from " . DB_TABLE_USERS;

    if ($result = $mysqli->query($sql)): 
?>

<table class="table">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Имя</th>
            <!--<th scope="col">Фамилия</th>-->
            <th scope="col">E-Mail</th>
            <th scope="col">Роль</th>
            <th scope="col">Удаление</th>
        </tr>
    </thead>
    <tbody>
        <? while ($user_db = $result->fetch_assoc()): ?>
            <tr>
                <th scope="row"><?= $user_db['id']; ?></th>
                <td><?= $user_db['first_name']; ?></td>
                <!--<td><?= $user_db['last_name']; ?></td>-->
                <td><?= $user_db['email']; ?></td>
                <td><?= ($user_db['is_admin']) ? 'Администратор' : 'Пользователь'; ?></td>
                <td>
                    <? if ($user['id'] == $user_db['id']): ?>
                        текущий
                    <? else: ?>
                        <a href="/admin/user_delete<?= SITE_URLS_SUFFIX; ?>?id=<?= $user_db['id']; ?>">удалить</a>
                    <? endif; ?>
                </td>
            </tr>
    <? endwhile; ?>
        </tbody>
    </table>
<? endif; ?>