<?

    $sql = "SELECT * from " . DB_TABLE_CATEGORIES . " ORDER BY `title`";

    if ($result = $mysqli->query($sql)): 
?>

<div class="row">
    <div class="col-12">
        <p class="text-right"><a href="/admin/category_new<?= SITE_URLS_SUFFIX; ?>">Добавить</a></p>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th scope="col">Название</th>
            <th scope="col">Редактирование</th>
            <th scope="col">Удаление</th>
        </tr>
    </thead>
    <tbody>
        <? while ($category = $result->fetch_assoc()): ?>
            <tr>
                <th scope="row"><?= $category['title']; ?></th>
                <td><a href="/admin/category_edit<?= SITE_URLS_SUFFIX; ?>?id=<?= $category['id']; ?>">редактировать</a></td>
                <td><a href="/admin/category_delete<?= SITE_URLS_SUFFIX; ?>?id=<?= $category['id']; ?>">удалить</a></td>
            </tr>
    <? endwhile; ?>
        </tbody>
    </table>
<? endif; ?>