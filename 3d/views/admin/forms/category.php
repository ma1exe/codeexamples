<form action="" method="POST">
    <div class="form-group">
        <label for="title">Название категории</label>
        <input name="title" type="text" class="form-control" id="title" placeholder="" 
            value="<?= ($result_category) ? $result_category['title'] : ''; ?>" 
        required>
    </div>
    <div class="form-group">
        <label for="description">Описание</label>
        <textarea name="description" class="form-control tinymce" id="description" rows="3"><?= ($result_category) ? $result_category['description'] : ''; ?></textarea>
    </div>
    <div class="form-group">
        <label for="alias">Алиас (URL; разрешены английские буквы, цифры, символы ".", "-" и "_")</label>
        <input name="alias" type="text" class="form-control" id="alias" placeholder="" 
        value="<?= ($result_category) ? $result_category['alias'] : ''; ?>"
        required>
    </div>
    <div class="form-group">
        <label for="meta_title">Название страницы</label>
        <input name="meta_title" type="text" class="form-control" id="meta_title" placeholder="" 
        value="<?= ($result_category) ? $result_category['meta_title'] : ''; ?>"
        >
    </div>
    <div class="form-group">
        <label for="meta_description">Описание страницы</label>
        <input name="meta_description" type="text" class="form-control" id="meta_description" placeholder="" 
        value="<?= ($result_category) ? $result_category['meta_description'] : ''; ?>"
        >
    </div>
    <div class="form-group">
        <label for="meta_keywords">Ключевые слова страницы</label>
        <input name="meta_keywords" type="text" class="form-control" id="meta_keywords" placeholder="" 
        value="<?= ($result_category) ? $result_category['meta_keywords'] : ''; ?>"
        >
    </div>
    <button name="submit" type="submit" class="btn btn-primary"><?= ($result_category) ? 'Обновить': 'Добавить'; ?></button>
</form>
