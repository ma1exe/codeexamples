<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use app\models\CatalogProducts;
use app\models\CatalogCategories;
use app\models\CatalogCurrencies;
use app\models\CatalogManufacturers;
use yii\data\Pagination;

class CatalogController extends GlobalController
{
    public function actionIndex($category_link = false) 
    {
        if ($category_link) {
            $current_category = CatalogCategories::find()
            ->where(['link' => $category_link])
            ->one();

            if (!$current_category) throw new \yii\web\NotFoundHttpException();

            $products_query = CatalogProducts::find()->where(['category_id' => $current_category['id']]);
            $pages = new Pagination(['totalCount' => $products_query->count()]);
            $products = $products_query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

            $popular_products = CatalogProducts::find()->where(['category_id' => $current_category['id']])->orderBy(['viewed' => SORT_DESC, 'price' => SORT_DESC])->limit(4)->all();
        } else {
            $current_category = false;

            $products_query = CatalogProducts::find();
            $pages = new Pagination(['totalCount' => $products_query->count()]);
            $products = $products_query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

            $popular_products = CatalogProducts::find()->orderBy(['viewed' => SORT_DESC, 'price' => SORT_DESC])->limit(4)->all();
        }

        /*$categories = CatalogCategories::find()->all();*/
        $manufacturers = CatalogManufacturers::find()->all();

        //$this->registerCssFile("/path/to/your/file/in/web/folder/style.css");
        return $this->render('index', compact('products', 'current_category', 'manufacturers', 'popular_products', 'pages'));
    }

    public function actionCategory($category_link = null) 
    {
        $category = CatalogCategories::find()
            ->where(['link' => $category_link])
            ->one();

        if (!$category) throw new \yii\web\NotFoundHttpException();

        $query = CatalogProducts::find()->where(['category_id' => $category['id']]);
        $pages = new Pagination(['totalCount' => $query->count()]);
        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $currencies_query = CatalogCurrencies::find()->asArray()->all();
        $currencies = [];
        foreach ($currencies_query as $currency) {
            $currencies[$currency['id']] = [
                'symbol' => $currency['symbol'],
                'rate' => $currency['rate']
            ];
        }

        return $this->render('category', compact('category', 'products', 'currencies', 'pages'));
    }

    public function actionProduct($category_link = null, $product_id = null) 
    {
        $category = CatalogCategories::find()
            ->where(['link' => $category_link])
            ->one();

        if (!$category) throw new \yii\web\NotFoundHttpException();

        $product = CatalogProducts::find()
            ->where(['id' => $product_id, 'category_id' => $category['id']])
            ->one();

        if (!$product) throw new \yii\web\NotFoundHttpException();

        $product->viewed++;
        $product->save();

        return $this->render('product', compact('product', 'category'));
    }
}