<?php

    use yii\helpers\Url;
    use yii\widgets\LinkPager;

?>

<div class="row">
    <div class="col-xs-12">
        <h2>Товары</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Название</th>
                    <th>Артикул</th>
                    <th>Категория</th>
                    <th>Производитель</th>
                    <th>Цена</th>
                    <th>Валюта</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <?php foreach ($products as $product): ?>
                <tr>
                    <td><?= $product['name']; ?></td>
                    <td><?= $product['sku']; ?></td>
                    <td><?= $product->category->name;; ?></td>
                    <td><?= $product->manufacturer->name; ?></td>
                    <td><?= $product['price']; ?></td>
                    <td><?= $product->currency->symbol; ?></td>
                    <td><a href="<?= Url::toRoute('admin/products/update/'.$product['id']); ?>">Редактировать</a></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?= LinkPager::widget([
    'pagination' => $pages,
]); ?>