$('.btn-add-to-cart-js').on('click', function(e) {
    var id = $(this).data('id');
    /*var quantity = 1;
    if ($(this).hasClass('btn-add-to-cart-category')) {
        var quantity = $(this).parent().find('#product-quantity-' + id).val();
    } else if ($(this).hasClass('btn-add-to-cart-product')) {
        var quantity = $(this).parent().parent().find('#product-quantity-' + id).val();
    }*/

    var data = {
        action: 'add',
        id: id
    };

    $.ajax({
        url: '/ajax/cart.php',
        type: 'POST',
        data: data,
        success: function(res) {
            var success = JSON.parse(res)['success']; 
            console.log(JSON.parse(res));
            if (success) {
                location.reload();
            } else {
                alert('При добавлении товара возникла ошибка. Обновите страницу или попробуйте позже.');
            }
        },
        error: function() {
            alert('При добавлении товара возникла ошибка. Обновите страницу или попробуйте позже.');
        }
    });

    e.preventDefault(); // или return false;
});

$('.btn-remove-from-cart-js').on('click', function(e) {
    var id = $(this).data('id');

    var data = {
        action: 'remove',
        id: id
    };

    $.ajax({
        url: '/ajax/cart.php',
        type: 'POST',
        data: data,
        success: function(res) {
            var success = JSON.parse(res)['success']; 
            console.log(JSON.parse(res));
            if (success) {
                location.reload();
            } else {
                alert('При удалении товара произошла ошибка. Обновите страницу или попробуйте позже.');
            }
        },
        error: function() {
            alert('При добавлении товара произошла ошибка. Обновите страницу или попробуйте позже.');
        }
    });
    e.preventDefault(); // или return false;
});

// ColorBox Responsive

$.colorbox.settings.maxWidth  = '95%';
$.colorbox.settings.maxHeight = '95%';

// ColorBox resize function
var resizeTimer;
function resizeColorBox()
{
    if (resizeTimer) clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
            if ($('#cboxOverlay').is(':visible')) {
                    $.colorbox.load(true);
            }
    }, 300);
}

// Resize ColorBox when resizing window or changing mobile device orientation
$(window).resize(resizeColorBox);

// ColorBox Responsive

$(document).ready(function() {
    $(".lightbox").colorbox({rel:"lightbox", title: false});
    $(".iframe").colorbox({iframe:true, width:"70%", height:"80%"});

    $('.form-anti-bot').hide(); // hide inputs from users

    $('#form_fegister').on('submit', function(e) {
        e.preventDefault();

        $.ajax({
            url: '/ajax/register.php',
            type: 'POST',
            data: $(this).serialize(),
            success: function(res) {
                var success = JSON.parse(res)['success']; 
                if (!success) {
                    var errors = JSON.parse(res)['errors']; 
                    if (errors) {
                        console.log(errors);
                        alert(errors.join('\n'));
                    } else {
                        alert('К сожалению, при регистрации произошла ошибка. Повторите попытку позже.');
                    }
                } else {
                    window.location.href = '/account.php';
                }
            },
            error: function() {
                alert('К сожалению, при регистрации произошла ошибка. Обновите страницу или попробуйте позже.');
            }
        });
    });

    $('#form_login').on('submit', function(e) {
        e.preventDefault();

        $.ajax({
            url: '/ajax/login.php',
            type: 'POST',
            data: $(this).serialize(),
            success: function(res) {
                var success = JSON.parse(res)['success']; 
                if (!success) {
                    var errors = JSON.parse(res)['errors']; 
                    if (errors) {
                        console.log(errors);
                        alert(errors.join('\n'));
                    } else {
                        alert('К сожалению, процесс авторизации не удался. Повторите попытку позже.');
                    }
                } else {
                    window.location.href = '/account.php';
                }
            },
            error: function() {
                alert('К сожалению, процесс авторизации не удался. Обновите страницу или попробуйте позже.');
            }
        });
    });

    $('#form_checkout').on('submit', function(e) {
        e.preventDefault();

        $.ajax({
            url: '/ajax/order_confirm.php',
            type: 'POST',
            data: $(this).serialize(),
            success: function(res) {
                var success = JSON.parse(res)['success']; 
                if (!success) {
                    var errors = JSON.parse(res)['errors']; 
                    if (errors) {
                        console.log(errors);
                        alert(errors.join('\n'));
                    } else {
                        alert('К сожалению, оформить заказ не получилось. Повторите попытку позже.');
                    }
                } else {
                    window.location.href = '/sposoby_oplaty.php';
                }
            },
            error: function() {
                alert('К сожалению, оформить заказ не получилось. Обновите страницу или попробуйте позже.');
            }
        });
    });

    $('.form_account_edit').on('submit', function(e) {
        e.preventDefault();

        var data = {};
        data.type = $(this).data('type');

        if (data.type == 'personal') {
            data.first_name = $(this).find('#first_name').val();
            data.last_name = $(this).find('#last_name').val();
            data.password = $(this).find('#password').val();
        } else if (data.type == 'password') {
            data.password = $(this).find('#password').val();
            data.new_password = $(this).find('#new_password').val();
            data.confirmation_new_password = $(this).find('#confirmation_new_password').val();
        }

        $.ajax({
            url: '/ajax/account_edit.php',
            type: 'POST',
            data: data,
            success: function(res) {
                if (!JSON.parse(res)['success']) {
                    var errors = JSON.parse(res)['errors']; 
                    if (errors) {
                        console.log(errors);
                        alert(errors.join('\n'));
                    } else {
                        alert('К сожалению, данные обновить не удалось. Повторите попытку позже.');
                    }
                } else {
                    window.location.href = JSON.parse(res)['success'];
                }
            },
            error: function() {
                alert('К сожалению, данные обновить не удалось. Обновите страницу или попробуйте позже.');
            }
        });
    });

    $('#closeMenuMobile').on('click', function() {
        $('.toggle-menu').trigger('click');
    });

});