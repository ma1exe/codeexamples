<?

	$result = $mysqli->query('
		SELECT
			p.id,
			p.title,
			p.description,
			p.image,
			p.price,
			a.alias,
			p.alias_id
		FROM
			' . DB_TABLE_PRODUCTS . ' p
		LEFT OUTER JOIN ' . DB_TABLE_ALIASES . ' a ON
			a.id = p.alias_id
		ORDER BY id DESC
		LIMIT 4'
	);

	$new_products = [];

	while ($new_product = $result->fetch_assoc()) {
		$new_products[] = $new_product;
	}

	$sql = 'SELECT * FROM ' . DB_TABLE_PAGES_EXTRA . ' WHERE id = 1';
	$result_page = $mysqli->query($sql);
	$page_extra = $result_page->fetch_assoc();

?>
<?= htmlspecialchars_decode($page_extra['blocks_1']); ?>
<? if ($new_products): ?>
	<h3>Новинки</h3>
	<div class="page">
		<div class="pageItem">
			<div class="row-fluid shop-products">
				<ul class="thumbnails">
					<? $is_first = true; ?>

					<? foreach ($new_products as $product): ?>

						<?
							$button_cart_title = (isset($cart['items'][$product['id']])) ? 'Убрать' : 'Купить';
							$button_cart_class = (isset($cart['items'][$product['id']])) ? 'btn-remove-from-cart-js' : 'btn-add-to-cart-js';
						?>

						<li class="item span3<?= ($is_first) ? ' first' : ''; ?>">
							<div class="thumbnail text-center">
								<a href="/<?= $product['alias']; ?>" class="image">
									<img src="<?= checkImageExists($product['image']); ?>" alt="<?= $product['title']; ?>"/>
									<span class="frame-overlay"></span><span class="price"><?= generatePriceString($product['price'], $current_rate, $current_symbol); ?></span>
									<span class="label new">Новинка</span>
								</a>
								<div class="inner notop nobottom text-center">
									<h4 class="title"><a href="/<?= $product['alias']; ?>"><?= $product['title']; ?></a></h4>
								</div>
							</div>
							<div class="inner darken notop text-center">
								<a class="btn btn-add-to-cart <?= $button_cart_class; ?>" href="javascript:void(0);" data-id="<?= $product['id']; ?>">
									<i class="fa fa-shopping-cart"></i> <?= $button_cart_title; ?>
								</a>
							</div>
						</li>

						<? $is_first = false; ?>

					<? endforeach; ?>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
<? endif; ?>

<?= htmlspecialchars_decode($page_extra['blocks_2']); ?>