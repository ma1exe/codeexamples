<?
    define('SITE_DEBUG_ENABLED', false);

    define('SITE_URL', 'http://ma1exe.beget.tech/');
    define('SITE_EMAIL_URL', '3d-model-stl.com'); // Адрес сайта, который указывается в почтовых сообщениях

    define('SITE_EMAIL', 'ma1exe@yandex.com');

    define('SITE_INFO_EMAIL', 'info@3d-model-stl.com'); // Почта, адрес которой указывается в качестве отправителя, а также на которую идут информационные сообщения

    define('SITE_URLS_SUFFIX', '.php');

    define('MIN_LENGTH_USER_PASSWORD', 6);

    define('MAX_LENGTH_USER_EMAIL', 255);
    define('MAX_LENGTH_USER_PASSWORD', 32);
    define('MAX_LENGTH_USER_FIRST_NAME', 255);
    define('MAX_LENGTH_USER_LAST_NAME', 255);

    define('MAX_ELEMENTS_PER_PAGE_PRODUCTS', 21);