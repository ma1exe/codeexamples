<h1>Сброс пароля</h1>

<?

    if (getFlash() && !$is_reload_required) {
        echo getFlash();
        clearFlash();
    }

?>

<? 
    if (isset($_POST['submit_step_1'])) {
        $result = $mysqli->query("SELECT * FROM " . DB_TABLE_USERS . " WHERE `email` = '" . htmlspecialchars($_POST['email']) . "'");
        if (!$user_db = $result->fetch_assoc()) {
            addFlash('<p>Пользователь с таким e-mail не найден.</p>');
        } else {
            $user_hash = generateHashString();
            $sql = "UPDATE " . DB_TABLE_USERS . " SET `hash_password_reset` = '" . $user_hash . "' WHERE id = " . $user_db['id'];
            if (!$mysqli->query($sql)) {
                addFlash('<p>При сбросе пароля произошла ошибка. Попробуйте позже.</p>');
            } else {
                // To send HTML mail, the Content-type header must be set
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                
                // Create email headers
                $headers .= 'From: ' . SITE_ADMIN_EMAIL . "\r\n".
                    'Reply-To: ' . $_POST['email'] . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();

                $message = "<html><body><p>Добрый час. На ваш адрес была запрошена ссылка на сброс пароля. Если вы не запрашивали ее, пожалуйста, проигнорируйте данное письмо.</p>";
                $reset_password_link = 'reset_password' . SITE_URLS_SUFFIX . '?step=2&email=' . $_POST['email'] . '&token=' . $user_hash;
                $message .= '<p>Ссылка: ' . SITE_URL . $reset_password_link . '</p></body></html>';
                if (mail($_POST['email'], 'Сброс пароля', $message, $headers)) {
                    addFlash('<p>Письмо со ссылкой на сброс пароля отправлено на указанный адрес.');
                    $is_reload_required = true;
                } else {
                    addFlash('<p>При отправке письма со ссылкой произошла ошибка. Попробуйте позже.');
                }
            }
        }
    }

?>

<? if (isset($_GET['step']) && $_GET['step'] == 2 && isset($_GET['email']) && isset($_GET['token']) && !empty($_GET['email']) && !empty($_GET['token'])): ?>
    <?
        $token = htmlspecialchars($_GET['token']);
        $email = htmlspecialchars($_GET['email']);
        $sql = "SELECT * FROM " . DB_TABLE_USERS . " WHERE `email` = '" . $email . "' AND `hash_password_reset` = '" . $token . "'";

        $result_token = $mysqli->query($sql);
        if ($user_db = $result_token->fetch_assoc()):
            if (isset($_POST['new_password']) && isset($_POST['confirmation_new_password'])):

                if ($_POST['new_password'] !== $_POST['confirmation_new_password']):
                    
                    addFlash('<p>Пароли не совпадают.</p>');

                else:
                    
                    if (strlen($_POST['new_password']) >= MIN_LENGTH_USER_PASSWORD && strlen($_POST['new_password']) <= MAX_LENGTH_USER_PASSWORD):

                        $new_password = password_hash($_POST['new_password'], PASSWORD_DEFAULT);
                        $new_hash = generateHashString();

                        $sql = "UPDATE " . DB_TABLE_USERS . " SET `password` = '" . $new_password . "', `hash` = '" . $new_hash . "', `hash_password_reset` = '" . generateHashString() . "'";

                        echo $sql;

                        if ($mysqli->query($sql)):
                            $login_link = '/login' . SITE_URLS_SUFFIX;
                            addFlash('<script>window.location.href = "' . $login_link . '";</script>');
                            addFlash('<p>Пароль успешно обновлен! Вы будете перенаправлены на страницу входа. Если этого не произошло, <a href="' . $login_link . '">нажмите сюда</a>.</p>');
                            $is_reload_required = true;
                        else:
                            addFlash('<p>При изменении пароля произошла ошибка. Проверьте введенные данные, либо повторите попыптку позднее.</p>');
                        endif;

                    elseif (strlen($_POST['new_password']) < MIN_LENGTH_USER_PASSWORD):

                        addFlash('<p>Длина пароля не может быть меньше ' . MIN_LENGTH_USER_PASSWORD . ' символов.</p>');

                    elseif (strlen($_POST['new_password']) < MIN_LENGTH_USER_PASSWORD):

                        addFlash('<p>Длина пароля не может быть больше ' . MAX_LENGTH_USER_PASSWORD . ' символов.</p>');

                    else:

                        addFlash('<p>При изменении пароля произошла ошибка. Проверьте введенные данные, либо повторите попыптку позднее.</p>');

                    endif;

                endif;

            endif;
    ?>

            <form action="<?= $_SERVER['REQUEST_URI']; ?>" method="POST">
                <div class="page">
                    <div class="pagecontent">
                        <span class="bold">Шаг 2:</span>
                        <br> Пожалуйста, введите ваш новый пароль и подтвердите его. <br><br>
                        <fieldset class="form">
                            <legend>Измените пароль в два шага.</legend>
                            <p><span class="bold">Новый пароль:</span> <input type="password" name="new_password" minlength="<?= MIN_LENGTH_USER_PASSWORD; ?>" maxlength="<?= MAX_LENGTH_USER_PASSWORD; ?>" required></p>
                            <p><span class="bold">Подтвердите пароль:</span> <input type="password" name="confirmation_new_password" minlength="<?= MIN_LENGTH_USER_PASSWORD; ?>" maxlength="<?= MAX_LENGTH_USER_PASSWORD; ?>" required></p>
                        </fieldset>
                    </div>
                </div>
                <div class="pagecontentfooter">
                    <span class="button">
                        <button type="submit" name="submit_step_2">
                            <img src="/web/images/icons/submit.png" alt="Продолжить" title=" Продолжить " width="12" height="12">&nbsp;Продолжить
                        </button>
                    </span>
                </div>
            </form>
        <? else: ?>
            <p>Неверный токен.</p>
        <? endif; ?>
<? else :?>

    <form action="/reset_password<?= SITE_URLS_SUFFIX; ?>" method="POST">
        <div class="page">
            <div class="pagecontent">
                <span class="bold">Шаг 1:</span>
                <br> Пожалуйста, введите E-Mail адрес, который Вы указывали при регистрации ! 
                <br> Нажмите кнопку продолжить, и на Ваш E-Mail будет отправлена ссылка для сброса пароля. <br><br>
                <fieldset class="form">
                    <legend>Измените пароль в два шага.</legend>
                    <p><span class="bold">E-Mail адрес:</span> <input type="email" name="email" required></p>
                </fieldset>
            </div>
        </div>
        <div class="pagecontentfooter">
            <span class="button">
                <button type="submit" name="submit_step_1">
                    <img src="/web/images/icons/submit.png" alt="Продолжить" title=" Продолжить " width="12" height="12">&nbsp;Продолжить
                </button>
            </span>
        </div>
    </form>
<? endif; ?>