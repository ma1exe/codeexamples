<?php

    use yii\helpers\Url;
    use yii\widgets\LinkPager;

?>

<div class="row">
    <div class="col-xs-12">
        <h2>Производители</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Название</th>
                </tr>
            </thead>
            <?php foreach ($manufacturers as $manufacturer): ?>
                <tr>
                    <td><?= $manufacturer['name']; ?></td>
                    <td><a href="<?= Url::toRoute('admin/manufacturers/update/'.$manufacturer['id']); ?>">Редактировать</a></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?= LinkPager::widget([
    'pagination' => $pages,
]); ?>