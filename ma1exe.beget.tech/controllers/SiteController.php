<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\CatalogProducts;
use app\models\CatalogCategories;
use app\models\CatalogCurrencies;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\db\Expression;

class SiteController extends GlobalController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $popular_products = CatalogProducts::find()->orderBy(['viewed' => SORT_DESC, 'name' => SORT_DESC])->limit(4)->all();

        $filtered_products = [];
        foreach ($popular_products as $product) {
            $filtered_products[] = $product['id'];
        }

        $new_products = CatalogProducts::find()->orderBy(['id' => SORT_DESC])->where(['not in', 'id', $filtered_products])->limit(4)->all();

        foreach ($new_products as $product) {
            $filtered_products[] = $product['id'];
        }

        $random_products = CatalogProducts::find()->orderBy(new Expression('rand()'))->where(['not in', 'id', $filtered_products])->limit(4)->all();

        return $this->render('index', compact('new_products', 'popular_products', 'random_products'));
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContacts()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contacts', [
            'model' => $model,
        ]);
    }

    /**
     * Displays delivery page.
     *
     * @return string
     */
    public function actionDelivery()
    {
        return $this->render('delivery');
    }
}
