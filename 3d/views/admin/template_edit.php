<?
    if (isset($_POST['submit'])) {
        $blocks_1 = htmlspecialchars($_POST['blocks_1']);
        $blocks_2 = htmlspecialchars($_POST['blocks_2']);

        $sql = 'UPDATE ' . DB_TABLE_PAGES_EXTRA . ' SET `blocks_1` = "' . $blocks_1 . '", `blocks_2` = "' . $blocks_2 . '" WHERE id = 2';
        if ($mysqli->query($sql)) {
            addFlash('<p>Шаблон обновлен!</p>');
            $is_reload_required = true;
        } else {
            addFlash('<p>При обновлении данных произошла ошибка.</p>');
            addFlash('<p>' . $mysqli->error . '</p>');
        }
    }

    $sql = 'SELECT * FROM ' . DB_TABLE_PAGES_EXTRA . ' WHERE id = 2';

    if (getFlash() && !$is_reload_required) {
        echo getFlash();
        clearFlash();
    }

    if ($result = $mysqli->query($sql)) {
        if ($page_extra = $result->fetch_assoc()) {
            $blocks_1_title = 'Левый блок';
            $blocks_2_title = 'Нижний блок';

            require('./views/admin/forms/pages_extra.php');
        } else {
            echo '<p>Не найдена запись в базе данных.</p>';
        }
    } else {
        echo '<p>Ошибка при работе с базой данных.</p>';
    }