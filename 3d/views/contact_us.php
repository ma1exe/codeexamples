<script>
	$(document).ready(function() {

		$('#contact_us :input:text:visible:enabled:first').focus();
	
	})
	
</script>
<h1>Свяжитесь с нами</h1>

<? 
    if (isset($_POST['submit']) && isset($_GET['action']) && $_GET['action'] == 'send') {
        if (empty($_POST['second_name']) && !empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['message'])) {

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            
            // Create email headers
            $headers .= 'From: ' . $_POST['email'] . "\r\n".
                'Reply-To: ' . SITE_INFO_EMAIL . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

            $message = "<html><body><p>E-Mail: " . htmlspecialchars($_POST['email']) . "</p><p>Имя: " . htmlspecialchars($_POST['name']) . "</p>";
            $message .= '<p>Сообщение: ' . htmlspecialchars($_POST['message']) .'</p></body></html>';

            if (mail(SITE_INFO_EMAIL, 'Сообщение с формы обратной связи ' . SITE_EMAIL_URL, $message, $headers)) {
                addFlash('<p>Ваше сообщение отправлено.</p>');
                $is_reload_required = true;
            } else {
                addFlash('<div class="contacterror">При отправке письма произошла ошибка. Повторите попытку позже.</div>');
            }
        } else {
            addFlash('<div class="contacterror">Пожалуйста, проверьте указанные в форме данные.</div>');
        }

    }
?>

<? 
    if (getFlash()) {
        echo getFlash();
        if (!$is_reload_required){
            clearFlash();
        }
    }
?>

<? if (!$is_reload_required): ?>

    <form name="contact_us" id="contact_us" action="/contact_us<?= SITE_URLS_SUFFIX; ?>?action=send" method="post">
        <div class="page">
            <div class="pagecontent">
                Форма обратной связи <br><br>
                <fieldset class="form">
                    <p><label for="name">Ваше имя:</label> <input id="name" type="text" name="name" required></p>
                    <p><label for="email">Ваш E-Mail:</label> <input id="email" type="email" name="email" required></p>
                    <p><label for="message">Ваше сообщение:</label> <textarea id="message" name="message" cols="50" rows="15" required></textarea></p>
                </fieldset>
            </div>
            <div class="form-anti-bot" style="/* display:none; */">
                <strong>Оставьте пустым</strong> 
                <span class="required">*</span>
                <input type="text" name="second_name" size="30" value="">
            </div>
            <div><span class="button"><button type="submit" name="submit"><img src="/web/images/icons/submit.png" alt="Продолжить" title=" Продолжить " width="12" height="12">&nbsp;Продолжить</button></span></div>
            <br>
            <div> Нажимая кнопку, я даю согласие на обработку своих персональных данных. <a href="/privacy<?= SITE_URLS_SUFFIX; ?>">Подробнее о защите персональной информации.</a></div>
        </div>
    </form>

<? endif; ?>