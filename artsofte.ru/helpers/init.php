<?
    define('AMOFIX_FOLDER', $_SERVER['DOCUMENT_ROOT'] . 'amofix/');

    define('DEFAULT_LOGS_FOLDER', AMOFIX_FOLDER . 'logs/'); // папка с логами общая
    define('AMOCRM_LOGS_FOLDER', DEFAULT_LOGS_FOLDER . 'amocrm/');  // папка с логами для амосрм
    define('PLANFIX_LOGS_FOLDER', DEFAULT_LOGS_FOLDER . 'planfix/');  // папка с логами для планфикса

    require(AMOFIX_FOLDER . 'helpers/logger.php'); // подключаем логгер
    require(AMOFIX_FOLDER . 'helpers/api_amocrm.php'); // подключаем api для работы с amocrm
    require(AMOFIX_FOLDER . 'helpers/api_planfix.php'); // подключаем api для работы с planfix