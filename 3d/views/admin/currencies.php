<?

    $sql = "SELECT * from " . DB_TABLE_CURRENCIES;

    if ($result = $mysqli->query($sql)): 
?>

<table class="table">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Код</th>
            <th scope="col">Символ</th>
            <th scope="col">Курс ЦБ РФ</th>
            <th scope="col">Обновлено</th>
        </tr>
    </thead>
    <tbody>
        <? while ($currency = $result->fetch_assoc()): ?>
            <tr>
                <th scope="row"><?= $currency['id']; ?></th>
                <td><?= $currency['code']; ?></td>
                <td><?= $currency['symbol']; ?></td>
                <td><?= $currency['rate']; ?> руб.</td>
                <td><?= $currency['updated']; ?></td>
            </tr>
    <? endwhile; ?>
        </tbody>
    </table>
<? endif; ?>