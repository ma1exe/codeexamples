<?
    require($_SERVER['DOCUMENT_ROOT'] . 'amofix/helpers/init.php');
    
    $log = new Logger();
    
    $log->lfile(PLANFIX_LOGS_FOLDER . @date('d_m_Y') . '_comment.txt');

    $log->lwrite("Поступил $_SERVER[REQUEST_METHOD]-запрос (тип контента $_SERVER[CONTENT_TYPE]) с IP $_SERVER[REMOTE_ADDR]");

    if (isset($_GET['test']) && $_GET['test'] == 'yes') {    }

    if ($_SERVER["CONTENT_TYPE"] === 'application/json') {
        $postData = file_get_contents('php://input');
        $data = json_decode($postData, true);
        if ($data !== NULL) {
            $log->lwrite(json_encode($data), true);

            $restricted_users = [
                4583362, // робот для амосрм 
                4583364 // робот уведомлений
            ];

            if (
                isset($data['from']) && $data['from'] === 'planfix' &&
                isset($data['user_id']) && intval($data['user_id']) > 0 && !in_array(intval($data['user_id']), $restricted_users) && 
                isset($data['amocrm_url']) && !empty($data['amocrm_url']) &&
                (
                    (isset($data['text']) && !empty($data['text'])) ||
                    (isset($data['files']) && !empty($data['files']))
                ) && 
                isset($data['task_id']) && intval($data['task_id']) > 0
            ) {

                $amocrm_lead_url = urldecode($data['amocrm_url']);
                $log->lwrite("URL для amoCRM: $amocrm_lead_url.");

                if (preg_match('/artsoftedigital\.amocrm\.ru\/leads\/detail\/(\d+)/', $amocrm_lead_url, $amocrm_lead_id)) {
                    $amocrm_lead_id = $amocrm_lead_id[1];
                    $log->lwrite("ID сделки в amoCRM = $amocrm_lead_id.");

                    $AmoCRM = new amoCRM();

                    $text = "Запись из Planfix (https://artsofte.planfix.ru/task/$data[task_id]/) от пользователя " . urldecode($data['user_name']);

                    if (isset($data['text']) && !empty($data['text'])) {
                        $text .= "\n\n" . urldecode($data['text']);
                    }

                    if (isset($data['files']) && !empty($data['files'])) {
                        $text .= "\n\nПрикрепленные файлы: \n";

                        $files = explode(',', urldecode($data['files']));
                        foreach ($files as $file) {
                            $text .= "\n- https://artsofte.planfix.ru/file/$file";
                        }
                    }

                    $amocrm_data = [[
                        "note_type" => "common",
                        "params" => [
                            "text" => $text
                        ]
                    ]];

                    $request = $AmoCRM->request("/api/v4/leads/$amocrm_lead_id/notes", false, $amocrm_data);
                } else {
                    $log->lwrite('Не найден ID сделки в amoCRM.');
                }

            } else {
                $log->lwrite('При проверке данных запроса произошла ошибка.');
            }

        } else {
            $log->lwrite('JSON пуст.');
        }
    }

    //echo 'loaded';

    $log->lclose();