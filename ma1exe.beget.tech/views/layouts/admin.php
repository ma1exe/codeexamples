<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;

AdminAsset::register($this);

$this->title = 'Панель администратора';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
            <!-- Logo -->
                <div class="logo">
                    <h1>Панель администратора</h1>
                </div>
            </div>
            <?php if (!Yii::$app->user->isGuest): ?>
                <div class="col-md-2">
                    <a href="<?= Url::toRoute('admin/logout'); ?>">Выйти</a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="row">
        <?php if (!Yii::$app->user->isGuest): ?>
            <div class="col-md-2">
                <div class="sidebar content-box" style="display: block;">
                    <ul class="nav">
                        <!-- Main menu -->
                        <li class="current"><a href="<?= Url::toRoute('admin/index'); ?>">
                            <i class="glyphicon glyphicon-home"></i> Главная
                        </a></li>
                        <li><a href="<?= Url::toRoute('admin/categories'); ?>">
                            <i class="glyphicon glyphicon-calendar"></i> Категории
                        </a></li>
                        <li><a href="<?= Url::toRoute('admin/products'); ?>">
                            <i class="glyphicon glyphicon-stats"></i> Товары
                        </a></li>
                        <li><a href="<?= Url::toRoute('admin/manufacturers'); ?>">
                            <i class="glyphicon glyphicon-record"></i> Производители
                        </a></li>
                        <li><a href="<?= Url::toRoute('admin/currencies'); ?>">
                            <i class="glyphicon glyphicon-list"></i> Валюты
                        </a></li>
                        <li><a href="<?= Url::toRoute('admin/orders'); ?>">
                            <i class="glyphicon glyphicon-record"></i> Заказы
                        </a></li>
                        <li><a href="<?= Url::toRoute('site/index'); ?>">
                            <i class="glyphicon glyphicon-pencil"></i> Вернуться на сайт
                        </a></li>
                    </ul>
                </div>
            </div>
        <?php endif; ?>
		<div class="col-md-10">
            <?= $content ?>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
