<?
    if (isset($_COOKIE['user_id']) && isset($_COOKIE['user_hash'])) {
        $user_id = intval($_COOKIE['user_id']);
        $user_hash = htmlspecialchars($_COOKIE['user_hash']);

        $sql = "SELECT * FROM " . DB_TABLE_USERS . " WHERE id = " . $user_id . " AND hash = '" . $user_hash . "'";
        $result = $mysqli->query($sql);

        if ($user_db = $result->fetch_assoc()) {
            setcookie('user_id', '', time() - 3600*24*30*12, '/');
            setcookie('user_hash', '', time() - 3600*24*30*12, '/', null, null, true); // httponly !!!
            header('Location: /login'  . SITE_URLS_SUFFIX);
        }
    }

    header('Location: /account' . SITE_URLS_SUFFIX);