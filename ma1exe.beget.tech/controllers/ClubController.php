<?php

namespace app\controllers;

use Yii;
use yii\web\NotFoundHttpException;

class ClubController extends GlobalController
{
    public $layout = 'club';

    public function actionIndex() 
    {
        //$this->registerCssFile("/path/to/your/file/in/web/folder/style.css");
        return $this->render('index');
    }
}