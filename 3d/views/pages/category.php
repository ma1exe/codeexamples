<?
    $result = $mysqli->query("SELECT * FROM " . DB_TABLE_CATEGORIES . " WHERE `alias_id` = '" . $page['id'] . "'");
    if (!$category = $result->fetch_assoc()):
        echo '<h1>Категория не найдена.</h1>';
    else:
        echo '<h1>' . $category['title'] . '</h1>';

        $allowed_sort = ['title', 'price'];
        $allowed_direction = ['asc', 'desc'];

        $sort = (isset($_GET['sort'])) ? htmlspecialchars($_GET['sort']) : false;
        $direction = (isset($_GET['direction'])) ? htmlspecialchars($_GET['direction']) : false;

        if ($sort && $direction) {
            $result_order_by = '';
            if (in_array($sort, $allowed_sort) && in_array($direction, $allowed_direction)) {
                $result_order_by = ' ORDER BY ' . $sort . ' ' . $direction;

            }
        }

        // <span class="right">Страницы: &nbsp;<b>1</b>&nbsp;&nbsp;<a href="https://artline3d.ru/bagety.html?page=2" class="pageResults" title=" Страница 2 ">2</a>&nbsp;&nbsp;<a href="https://artline3d.ru/bagety.html?page=3" class="pageResults" title=" Страница 3 ">3</a>&nbsp;&nbsp;<a href="https://artline3d.ru/bagety.html?page=2" class="pageResults" title=" Следующая страница ">Следующая</a>&nbsp;</span> 

        $offset = '';
        $offset_num = 0;
        if (isset($_GET['page']) && intval($_GET['page']) > 1) {
            $offset_num = (intval($_GET['page']) - 1) * MAX_ELEMENTS_PER_PAGE_PRODUCTS;
            $offset = ' OFFSET ' . $offset_num;
        }

        $result = $mysqli->query('
            SELECT 
                p.id, 
                p.title,
                p.description,
                p.image,
                p.category_id,
                p.alias_id,
                p.price,
                a.alias
            FROM ' . DB_TABLE_PRODUCTS . ' p
            LEFT OUTER JOIN ' . DB_TABLE_ALIASES . ' a 
            ON 
                a.id = p.alias_id 
            WHERE `category_id` = ' . $category['id'] . $result_order_by . 
            ' LIMIT ' . MAX_ELEMENTS_PER_PAGE_PRODUCTS . $offset
        );
        $i = 0;

        $products = [];

        $products_total_count = $mysqli->query("SELECT COUNT(id) FROM " . DB_TABLE_PRODUCTS . " WHERE category_id = " . $category['id']);
        $products_total_count = $products_total_count->fetch_assoc()['COUNT(id)'];

        while ($product = $result->fetch_assoc()) {
            $products[] = $product;
        }
        unset($result);

        if (!$products):
            echo '<p>Товары не найдены.</p>';
        else:

            $products_total_pages = ceil($products_total_count / MAX_ELEMENTS_PER_PAGE_PRODUCTS);

            $navigation_str_pagination = '<span class="right">Страницы: &nbsp;<b>' . $products_total_pages . '</b>&nbsp;</span>';
            if ($products_total_pages > 1) {
                $navigation_str_pagination = '<span class="right">Страницы:';
                if ($_GET['page'] > 1) {
                    $navigation_str_pagination .= '&nbsp;<a href="/' . $page['alias'] . '/?page=' . (intval($_GET['page']) - 1) . '" class="pageResults" title=" Предыдущая страница ">Предыдущая</a>&nbsp;&nbsp;';
                }
                for ($n = 1; $n <= $products_total_pages; $n++) {
                    if (intval($_GET['page'] == $n)) {
                        $navigation_str_pagination .= '&nbsp;<b>' . $n . '</b>&nbsp;&nbsp;';
                    } else {
                        $navigation_str_pagination .= '<a href="/' . $page['alias'] . '/?page=' . $n . '" class="pageResults" title="Страница ' . $n . '">' . $n . '</a>&nbsp;';
                    }
                }
                if ($_GET['page'] < $products_total_pages) {
                    $navigation_str_pagination .= '<a href="/' . $page['alias'] . '/?page=' . (intval($_GET['page']) + 1) . '" class="pageResults" title=" Следующая страница ">Следующая</a>&nbsp;';
                }
                $navigation_str_pagination .= '</span>'; 
            }

            $navigation_str = '
                <div class="navigation">
                    ' . $navigation_str_pagination . '
                    Показано <span class="bold">' . ($offset_num + 1) . '</span> - <span class="bold">' . (count($products) + $offset_num) . '</span> 
                    (всего позиций: <span class="bold">' . $products_total_count . '</span>)
                </div>
            ';

?>
            <div class="page">
                <div class="pageItem">
                    <p> Сортировка: 
                        <a href="/<?= $path[1]; ?>?sort=title&amp;direction=asc">название ▲</a>
                        <a href="/<?= $path[1]; ?>?sort=title&amp;direction=desc">▼</a> | 
                        <a href="/<?= $path[1]; ?>?sort=price&amp;direction=asc">цена ▲</a>
                        <a href="/<?= $path[1]; ?>?sort=price&amp;direction=desc">▼</a>
                    </p>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
            <?= $navigation_str; ?>
            <div class="clear"></div>
            <div class="row-fluid shop-products">
                <ul class="thumbnails">
                    <? foreach ($products as $product): ?>
                        <? 
                            $i++;
							$button_cart_title = (isset($cart['items'][$product['id']])) ? 'Убрать из корзины' : 'Добавить в корзину';
							$button_cart_class = (isset($cart['items'][$product['id']])) ? 'btn-remove-from-cart-js' : 'btn-add-to-cart-js';
						?>

                        <li class="item span4<?= ($i % 4 == 0 || $i == 1) ? ' first' : ''; ?>">
                            <div class="form-inline justify-content-center" >
                                <div class="thumbnail text-center"">
                                    <a href="/<?= $product['alias']; ?>" class="image">
                                        <img src="<?= checkImageExists($product['image']); ?>" alt="<?= $product['title']; ?>">
                                        <span class="frame-overlay"></span>
                                        <span class="price"><?= generatePriceString($product['price'], $current_rate, $current_symbol); ?></span>
                                    </a>
                                    <div class="inner notop nobottom text-center">
                                        <h4 class="title"><a href="/<?= $product['alias']; ?>"><?= $product['title']; ?></a></h4>
                                    </div>
                                </div>
                                <div class="inner darken notop text-center">
                                    <button class="btn btn-add-to-cart <?= $button_cart_class; ?>" data-id="<?= $product['id']; ?>">
                                        <i class="fa fa-shopping-cart"></i> <?= $button_cart_title; ?>
                                    </button>
                                </div>
                            </div>
                        </li>

                    <? endforeach; ?>
                </ul>
            </div>
            <div class="clear"></div>
                <?= $navigation_str; ?>
            <div class="clear"></div>
            <div class="page">
                <div class="pageItem">
                    <?= htmlspecialchars_decode($category['description']); ?>
                    <div class="clear"></div>
                </div>
            </div>
        <? endif;?>
    <? endif;?>