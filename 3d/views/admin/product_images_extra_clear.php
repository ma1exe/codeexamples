<?

    $sql = "SELECT * FROM " . DB_TABLE_PRODUCTS . " WHERE id = " . $_GET['id'];

    if ($result = $mysqli->query($sql)) {
        if ($result_product = $result->fetch_assoc()) {

            if ($result_product['images_extra']) {
                $images_extra = explode(',', $result_product['images_extra']);

                foreach ($images_extra as $image) {
                    $image_path = $_SERVER['DOCUMENT_ROOT'] . $image;

                    if (unlink($image_path)) {
                        echo '<p>Изображение "' . $image . '" успешно удалено с диска.</p>';
                    } else {
                        echo '<p>При удалении изображения "' . $image . '" с диска произошла ошибка.</p>';
                    }
                }

                $sql = "UPDATE " . DB_TABLE_PRODUCTS . " SET images_extra = '' WHERE id = " . $_GET['id'];

                if ($result_update = $mysqli->query($sql)) {
                    echo '<p>Изображения продукта успешно удалены из базы данных.</p>';
                } else {
                    echo '<p>При удалении изображений продукта из базы данных произошла ошибка.</p>';
                }
            } else {
                echo '<p>У продукта нет доп. изображений.</p>';
            }

        } else {
            echo '<p>Ошибка при работе с запросом на получение товара.</p>';
        }
    } else {
        echo '<p>Товар не найден.</p>';
    }
  
?>

<p><a href="<?= $_SERVER['HTTP_REFERER']; ?>">Вернуться назад</a>.</p>