<?
    require($_SERVER['DOCUMENT_ROOT'] . 'amofix/helpers/init.php');
    
    $log = new Logger();
    
    $log->lfile(AMOCRM_LOGS_FOLDER . @date('d_m_Y') . '_webhook_note.txt');

    $log->lwrite("Поступил $_SERVER[REQUEST_METHOD]-запрос с IP $_SERVER[REMOTE_ADDR]");

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $log->lwrite(json_encode($_POST, true));
        //$log->lwrite($_POST['leads']['note'][0]['note']['text']);

        $allowed_note_types = [
            4, // примечание с текстом
            5 // примечание с файлом
        ];

        if (
            isset($_POST['leads']['note'][0]['note']['note_type']) && 
            intval($_POST['leads']['note'][0]['note']['note_type']) > 0 && 
            in_array(intval($_POST['leads']['note'][0]['note']['note_type']), $allowed_note_types) && 

            isset($_POST['leads']['note'][0]['note']['element_id']) && 
            intval($_POST['leads']['note'][0]['note']['element_id']) > 0
        ) {

            preg_match_all('/artsofte\.planfix\.ru\/task\/(\d+)/', $_POST['leads']['note'][0]['note']['text'], $planfix_task_id_fix);
            $bool_planfix_double = false;
            if ($planfix_task_id_fix !== null) {
                foreach ($planfix_task_id_fix[1] as $task_id_fix) {
                    $double_string = "Запись из Planfix (https://artsofte.planfix.ru/task/$task_id_fix/)";
                    if (substr($_POST['leads']['note'][0]['note']['text'], 0, strlen($double_string)) === $double_string) {
                        $bool_planfix_double = true;
                    }
                }
            }

            if ($bool_planfix_double) {
                $log->lwrite("Упс! Кажется, это сообщение-дубль.");
            } else {
                
                $lead_id = intval($_POST['leads']['note'][0]['note']['element_id']);
                $log->lwrite("ID сделки = $lead_id");

                $AmoCRM = new AmoCRM();

                $request = $AmoCRM->request("/api/v4/leads/$lead_id");

                $log->lwrite(json_encode($request, true));

                if ($request['success']) {
                    $response = json_decode($request['response'], true);
                    $bool_planfix_task_id = false;
                    $planfix_task_url = false;

                    if (isset($response['custom_fields_values'][0]['values'][0]['value'])) {
                        foreach ($response['custom_fields_values'] as $custom_field) {
                            if (
                                intval($custom_field['field_id']) === 258955 && 
                                isset($custom_field['values'][0]['value']) &&
                                preg_match('/artsofte\.planfix\.ru\/task\/(\d+)/', $custom_field['values'][0]['value'], $planfix_task_id)
                            ) {
                                $bool_planfix_task_id = true;
                                $planfix_task_url = $custom_field['values'][0]['value'];
                                $log->lwrite("URL для планфикса найден: $planfix_task_url.");
                            }
                        }
                    }

                    if (
                        $bool_planfix_task_id
                    ) {
                        $planfix_task_id = $planfix_task_id[1];
                        $log->lwrite("ID задачи в планфиксе = $planfix_task_id.");

                        $Planfix = new Planfix();

                        $text = "Запись из AmoCRM (https://artsoftedigital.amocrm.ru/leads/detail/$lead_id)";

                        $amocrm_user_name = $AmoCRM->getUser($_POST['leads']['note'][0]['note']['created_by']);

                        if ($amocrm_user_name) {
                            $text .= ' от пользователя ' . $amocrm_user_name . '<br/><br/>';
                        } else {
                            $text .= '<br/><br/>';
                        }

                        if (isset($_POST['leads']['note'][0]['note']['attachement']) && !empty($_POST['leads']['note'][0]['note']['attachement'])) {
                            $file = $_POST['leads']['note'][0]['note']['attachement'];
                            $text .= "Прикрепленный файл: https://artsoftedigital.amocrm.ru/download/$file";
                        } else {
                            $text .= $_POST['leads']['note'][0]['note']['text'];
                        }

                        // Получаем всех пользователей задачи: аудиторы, исполнители, участники

                        $requestXml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>
                        <request method="task.get">
                        <account></account>
                        <sid></sid>
                        <task>
                            <id></id>
                            <general>' . $planfix_task_id . '</general>
                        </task>
                        </request>');

                        $requestXml->account = $Planfix->account;
                        $requestXml->pageCurrent = 1;

                        $response = new SimpleXMLElement($Planfix->request($requestXml));

                        // workers - исполнители
                        // members - участники 
                        // auditors - аудиторы

                        // $users_types = ['workers', 'members', 'auditors']; 
                        $users_types = ['workers'];

                        $users = [];
                        $groups = [];

                        foreach ($users_types as $user_type) {
                            if (isset($response->task->$user_type->groups)) {
                                foreach ($response->task->$user_type->groups->children() as $group) {
                                    $id = $group->id->__toString();
                                    if (!in_array($id, $groups)) {
                                        $groups[] = $id;
                                    }
                                }
                            }

                            if (isset($response->task->$user_type->users)) {
                                foreach ($response->task->$user_type->users->children() as $user) {
                                    $id = $user->id->__toString();
                                    if (!in_array($id, $users)) {
                                        $users[] = $id;
                                    }
                                }
                            }
                        }

                        foreach ($groups as $group) {

                            $requestXml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>
                            <request method="user.getList">
                            <userGroup>
                                <id>' . $group . '</id>
                            </userGroup>
                            </request>');

                            $requestXml->account = $Planfix->account;
                            $requestXml->pageCurrent = 1;

                            $response_users = new SimpleXMLElement($Planfix->request($requestXml));

                            if (isset($response_users->users)) {
                                foreach ($response_users->users->children() as $user) {
                                    $id = $user->id->__toString();
                                    if (!in_array($id, $users)) {
                                        $users[] = $id;
                                    }
                                }
                            }
                        }

                        if ($users) {
                            $notify_users = '<notifiedList><user>';
                            foreach ($users as $user) {
                                $notify_users .= "<id>$user</id>";
                            }
                            $notify_users .= '</user></notifiedList>';
                        } else {
                            $notify_users = '';
                        }

                        //

                        $requestXml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>
                        <request method="action.add">
                            <account></account>
                            <sid></sid>
                            <action>
                                <description><![CDATA[' . $text . ']]></description>
                                <task>
                                    <general>' . $planfix_task_id . '</general>
                                </task>
                                <owner>
                                    <id>4583362</id>
                                </owner>' . $notify_users . '
                            </action>
                        </request>');

                        $requestXml->account = $Planfix->account;
                        $requestXml->pageCurrent = 1;

                        $Planfix->request($requestXml);

                    } else {
                        $log->lwrite('URL для планфикса не обнаружен.');
                    }
                }
            }
        } else {
            $log->lwrite('Запрос не содержит ID сделки, либо тип примечания не разрешен.');
        }
     
    }
    
    // close log file
    $log->lclose();