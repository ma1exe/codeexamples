<?
    //$sql = "DELETE FROM " . DB_TABLE_CATEGORIES . " WHERE id = " . $_GET['id'];

    $sql = "SELECT `alias_id` FROM " . DB_TABLE_CATEGORIES . " WHERE id = " . $_GET['id'];

    if ($result = $mysqli->query($sql)): 
        if ($category = $result->fetch_assoc()):
            $sql_category = "DELETE FROM " . DB_TABLE_CATEGORIES . " WHERE id = " . $_GET['id'];
            if ($mysqli->query($sql_category)):
?>
                <p>Категория успешно удалена.</p>
                <? 
                    $sql_alias = "DELETE FROM " . DB_TABLE_ALIASES . " WHERE id = " . $category['alias_id'];
                    if ($mysqli->query($sql_alias)):
                ?>
                        <p>Алиас успешно удален.</p>
                    <? else: ?>
                        <p>При удалении алиаса произошла ошибка.</p>
                    <? endif; ?>
            <? else: ?>
                <p>При удалении категории произошла ошибка.</p>
            <? endif; ?>
        <? else: ?>
            <p>Категория не найдена.</p>
        <? endif; ?>
<? else: ?>
    <p>Ошибка при удалении: некорректный запрос к базе данных.</p>
<? endif; ?>

<p><a href="/admin/categories<?= SITE_URLS_SUFFIX; ?>">Вернуться назад</a>.</p>