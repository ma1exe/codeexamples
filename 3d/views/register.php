<h1>Регистрация</h1>
<form name="create_account" id="form_fegister" action="/ajax/register<?= SITE_URLS_SUFFIX; ?>" method="post">
    <div class="page">
        <div class="pagecontent">
            <span class="Requirement"> поля, отмеченные *, обязательны для заполнения</span><br><br>
            <fieldset class="form">
                <legend>Ваши персональные данные</legend>
                <p><label for="first_name">Имя:</label> <input type="text" name="first_name" id="first_name" required>&nbsp;<span class="Requirement">*</span></p>
                <!--
                    <p>
                        <label for="last_name">Фамилия:</label> <input type="text" name="last_name" id="last_name" required>
                        &nbsp;<span class="Requirement">*</span>
                    </p>
                -->
                <p><label for="email">Ваш E-Mail:</label> <input type="text" name="email" id="email_address" required>&nbsp;<span class="Requirement">*</span></p>
            </fieldset>
            <fieldset class="form"></fieldset>
            <fieldset class="form"></fieldset>
            <fieldset class="form">
                <legend>Укажите свой пароль.</legend>
                <p><label for="password">Введите пароль:</label> <input type="password" name="password" id="password" required>&nbsp;<span class="Requirement">*</span></p>
                <p>
                    <label for="confirmation_password">Подтвердите пароль:</label> 
                    <input type="password" name="confirmation_password" id="confirmation_password" required>
                    &nbsp;<span class="Requirement">*</span>
                </p>
            </fieldset>
            <div class="form-anti-bot"><strong>Оставьте поле пустым</strong> <span class="required">*</span><input type="email" name="confirmation_email" id="confirmation_email" size="30" value=""></div>
        </div>
    </div>
    <div class="pagecontentfooter"><span class="button"><button type="submit"><img src="/web/images/icons/submit.png" alt="Зарегистрироваться" title=" Зарегистрироваться " width="12" height="12">&nbsp;Зарегистрироваться</button></span></div>
    <br>
    <div> Нажимая кнопку, я даю согласие на обработку своих персональных данных. <a href="/privacy<?= SITE_URLS_SUFFIX; ?>">Подробнее о защите персональной информации.</a></div>
</form>