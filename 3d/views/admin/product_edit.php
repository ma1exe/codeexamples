<?

    if (isset($_GET['id']) && intval($_GET['id'])) {
        $result = $mysqli->query('
            SELECT
                p.id,
                p.title,
                p.description,
                p.image,
                p.images_extra,
                p.price,
                p.category_id,
                a.alias,
                p.alias_id,
                a.meta_title,
                a.meta_description,
                a.meta_keywords
            FROM
                ' . DB_TABLE_PRODUCTS . ' p
            LEFT OUTER JOIN ' . DB_TABLE_ALIASES . ' a ON
                a.id = p.alias_id
            WHERE
                p.id = ' . $_GET['id']
        );
        if ($result && $result->num_rows) {
            $result_product = $result->fetch_assoc();
        } else {
            echo '<p>Товар не найден.</p>';
        }
    } else {
        echo '<p>Товар не найден.</p>';
    }

    if ($result_product) {

        $allowed_image_extensions = ['jpg', 'jpeg', 'png', 'gif'];
        $allowed_image_extensions_str = [];

        foreach ($allowed_image_extensions as $ext) {
            $allowed_image_extensions_str[] = '.' . $ext;
        }

        $allowed_image_extensions_str = implode(', ', $allowed_image_extensions_str);

        $current_image = false;

        if (isset($_POST['submit'])) {
            // Если нет ошибок
            if (!empty($_POST['title']) && intval($_POST['price']) >= 0 && !empty($_POST['alias']) && intval($_GET['id'])) {
    
                $form = $_POST;

                if (isset($_FILES['image']['size']) && intval($_FILES['image']['size'])) {
                    $file_name = basename($_FILES['image']['name']);
                    $file_info = pathinfo($file_name);
    
                    if (in_array(strtolower($file_info['extension']), $allowed_image_extensions)) {
                        $file_new_name .= $file_info['filename'] . '.' . $file_info['extension'];
    
                        $file_path = $_SERVER['DOCUMENT_ROOT'] . '/web/images/products/' . $file_new_name;
    
                        if (move_uploaded_file($_FILES['image']['tmp_name'], $file_path)) {
                            $form['image'] =  '/web/images/products/' . $file_new_name;
                        } else {
                            unset($form['image']);
                        }
                    } else {
                        addFlash('<p>Допустимые расширения изображения: ' . implode(', ', $allowed_image_extensions_str) . '</p>'); 
                        unset($form['image']);
                    }
                }

                $images_extra = [];

                if (isset($_FILES['images_extra'])) {
                    $images_extra_total = count($_FILES['images_extra']['name']);
                    $images_extra_errors = false;

                    for( $i=0 ; $i < $images_extra_total ; $i++ ) {
                        $file_name = basename($_FILES['images_extra']['name'][$i]);
                        $file_info = pathinfo($file_name);

                        if (in_array(strtolower($file_info['extension']), $allowed_image_extensions)) {

                            $file_new_name .= $file_info['filename'] . '.' . $file_info['extension'];
            
                            $file_path = $_SERVER['DOCUMENT_ROOT'] . '/web/images/products/' . $file_new_name;
        
                            if (move_uploaded_file($_FILES['images_extra']['tmp_name'][$i], $file_path)) {
                                $images_extra[] = '/web/images/products/' . $file_new_name;
                            } else {
                                $images_extra_errors = false;
                            }
                        } else {
                            $images_extra_errors = false;
                        }
                    }

                    if ($images_extra_errors) {
                        addFlash('<p>При загрузке доп. изображений произошла одна или несколько ошибок.</p>'); 
                    }
                }

                if ($images_extra) {
                    $form['images_extra'] = implode(',', $images_extra);
                } else {
                    unset($form['images_extra']);
                }

                $meta_title = htmlspecialchars($form['meta_title']);
                $meta_description = htmlspecialchars($form['meta_description']);
                $meta_keywords = htmlspecialchars($form['meta_keywords']);

                unset($form['submit'], $form['image_name'], $form['meta_title'], $form['meta_description'], $form['meta_keywords']);
    
                $new_alias = makeAlias($form['alias']);
    
                $sql = "UPDATE " . DB_TABLE_ALIASES . " SET `alias` = '" . $new_alias . "', `meta_title` = '" . $meta_title . "', `meta_description` = '" . $meta_description . "', `meta_keywords` = '" . $meta_keywords . "' WHERE id = " . $result_product['alias_id'];
    
                if ($mysqli->query($sql)) {

                    // Рекомендуемые
                    if (!empty($form['products_related'])) {
                        $sql = "DELETE FROM " . DB_TABLE_PRODUCTS_RELATED . " WHERE `product_id` = " . intval($_GET['id']);
                        if ($mysqli->query($sql)) {
                            $rel_products_str = [];
                            foreach ($form['products_related'] as $rel_product) {
                                $rel_products_str[] = '(' . intval($_GET['id']) . ', ' . intval($rel_product) . ')';
                            }
                            $sql = "INSERT INTO " . DB_TABLE_PRODUCTS_RELATED . " VALUES " . implode(', ', $rel_products_str);
                            if (!$mysqli->query($sql)) {
                                addFlash('<p>Рекомендуемые продукты не обновлены: произошла ошибка при записи новых данных.</p>'); 
                            }
                        } else {
                            addFlash('<p>Рекомендуемые продукты не обновлены: произошла ошибка при удалении старых данных.</p>'); 
                        }
                    }

                    
                    unset($form['alias'], $new_alias, $meta_title, $meta_description, $meta_keywords, $form['products_related']);
    
                    $sql = makeSqlUpdate(DB_TABLE_PRODUCTS, $form, intval($_GET['id']));
    
                    if ($result = $mysqli->query($sql)) {
                        $result = $mysqli->query('
                            SELECT
                                p.id,
                                p.title,
                                p.description,
                                p.image,
                                p.price,
                                p.category_id,
                                a.alias,
                                p.alias_id
                            FROM
                                ' . DB_TABLE_PRODUCTS . ' p
                            LEFT OUTER JOIN ' . DB_TABLE_ALIASES . ' a ON
                                a.id = p.alias_id
                            WHERE
                                p.id = ' . $_GET['id']
                        );
                        $result_product = $result->fetch_assoc();
                        addFlash('<p>Товар обновлен!</p>'); 
                        $is_reload_required = true;
                    } else {
                        addFlash('<p>Товар не обновлен: произошла ошибка при работе с базой данных.</p>');
                        addFlash('<p>' . $mysqli->error . '</p>');
                    }
                } else {
                    addFlash('<p>Товар не обновлен: ошибка при обновлении алиаса и meta-тегов.</p>');
                }
                
            } else {
                addFlash('<p>При обновлении товара произошли следующие ошибки:<ul>');
    
                if (empty($_POST['title'])) {
                    addFlash('<li>Название товара отсутствует.</li>');
                }
                
                if (intval($_POST['price']) < 0) {
                    addFlash('<li>Цена товара не может быть отрицательным числом.</li>');
                }
    
                if (empty($_POST['alias'])) {
                    addFlash('<li>Алиас не может быть пустым.</li>');
                }
    
                addFlash('</ul></p>');
            }

        } 

        if (getFlash() && !$is_reload_required) {
            echo getFlash();
            clearFlash();
        }

        require('./views/admin/forms/product.php');

    }
