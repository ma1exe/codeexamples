<?

    $result_product = false;
    $result_alias = false;
    $result_related = [];

    if (isset($_GET['id']) && intval($_GET['id']) > 0) {
        $result = $mysqli->query('
            SELECT
                *
            FROM
                ' . DB_TABLE_PRODUCTS . '
            WHERE
                id = ' . $_GET['id']
        );
        if ($result && $result->num_rows) {
            $result_product = $result->fetch_assoc();

            $result2 = $mysqli->query('
                SELECT
                    *
                FROM
                    ' . DB_TABLE_ALIASES . '
                WHERE
                    id = ' . $result_product['alias_id']
            );

            $result_alias = $result2->fetch_assoc();

            $result3 = $mysqli->query('
                SELECT
                    *
                FROM
                    ' . DB_TABLE_PRODUCTS_RELATED . '
                WHERE
                    product_id = ' . $_GET['id']
            );

            while ($result_rel = $result3->fetch_assoc()) {
                $result_related[] = $result_rel['related_id'];
            }

        } else {
            echo '<p>Товар не найден.</p>';
        }
    } else {
        echo '<p>Товар не найден.</p>';
    }

    if ($result_product) {

        unset($result_product['id']);

        $result_product['title'] .= ' (копия ' . date('d/m/Y H:i:s') . ')';

        $result_product['image'] = '';
        $result_product['images_extra'] = '';

        $result_product['alias_id'] = '';

        if ($result_alias) {
            $sql = "INSERT INTO " . DB_TABLE_ALIASES . " (`alias`, `type`, `meta_title`, `meta_description`, `meta_keywords`) VALUES ('" . generateHashString() . "', 'product', '" . $result_alias['meta_title'] . "', '" . $result_alias['meta_description'] . "', '" . $result_alias['meta_keywords'] . "')";

            if ($mysqli->query($sql)) {
                $result_product['alias_id'] = $mysqli->insert_id;
            } else {
                addFlash('<p>Алиас не скопирован: произошла ошибка при записи данных.</p>'); 
            }
        }

        $result_product['description'] = htmlspecialchars_decode($result_product['description']);

        $sql = makeSqlInsert(DB_TABLE_PRODUCTS, $result_product);

        if ($mysqli->query($sql)) {
            addFlash('<p>Товар скопирован.</p>');

            if (!empty($result_related)) {
                $rel_products_str = [];
                foreach ($result_related as $rel_product) {
                    $rel_products_str[] = '(' . intval($mysqli->insert_id) . ', ' . intval($rel_product) . ')';
                }
                $sql = "INSERT INTO " . DB_TABLE_PRODUCTS_RELATED . " VALUES " . implode(', ', $rel_products_str);
                if (!$mysqli->query($sql)) {
                    addFlash('<p>Рекомендуемые продукты не скопированы: произошла ошибка при записи данных.</p>'); 
                }
            }
        } else {
            addFlash('<p>При копировании товара произошла ошибка.</p>');
        }

        if (getFlash()) {
            echo getFlash();
            clearFlash();
        }

    }

?>

<p><a href="/admin/products<?= SITE_URLS_SUFFIX; ?>">Вернуться к продуктам</a>.</p>
