<?php 
    use yii\helpers\Url;
?>

<!-- aside Widget -->
<div class="aside">
    <h3 class="aside-title">Цена</h3>
    <div class="price-filter">
        <div id="price-slider"></div>
        <div class="input-number price-min">
            <input id="price-min" type="number">
            <span class="qty-up">+</span>
            <span class="qty-down">-</span>
        </div>
        <span>-</span>
        <div class="input-number price-max">
            <input id="price-max" type="number">
            <span class="qty-up">+</span>
            <span class="qty-down">-</span>
        </div>
    </div>
</div>
<!-- /aside Widget -->

<!-- aside Widget -->
<div class="aside">
    <h3 class="aside-title">Производитель</h3>
    <div class="checkbox-filter">
        <?php foreach ($manufacturers as $manufacturer): ?>

            <div class="input-checkbox">
                <input type="checkbox" id="brand-<?= $manufacturer['id']; ?>">
                <label for="brand-<?= $manufacturer['id']; ?>">
                    <span></span>
                    <?= $manufacturer['name']; ?>
                </label>
            </div>

        <?php endforeach; ?>
    </div>
</div>
<!-- /aside Widget -->

<!-- aside Widget -->
<div class="aside">
    <h3 class="aside-title">Популярные</h3>
    <?php foreach ($popular_products as $product): ?>
        <?php
            $image_url = $this->context->isImageExists($product['image']);
            $price_converted = $this->context->convertPriceWithRate($product['price'], $product->currency->rate);
        ?>

        <div class="product-widget">
            <div class="product-img">
                <img src="<?= $image_url ?>" alt="<?= $product['name']; ?>">
            </div>
            <div class="product-body">
                <p class="product-category"><?= $product->category->name; ?></p>
                <h3 class="product-name">
                    <a href="<?= Url::toRoute('catalog/'.$product->category->link.'/'.$product['id']); ?>">
                        <?= $product['name']; ?>
                    </a>
                </h3>
                <h4 class="product-price"><?= $this->context->formatPriceToRUB($price_converted); ?> руб.</h4>
            </div>
        </div>

    <?php endforeach; ?>
</div>
<!-- /aside Widget -->