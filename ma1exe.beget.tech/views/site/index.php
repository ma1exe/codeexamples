<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Главная';
?>

<!-- SECTION -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- shop -->
            <div class="col-md-3 col-xs-6">
                <div class="shop">
                    <div class="shop-img">
                        <img src="/images/catalog/engines/black-bee.jpg" alt="Двигатели">
                    </div>
                    <div class="shop-body">
                        <h3>Двигатели</h3>
                        <a href="<?= Url::toRoute('catalog/engines'); ?>" class="cta-btn">Перейти <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- /shop -->

            <!-- shop -->
            <div class="col-md-3 col-xs-6">
                <div class="shop">
                    <div class="shop-img">
                        <img src="/images/catalog/spares/117.png" alt="Запчасти">
                    </div>
                    <div class="shop-body">
                        <h3>Запчасти</h3>
                        <a href="<?= Url::toRoute('catalog/spares'); ?>" class="cta-btn">Перейти <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- /shop -->

            <!-- shop -->
            <div class="col-md-3 col-xs-6">
                <div class="shop">
                    <div class="shop-img">
                        <img src="/images/catalog/paragliders/zorro_2_1.jpg" alt="Парапланы">
                    </div>
                    <div class="shop-body">
                        <h3>Парапланы</h3>
                        <a href="<?= Url::toRoute('catalog/paragliders'); ?>" class="cta-btn">Перейти <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- /shop -->

            <!-- shop -->
            <div class="col-md-3 col-xs-6">
                <div class="shop">
                    <div class="shop-img">
                        <img src="/images/catalog/paramotors/vityaz_1.jpg" alt="Парамоторы">
                    </div>
                    <div class="shop-body">
                        <h3>Парамоторы</h3>
                        <a href="<?= Url::toRoute('catalog/paramotors'); ?>" class="cta-btn">Перейти <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- /shop -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /SECTION -->

<!-- SECTION -->
<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-xs-12">
						<div class="section-title">
							<h3 class="title">Новинки</h3>
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-xs-12">
						<div class="row">
							<?php foreach($new_products as $product): ?>
								<div class="col-xs-6 col-md-3">
									<?= $this->render('//blocks/product', compact('product')); ?>
								</div>
							<? endforeach; ?>
						</div>
					</div>
					<!-- Products tab & slick -->
				</div>
				<!-- /row -->

				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-xs-12">
						<div class="section-title">
							<h3 class="title">Популярные</h3>
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-xs-12">
						<div class="row">
							<?php foreach($popular_products as $product): ?>
								<div class="col-xs-6 col-md-3">
									<?= $this->render('//blocks/product', compact('product')); ?>
								</div>
							<? endforeach; ?>
						</div>
					</div>
					<!-- Products tab & slick -->
				</div>
				<!-- /row -->

				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-xs-12">
						<div class="section-title">
							<h3 class="title">Случайные</h3>
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-xs-12">
						<div class="row">
							<?php foreach($random_products as $product): ?>
								<div class="col-xs-6 col-md-3">
									<?= $this->render('//blocks/product', compact('product')); ?>
								</div>
							<? endforeach; ?>
						</div>
					</div>
					<!-- Products tab & slick -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->