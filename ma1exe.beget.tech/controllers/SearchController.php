<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use app\models\CatalogProducts;
use app\models\CatalogCategories;
use app\models\CatalogCurrencies;
use app\models\CatalogManufacturers;
use yii\data\Pagination;

class SearchController extends GlobalController
{
    public function actionIndex($category_link = false) 
    {
        $products = [];
        $categories = [];
        $currencies = [];

        $search_query = Yii::$app->request->get('text');

        if ($search_query && strlen($search_query) > 2) {
            $products = CatalogProducts::find()->where(['like', 'name', $search_query])->orWhere(['like', 'sku', $search_query])->asArray()->all();
        }
        $categories_query = CatalogCategories::find()->asArray()->all();
        
        foreach ($categories_query as $category) {
            $categories[$category['id']] = [
                'id' => $category['id'],
                'name' => $category['name'],
                'link' => $category['link']
            ];
        }

        $currencies_query = CatalogCurrencies::find()->asArray()->all();
        foreach ($currencies_query as $currency) {
            $currencies[$currency['id']] = [
                'symbol' => $currency['symbol'],
                'rate' => $currency['rate']
            ];
        }

        //$this->registerCssFile("/path/to/your/file/in/web/folder/style.css");
        return $this->render('index', compact('products', 'categories', 'currencies'));
    }

}