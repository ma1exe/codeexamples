<?php 
    use yii\helpers\Url;
    use yii\widgets\LinkPager; 

    $this->title = 'Корзина';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="col-md-12">

        <div class="row">
            <div class="col-xs-12">
                <h1><?= $this->title; ?></h1>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <?php if ($this->context->cart_products): ?>
                    <?php foreach ($this->context->cart_products as $product): ?>
                        <p><?= $product['name']; ?>, <?= $product['price']; ?> x<?= $product['count']; ?></p>
                    <?php endforeach; ?>
                    <a href="<?= Url::toRoute('cart/clearitems'); ?>">Очистить корзину</a>
                    <a href="<?= Url::toRoute('cart/checkout'); ?>">Оформить заказ</a>
                <?php else: ?>
                    <p>Корзина пуста.</p>
                    <p> Вы можете <a href="<?= Url::toRoute('catalog/index'); ?>">перейти в каталог</a> или <a href="<?= Url::toRoute('site/index'); ?>">вернуться на главную</a>.</p>
                <?php endif; ?>
            </div>
        </div>

    </div>
</div>