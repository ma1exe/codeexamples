<?

    $errors = [];

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($user['id']) && intval($user['id'])) {

            $sql_order = "INSERT INTO " . DB_TABLE_ORDERS . " (`user_id`, `comment`, `total_sum`) VALUES (" . $user['id'] . ", '" . htmlspecialchars($_POST['comment']) . "', " . $_POST['total_sum'] . ")";

            if ($mysqli->query($sql_order)) {
                $order_id = $mysqli->insert_id;
                $sql_order_products = "INSERT INTO " . DB_TABLE_ORDERS_PRODUCTS . " (`order_id`, `product_id`, `quantity`) VALUES ";
                $sql_order_products_arr = [];
                foreach ($_POST['items'] as $id => $quantity) {
                    $sql_order_products_arr[] = "(" . $order_id . ", " . $id . ", " . 1 . ")";
                }
                $sql_order_products .= implode(', ', $sql_order_products_arr);
    
                if ($mysqli->query($sql_order_products)) {
                    $_SESSION['cart'] = [];
                    echo json_encode(['success' => true, 'errors' => $errors]);
                } else {
                    $errors[] = 'При добавлении товаров к заказу произошла ошибка. Попробуйте позже.';
                    // $errors[] = $sql;
                    echo json_encode(['success' => false, 'errors' => $errors]);
                }
            } else {
                $errors[] = 'При оформлении заказа произошла ошибка. Попробуйте позже.';
                // $errors[] = $sql;
                echo json_encode(['success' => false, 'errors' => $errors]);
            }
        } else {
            if (empty($_POST['email'])) {
                $errors[] = 'E-mail не может быть пустым.';
            }
            if (empty($_POST['first_name'])) {
                $errors[] = 'Имя не может быть пустым.';
            }
            /*if (empty($_POST['last_name'])) {
                $errors[] = 'Фамилия не может быть пустой.';
            }*/

            if ($errors) {
                echo json_encode(['success' => false, 'errors' => $errors]);
            } else {
                $user = $mysqli->query("SELECT * FROM " . DB_TABLE_USERS . " WHERE `email` = '" . htmlspecialchars($_POST['email']) . "'");
                if (!$user_db = $user->fetch_assoc()) {
                    $password = generateHashString(MIN_LENGTH_USER_PASSWORD);
                    $email = htmlspecialchars($_POST['email']);
                    $first_name = htmlspecialchars($_POST['first_name']);
                    $last_name = ''; //htmlspecialchars($_POST['last_name']);
                    $user_hash = generateHashString();
                    $sql = "INSERT INTO " . DB_TABLE_USERS . " (`email`, `password`, `first_name`, `last_name`, `hash`) VALUES ('" . $email . "', '" . password_hash($password, PASSWORD_DEFAULT) .  "', '" . $first_name . "', '" . $last_name . "', '" . $user_hash . "')";
                    if (!$mysqli->query($sql)) {
                        $errors[] = 'При создании пользователя произошла ошибка. Попробуйте позже.';
                        // $errors[] = $sql;
                        echo json_encode(['success' => false, 'errors' => $errors]);
                    } else {
                        setcookie('user_id', $mysqli->insert_id, time()+60*60*24*30, '/');
                        setcookie('user_hash', $user_hash, time()+60*60*24*30, '/', null, null, true); // httponly !!! , null, null, true
                        
                        $sql_order = "INSERT INTO " . DB_TABLE_ORDERS . " (`user_id`, `comment`, `total_sum`) VALUES (" . $user_db['id'] . ", '" . htmlspecialchars($_POST['comment']) . "', " . $_POST['total_sum'] . ")";

                        if ($mysqli->query($sql_order)) {
                            $order_id = $mysqli->insert_id;
                            $sql_order_products = "INSERT INTO " . DB_TABLE_ORDERS_PRODUCTS . " (`order_id`, `product_id`, `quantity`) VALUES ";
                            $sql_order_products_arr = [];
                            foreach ($_POST['items'] as $id => $quantity) {
                                $sql_order_products_arr[] = "(" . $order_id . ", " . $id . ", " . $quantity . ")";
                            }
                            $sql_order_products .= implode(', ', $sql_order_products_arr);
                
                            if ($mysqli->query($sql_order_products)) {
                                $_SESSION['cart'] = [];
                                echo json_encode(['success' => true, 'errors' => $errors]);
                            } else {
                                $errors[] = 'При добавлении товаров к заказу произошла ошибка. Попробуйте позже.';
                                // $errors[] = $sql;
                                echo json_encode(['success' => false, 'errors' => $errors]);
                            }
                        } else {
                            $errors[] = 'При оформлении заказа произошла ошибка. Попробуйте позже.';
                            // $errors[] = $sql;
                            echo json_encode(['success' => false, 'errors' => $errors]);
                        }
                    }
                } else {
                    $errors[] = 'Пользователь с таким e-mail уже зарегистрирован. Пожалуйста, войдите в систему и повторите попытку.';
                    echo json_encode(['success' => false, 'errors' => $errors]);
                }
            }
        }
    }

        /*} else {
            echo json_encode(['success' => false, 'errors' => $errors]);
        }
    } else {
        echo json_encode(['success' => false, 'errors' => $errors]);
    }*/