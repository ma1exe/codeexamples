<?    
    if (isset($_GET['id']) && intval($_GET['id'])) {
        $result = $mysqli->query('
            SELECT
                o.user_id,
                o.comment,
                o.total_sum,
                o.date,
                u.first_name,
                u.last_name
            FROM
                ' . DB_TABLE_ORDERS . ' o
            LEFT OUTER JOIN ' . DB_TABLE_USERS . ' u ON
                u.id = o.user_id
            WHERE
                o.id = ' . $_GET['id']
        );
        if ($result && $result->num_rows) {
            $order = $result->fetch_assoc();
            require('./views/admin/forms/order.php');
        } else {
            echo '<p>Заказ не найден.</p>';
        }
    } else {
        echo '<p>Заказ не найден.</p>';
        die();
    }