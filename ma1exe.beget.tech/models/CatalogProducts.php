<?php

namespace app\models;
 
use yii\db\ActiveRecord;
 
class CatalogProducts extends ActiveRecord
{


    function rules() {
        return [
            [['name', 'category_id', 'manufacturer_id', 'price', 'currency_id'], 'required'],
            ['description', 'safe']
        ];
    }


    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'sku' => 'Артикул',
            'description' => 'Описание',
            'category_id' => 'Категория',
            'manufacturer_id' => 'Производитель',
            'price' => 'Цена',
            'currency_id' => 'Валюта',
            'updated' => 'Обновлен',
            'viewed' => 'Количество просмотров',
        ];
    }


    function getCategory() {
        return $this->hasOne(CatalogCategories::className(), ['id' => 'category_id']);
    }


    function getCurrency() {
        return $this->hasOne(CatalogCurrencies::className(), ['id' => 'currency_id']);
    }


    function getManufacturer() {
        return $this->hasOne(CatalogManufacturers::className(), ['id' => 'manufacturer_id']);
    }

    
}