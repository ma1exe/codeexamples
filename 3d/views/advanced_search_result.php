<? 
    $keywords = (isset($_GET['keywords']) && !empty($_GET['keywords'])) ? htmlspecialchars($_GET['keywords']) : '';

    $products_total_count = $mysqli->query("SELECT COUNT(id) FROM " . DB_TABLE_PRODUCTS . " WHERE `title` LIKE '%" . $keywords . "%' OR `description` LIKE '%" . $keywords . "%'");
    $products_total_count = $products_total_count->fetch_assoc()['COUNT(id)'];

    $allowed_sort = ['title', 'price'];
    $allowed_direction = ['asc', 'desc'];

    $sort = (isset($_GET['sort'])) ? htmlspecialchars($_GET['sort']) : false;
    $direction = (isset($_GET['direction'])) ? htmlspecialchars($_GET['direction']) : false;

    if ($sort && $direction) {
        $result_order_by = '';
        if (in_array($sort, $allowed_sort) && in_array($direction, $allowed_direction)) {
            $result_order_by = ' ORDER BY ' . $sort . ' ' . $direction;

        }
    }

    $on_page = MAX_ELEMENTS_PER_PAGE_PRODUCTS;

    if (isset($_GET['on_page']) && intval($_GET['on_page']) > 0) {
        $on_page = intval($_GET['on_page']);
    }

    $offset = '';
    $offset_num = 0;
    if (isset($_GET['page']) && intval($_GET['page']) > 1) {
        $offset_num = (intval($_GET['page']) - 1) * $on_page;
        $offset = ' OFFSET ' . $offset_num;
    }

    $sql = "
        SELECT 
                p.id, 
                p.title,
                p.description,
                p.image,
                p.category_id,
                p.alias_id,
                p.price,
                a.alias
            FROM " . DB_TABLE_PRODUCTS . " p
            LEFT OUTER JOIN " . DB_TABLE_ALIASES . " a 
            ON 
                a.id = p.alias_id 
        WHERE `title` LIKE '%" . $keywords . "%' OR `description` LIKE '%" . $keywords . "%' "  . $result_order_by . "
         LIMIT " . $on_page . $offset;

    $result = $mysqli->query($sql);

    $products = [];

    while ($product = $result->fetch_assoc()) {
        $products[] = $product;
    }
    unset($result);
?>

<? if ($products): ?>
    <h1><?= $keywords; ?></h1>

    <?
    $products_total_pages = ceil($products_total_count / $on_page);

    function createUrlWithPage($page = 1) {
        $query = $_GET;
        // replace parameter(s)
        $query['page'] = $page;
        // rebuild url
        $query_result = http_build_query($query);
        // new link
        return '/advanced_search_result' . SITE_URLS_SUFFIX . '?' . $query_result;
    }

    $navigation_str_pagination = '<span class="right">Страницы: &nbsp;<b>' . $products_total_pages . '</b>&nbsp;</span>';
    if ($products_total_pages > 1) {
        $navigation_str_pagination = '<span class="right">Страницы:';
        if ($_GET['page'] > 1) {
            $navigation_str_pagination .= '&nbsp;<a href="' . createUrlWithPage(intval($_GET['page']) - 1) . '" class="pageResults" title=" Предыдущая страница ">Предыдущая</a>&nbsp;&nbsp;';
        }
        for ($n = 1; $n <= $products_total_pages; $n++) {
            if (intval($_GET['page'] == $n)) {
                $navigation_str_pagination .= '&nbsp;<b>' . $n . '</b>&nbsp;&nbsp;';
            } else {
                $navigation_str_pagination .= '<a href="' . createUrlWithPage($n) . '" class="pageResults" title="Страница ' . $n . '">' . $n . '</a>&nbsp;';
            }
        }
        if ($_GET['page'] < $products_total_pages) {
            $navigation_str_pagination .= '<a href="' . createUrlWithPage(intval($_GET['page']) + 1) . '" class="pageResults" title=" Следующая страница ">Следующая</a>&nbsp;';
        }
        $navigation_str_pagination .= '</span>'; 
    }

    $navigation_str = '
        <div class="navigation">
            ' . $navigation_str_pagination . '
            Показано <span class="bold">' . ($offset_num + 1) . '</span> - <span class="bold">' . (count($products) + $offset_num) . '</span> 
            (всего позиций: <span class="bold">' . $products_total_count . '</span>)
        </div>
    ';

    function createUrlWithOnPage($on_page = 1) {
        $query = $_GET;
        // replace parameter(s)
        $query['on_page'] = $on_page;
        // rebuild url
        $query_result = http_build_query($query);
        // new link
        return '/advanced_search_result' . SITE_URLS_SUFFIX . '?' . $query_result;
    }

    function createUrlWithSortAndDirection($sort, $direction) {
        $query = $_GET;
        // replace parameter(s)
        $query['sort'] = $sort;
        $query['direction'] = $direction;
        // rebuild url
        $query_result = http_build_query($query);
        // new link
        return '/advanced_search_result' . SITE_URLS_SUFFIX . '?' . $query_result;
    }

?>
    <div class="page">
        <div class="pageItem">
            <p> Сортировка: 
                <a href="<?= createUrlWithSortAndDirection('title', 'asc'); ?>">название ▲</a>
                <a href="<?= createUrlWithSortAndDirection('title', 'desc'); ?>">▼</a> | 
                <a href="<?= createUrlWithSortAndDirection('price', 'asc'); ?>">цена ▲</a>
                <a href="<?= createUrlWithSortAndDirection('title', 'desc'); ?>">▼</a>
            </p>
            <p> Товаров на странице: <a href="<?= createUrlWithOnPage(10); ?>">10</a>&nbsp;<a href="<?= createUrlWithOnPage(20); ?>">20</a>&nbsp;<a href="<?= createUrlWithOnPage(50); ?>">50</a>&nbsp;</p>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
    <?= $navigation_str; ?>
    <div class="clear"></div>
    <div class="row-fluid shop-products">
        <ul class="thumbnails">
            <? foreach ($products as $product): ?>
                <? 
                    $i++;
                    $button_cart_title = (isset($cart['items'][$product['id']])) ? 'Убрать' : 'В корзину';
                    $button_cart_class = (isset($cart['items'][$product['id']])) ? 'btn-remove-from-cart-js' : 'btn-add-to-cart-js';
                ?>

                <li class="item span3<?= ($i % 3 == 0 || $i == 1) ? ' first' : ''; ?>">
                    <div class="form-inline justify-content-center" >
                        <div class="thumbnail text-center"">
                            <a href="/<?= $product['alias']; ?>" class="image">
                                <img src="<?= checkImageExists($product['image']); ?>" alt="<?= $product['title']; ?>">
                                <span class="frame-overlay"></span>
                                <span class="price"><?= generatePriceString($product['price'], $current_rate, $current_symbol); ?></span>
                            </a>
                            <div class="inner notop nobottom text-center">
                                <h4 class="title"><a href="/<?= $product['alias']; ?>"><?= $product['title']; ?></a></h4>
                            </div>
                        </div>
                        <div class="inner darken notop text-center">
                            <button class="btn btn-add-to-cart <?= $button_cart_class; ?>" data-id="<?= $product['id']; ?>">
                                <i class="fa fa-shopping-cart"></i> <?= $button_cart_title; ?>
                            </button>
                        </div>
                    </div>
                </li>

            <? endforeach; ?>
        </ul>
    </div>
    <div class="clear"></div>
        <?= $navigation_str; ?>
    <div class="clear"></div>

<? else: ?>

    <h1>Товар не найден!</h1>
    <form name="new_find" id="new_find" action="/advanced_search_result<?= SITE_URLS_SUFFIX; ?>" method="get">
        <div class="page">
            <div class="pagecontent">
                Не найдено товаров, соответствующих Вашему запросу.<br><span class="bold">Воспользуйтесь поиском!</span><br><br>
                <fieldset class="form">
                    <legend>Ключевые слова:</legend>
                    <p><input type="text" name="keywords" size="15" maxlength="30"></p>
                    <p>
                        <span class="button">
                            <button type="submit">
                                <img src="/web/images/icons/search.png" alt="Поиск" title=" Поиск " width="12" height="12">&nbsp;Поиск
                            </button>
                        </span>
                    </p>
                </fieldset>
            </div>
        </div>
        <div class="pagecontentfooter">
            <a class="button" href="javascript:history.back(1)">
                <span><img src="/web/images/icons/back.png" alt="Назад" title=" Назад " width="12" height="12">&nbsp;Назад</span>
            </a>
        </div>
    </form>

<? endif; ?>