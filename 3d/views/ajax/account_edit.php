<?

    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['type'])) {

        if (isset($user['id']) && intval($user['id'])) {

            $errors = [];
            $success = false;

            switch ($_POST['type']) {
                case 'personal':
                    $current_password_res = $mysqli->query(
                        "SELECT `password` FROM " . DB_TABLE_USERS . " WHERE id = " . $user['id']
                    );
            
                    if ($current_password = $current_password_res->fetch_assoc()) {
                        if (password_verify($_POST['password'], $current_password['password'])) {
                            $first_name = htmlspecialchars($_POST['first_name']);
                            $last_name = ''; //htmlspecialchars($_POST['last_name']);
                            $sql = "UPDATE " . DB_TABLE_USERS . " SET `first_name` = '" . $first_name .  "', `last_name` = '" . $last_name . "', `hash_password_reset` = '" . generateHashString() . "' WHERE id = " . $user['id'];
                            if ($mysqli->query($sql)) {
                                $success = '/account_edit' . SITE_URLS_SUFFIX;
                            } else {
                                $errors[] = 'Ошибка при обновлении данных. Обновите страницу или повторите попытку позже.';
                            }
                        } else {
                            $errors[] = 'Текущий пароль неверен.';
                        }
                    } else {
                        $errors[] = 'Ошибка при проверке пароля. Обновите страницу или повторите попытку позже.';
                    }

                    echo json_encode(['success' => $success, 'errors' => $errors]);
                    break;

                case 'password':
                    $current_password_res = $mysqli->query(
                        "SELECT `password` FROM " . DB_TABLE_USERS . " WHERE id = " . $user['id']
                    );
                    
                    if ($current_password = $current_password_res->fetch_assoc()) {
                        if (!password_verify($_POST['password'], $current_password['password'])) {
                            $errors[] = 'Текущий пароль неверен.';
                        }
                        
                        if ($_POST['new_password'] !== $_POST['confirmation_new_password']) {
                            $errors[] = 'Неверное подтверждение нового пароля.';
                        }
            
                        if (
                            password_verify($_POST['password'], $current_password['password']) &&
                            $_POST['new_password'] === $_POST['confirmation_new_password']
                        ) {
                            $user_hash = generateHashString();
                            $password = password_hash($_POST['new_password'], PASSWORD_DEFAULT);
                            $sql = "UPDATE " . DB_TABLE_USERS . " SET `password` = '" . $password .  "', `hash` = '" . $user_hash . "', `hash_password_reset` = '" . generateHashString() . "' WHERE id = " . $user['id'];
                            if ($mysqli->query($sql)) {
                                $success = '/login' . SITE_URLS_SUFFIX;
                            } else {
                                $errors[] = 'Ошибка при обновлении пароля. Обновите страницу или повторите попытку позже.';
                            }
                        }
                    }

                    echo json_encode(['success' => $success, 'errors' => $errors]);
                    break;
                
                default:
                    # code...
                    break;
            }

        }

    }