<?
    define('DB_HOST', '127.0.0.1');
    define('DB_LOGIN', '');
    define('DB_PASSWORD', '');
    define('DB_TABLE', '');

    define('DB_TABLE_PRODUCTS', 'products');
    define('DB_TABLE_CATEGORIES', 'categories');
    define('DB_TABLE_ALIASES', 'aliases');
    define('DB_TABLE_ORDERS', 'orders');
    define('DB_TABLE_ORDERS_PRODUCTS', 'orders_products');
    define('DB_TABLE_USERS', 'users');
    define('DB_TABLE_PAGES_EXTRA', 'pages_extra');
    define('DB_TABLE_CURRENCIES', 'currencies');
    define('DB_TABLE_PRODUCTS_RELATED', 'products_related');

    $mysqli = new mysqli(DB_HOST, DB_LOGIN, DB_PASSWORD, DB_TABLE);

    if ($mysqli->connect_errno) {
        echo 'При подключении к базе данных произошла ошибка!';
    }