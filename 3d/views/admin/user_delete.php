<? if ($user['id'] === $_GET['id']): ?>
    <p>Нельзя удалить самого себя. Войдите под другим пользователем.</p>
<? else: ?>

    <?

    $sql = "SELECT * FROM " . DB_TABLE_USERS . " WHERE id = " . $_GET['id'];

    if ($result = $mysqli->query($sql)): 
        if ($user_db = $result->fetch_assoc()):
            $sql_user = "DELETE FROM " . DB_TABLE_USERS . " WHERE id = " . $_GET['id'];
            if ($mysqli->query($sql_user)): 
    ?>
                <p>Пользователь успешно удален.</p>
            <? else: ?>
                <p>При удалении пользователя произошла ошибка.</p>
            <? endif; ?>
        <? else: ?>
            <p>Пользователь не найден.</p>
        <? endif; ?>
    <? else: ?>
        <p>Ошибка при удалении: некорректный запрос к базе данных.</p>
    <? endif; ?>

<? endif; ?>
<p><a href="/admin/users<?= SITE_URLS_SUFFIX; ?>">Вернуться назад</a>.</p>