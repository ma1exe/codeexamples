<?

    $sql = "SELECT * from " . DB_TABLE_PRODUCTS . " ORDER BY `title`";

    if ($result = $mysqli->query($sql)): 
?>

<div class="row">
    <div class="col-12">
        <p class="text-right"><a href="/admin/product_new<?= SITE_URLS_SUFFIX; ?>">Добавить</a></p>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
            <th scope="col">Редактирование</th>
            <th scope="col">Копирование</th>
            <th scope="col">Удаление</th>
        </tr>
    </thead>
    <tbody>
        <? while ($product = $result->fetch_assoc()): ?>
            <tr>
                <th scope="row"><?= $product['title']; ?></th>
                <td><?= $product['price']; ?></td>
                <td><a href="/admin/product_edit<?= SITE_URLS_SUFFIX; ?>?id=<?= $product['id']; ?>">редактировать</a></td>
                <td><a href="/admin/product_copy<?= SITE_URLS_SUFFIX; ?>?id=<?= $product['id']; ?>">копировать</a></td>
                <td><a href="/admin/product_delete<?= SITE_URLS_SUFFIX; ?>?id=<?= $product['id']; ?>">удалить</a></td>
            </tr>
    <? endwhile; ?>
        </tbody>
    </table>
<? endif; ?>