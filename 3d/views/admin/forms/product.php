<? 
    $categories = [];
    $products = [];
    $products_related = [];

    $sql = "SELECT * from " . DB_TABLE_CATEGORIES;

    $result_categories = $mysqli->query($sql);
    while ($category = $result_categories->fetch_assoc()) {
        $categories[] = $category;
    }

    if ($result_product) {    
        $sql = "SELECT * from " . DB_TABLE_PRODUCTS_RELATED . " WHERE `product_id` = " . $result_product['id'];
    
        $result_products = $mysqli->query($sql);
        while ($product = $result_products->fetch_assoc()) {
            $products_related[] = $product['related_id'];
        }

        $sql = "SELECT * from " . DB_TABLE_PRODUCTS . " WHERE `id` <> " . $result_product['id'] . " ORDER BY `title`";
    } else {
        $sql = "SELECT * from " . DB_TABLE_PRODUCTS . " ORDER BY `title`";
    }

    $result_products = $mysqli->query($sql);
    while ($product = $result_products->fetch_assoc()) {
        $products[] = $product;
    }
?>

<form action="" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Название товара</label>
        <input name="title" type="text" class="form-control" id="title" placeholder="" 
            value="<?= ($result_product) ? $result_product['title'] : ''; ?>" 
        required>
    </div>
    <div class="form-group">
        <label for="description">Описание</label>
        <textarea name="description" class="form-control tinymce" id="description" rows="3"><?= ($result_product) ? $result_product['description'] : ''; ?></textarea>
    </div>
    <? if ($result_product['image']): ?>
        <div class="form-group">
            <label for="image_info">Изображение товара</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">URL</span>
                </div>
                <input type="text" id="image_info" class="form-control" value="<?= $result_product['image']; ?>" disabled>
                <div class="input-group-append">
                    <a class="btn btn-outline-secondary" type="button" href="<?= $result_product['image']; ?>" target="_blank">Посмотреть</a>
                    <a class="btn btn-outline-secondary" type="button" href="/admin/product_image_delete<?= SITE_URLS_SUFFIX; ?>?id=<?= $_GET['id']; ?>">Удалить</a>
                </div>
            </div>
        </div>
    <? else: ?>
        <div class="form-group">
            <label for="image">Изображение товара (допустимые расширения: <?= $allowed_image_extensions_str; ?>)</label>
            <div class="input-group mb-3">
                <div class="custom-file">
                    <input name="image" type="file" class="custom-file-input" id="image" accept="<?= $allowed_image_extensions_str; ?>">
                    <label id="image_label" class="custom-file-label" for="image">Выберите файл</label>
                </div>
            </div>
        </div>
    <? endif; ?>
    <? if ($result_product['images_extra']): ?>
        <? 
            $images_extra = explode(',', $result_product['images_extra']);
            foreach ($images_extra as $extra_img_src):
        ?>
                <div class="form-group">
                    <label for="image_info">Дополнительное изображение</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">URL</span>
                        </div>
                        <input type="text" class="form-control" value="<?= $extra_img_src; ?>" disabled>
                        <div class="input-group-append">
                            <a class="btn btn-outline-secondary" type="button" href="<?= $extra_img_src; ?>" target="_blank">Посмотреть</a>
                        </div>
                    </div>
                </div>
        <? endforeach; ?>
        <p><a href="/admin/product_images_extra_clear<?= SITE_URLS_SUFFIX; ?>?id=<?= $_GET['id']; ?>">Удалить все доп. изображения</a></p>
    <? else: ?>
        <div class="form-group">
            <label for="images_extra">Дополнительные изображения (допустимые расширения: <?= $allowed_image_extensions_str; ?>)</label>
            <div class="input-group mb-3">
                <div class="custom-file">
                    <input name="images_extra[]" type="file" class="custom-file-input" id="images_extra" accept="<?= $allowed_image_extensions_str; ?>" multiple>
                    <label id="images_extra_label" class="custom-file-label" for="image">Выберите файлы</label>
                </div>
            </div>
        </div>
    <? endif; ?>
    <div class="form-group">
        <label for="category_id">Категория</label>
        <select name="category_id" id="category_id" class="form-control">
            <? foreach ($categories as $category): ?>

                <option value="<?= $category['id']; ?>"<?= (($result_product) && $result_product['category_id'] == $category['id']) ? ' selected' : ''; ?>><?= $category['title']; ?></option>

            <? endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="price">Цена</label>
        <input name="price" type="number" class="form-control" id="price" placeholder="" value="<?= ($result_product) ? $result_product['price'] : '0'; ?>" required>
    </div>
    <div class="form-group">
        <label for="alias">Алиас (URL; разрешены английские буквы, цифры, символы ".", "-" и "_")</label>
        <input name="alias" type="text" class="form-control" id="alias" placeholder="" 
        value="<?= ($result_product) ? $result_product['alias'] : ''; ?>"
        required>
    </div>
    <div class="form-group">
        <label for="meta_title">Название страницы</label>
        <input name="meta_title" type="text" class="form-control" id="meta_title" placeholder="" 
        value="<?= ($result_product) ? $result_product['meta_title'] : ''; ?>"
        >
    </div>
    <div class="form-group">
        <label for="meta_description">Описание страницы</label>
        <input name="meta_description" type="text" class="form-control" id="meta_description" placeholder="" 
        value="<?= ($result_product) ? $result_product['meta_description'] : ''; ?>"
        >
    </div>
    <div class="form-group">
        <label for="meta_keywords">Ключевые слова страницы</label>
        <input name="meta_keywords" type="text" class="form-control" id="meta_keywords" placeholder="" 
        value="<?= ($result_product) ? $result_product['meta_keywords'] : ''; ?>"
        >
    </div>
    <div class="form-group">
        <label for="products_related">Рекомендуемые товары (для выбора нескольких зажать ctrl)</label>
        <select name="products_related[]" id="products_related" class="form-control" multiple>
            <? foreach ($products as $product): ?>

                <option value="<?= $product['id']; ?>" <?= (($result_product) && in_array($product['id'], $products_related)) ? ' selected="selected"' : ''; ?> ><?= $product['title']; ?></option>

            <? endforeach; ?>
        </select>
    </div>
    <button name="submit" type="submit" class="btn btn-primary"><?= ($result_product) ? 'Обновить': 'Добавить'; ?></button>
</form>