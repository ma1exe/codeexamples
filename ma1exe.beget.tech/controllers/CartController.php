<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use app\models\CatalogProducts;
use app\models\CatalogCategories;
use app\models\CatalogCurrencies;
use app\models\CatalogManufacturers;
use yii\data\Pagination;

class CartController extends GlobalController
{
    public function actionIndex($category_link = false) 
    {
        $categories_query = CatalogCategories::find()->asArray()->all();
        $categories = [];
        foreach ($categories_query as $category) {
            $categories[$category['id']] = [
                'id' => $category['id'],
                'name' => $category['name'],
                'link' => $category['link']
            ];
        }

        $currencies_query = CatalogCurrencies::find()->asArray()->all();
        $currencies = [];
        foreach ($currencies_query as $currency) {
            $currencies[$currency['id']] = [
                'symbol' => $currency['symbol'],
                'rate' => $currency['rate']
            ];
        }

        $manufacturers = CatalogManufacturers::find()->asArray()->all();

        //$this->registerCssFile("/path/to/your/file/in/web/folder/style.css");
        return $this->render('index', compact('products', 'categories', 'currencies', 'manufacturers'));
    }

    public function actionCheckout() 
    {
        return $this->render('checkout');
    }

    public function actionAdditem() {
        if (Yii::$app->request->get('id')) {
            $product_id = Yii::$app->request->get('id');
            $product = CatalogProducts::findOne($product_id);
            if ($product) {
                Yii::$app->session->open();
                if (!Yii::$app->session->has('cart')) {
                    Yii::$app->session->set('cart', []);
                }
                $cart = Yii::$app->session->get('cart');
                if (isset($cart['products'][$product_id]) && intval($cart['products'][$product_id])) {
                    $cart['products'][$product_id] += 1;
                } else {
                    $cart['products'][$product_id] = 1;
                }
                Yii::$app->session->set('cart', $cart);

                return false;
            }
        }

        throw new \yii\web\NotFoundHttpException();
    }


    public function actionClearitems() {
        Yii::$app->session->open();
        if (Yii::$app->session->has('cart')) {
            $cart = Yii::$app->session->get('cart');
            if (isset($cart['products'])) {
                $cart['products'] = [];
                Yii::$app->session->set('cart', $cart);
            }
        }

        return false;
    }


    public function actionProduct($category_link = null, $product_id = null) 
    {
        $category = CatalogCategories::find()
            ->where(['link' => $category_link])
            ->one();

        if (!$category) throw new \yii\web\NotFoundHttpException();

        $product = CatalogProducts::find()
            ->where(['id' => $product_id, 'category_id' => $category['id']])
            ->one();

        if (!$product) throw new \yii\web\NotFoundHttpException();

        $currencies_query = CatalogCurrencies::find()->asArray()->all();
        $currencies = [];
        foreach ($currencies_query as $currency) {
            $currencies[$currency['id']] = [
                'symbol' => $currency['symbol'],
                'rate' => $currency['rate']
            ];
        }

        return $this->render('product', compact('product', 'category', 'currencies'));
    }
}