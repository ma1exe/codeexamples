<?
    $result_category = false;

    if (isset($_GET['id']) && intval($_GET['id'])) {
        $result = $mysqli->query('
            SELECT
                c.id,
                c.title,
                c.description,
                a.alias,
                c.alias_id,
                a.meta_title,
                a.meta_description,
                a.meta_keywords
            FROM
                ' . DB_TABLE_CATEGORIES . ' c
            LEFT OUTER JOIN ' . DB_TABLE_ALIASES . ' a ON
                a.id = c.alias_id
            WHERE
                c.id = ' . $_GET['id']
        );
        if ($result && $result->num_rows) {
            $result_category = $result->fetch_assoc();
            if (getFlash()) {
                echo getFlash();
                clearFlash();
            }
        } else {
            echo '<p>Категория не найдена.</p>';
        }
    } else {
        echo '<p>Категория не найдена.</p>';
    }

    if ($result_category) {
        
        if (isset($_POST['submit'])) {
            // Если нет ошибок
            if (!empty($_POST['title']) && !empty($_POST['alias']) && intval($_GET['id'])) {
    
                $form = $_POST;

                $meta_title = htmlspecialchars($form['meta_title']);
                $meta_description = htmlspecialchars($form['meta_description']);
                $meta_keywords = htmlspecialchars($form['meta_keywords']);

                unset($form['submit'], $form['meta_title'], $form['meta_description'], $form['meta_keywords']);
    
                $new_alias = makeAlias($form['alias']);
    
                $sql = "UPDATE " . DB_TABLE_ALIASES . " SET alias = '" . $new_alias . "', `meta_title` = '" . $meta_title . "', `meta_description` = '" . $meta_description . "', `meta_keywords` = '" . $meta_keywords . "' WHERE id = " . $result_category['alias_id'];
    
                if ($mysqli->query($sql)) {

                    unset($form['alias'], $new_alias);
    
                    $sql = makeSqlUpdate(DB_TABLE_CATEGORIES, $form, intval($_GET['id']));
    
                    if ($result = $mysqli->query($sql)) {
                        $result = $mysqli->query('
                            SELECT
                                c.id,
                                c.title,
                                c.description,
                                a.alias,
                                c.alias_id
                            FROM
                                ' . DB_TABLE_CATEGORIES . ' c
                            LEFT OUTER JOIN ' . DB_TABLE_ALIASES . ' a ON
                                a.id = c.alias_id
                            WHERE
                                c.id = ' . $_GET['id']
                        );
                        $result_category = $result->fetch_assoc();
                        addFlash('<p>Категория обновлена!</p>'); 
                        $is_reload_required = true;
                    } else {
                        addFlash('<p>Категория не обновлен: произошла ошибка при работе с базой данных.</p>');
                        addFlash('<p>' . $mysqli->error . '</p>');
                    }
                } else {
                    addFlash('<p>Категория не обновлена: ошибка при обновлении алиаса и meta-тегов.</p>');
                }
                
            } else {
                addFlash('<p>При обновлении категории произошли следующие ошибки:<ul>');
    
                if (empty($_POST['title'])) {
                    addFlash('<li>Название категории отсутствует.</li>');
                }
    
                if (empty($_POST['alias'])) {
                    addFlash('<li>Алиас не может быть пустым.</li>');
                }
    
                addFlash('</ul></p>');
            }
        } 

        if (getFlash() && !$is_reload_required) {
            echo getFlash();
            clearFlash();
        }

        require('./views/admin/forms/category.php');
    }