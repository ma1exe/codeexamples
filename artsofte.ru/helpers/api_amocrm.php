<?

    class AmoCRM {
        private $log;

        private $subdomain;
        private $link;

        private $client_id;
        private $client_secret;
        private $redirect_uri;

        private $token_file;
        private $users_file;

        private $token_type;
        private $token_expires_at;
        private $access_token;
        private $refresh_token;

        function __construct() {
            $this->log = new Logger();
            $this->log->lfile(AMOCRM_LOGS_FOLDER . @date('d_m_Y') . '_api.txt');

            $this->subdomain = 'artsoftedigital';
            $this->link = 'https://' . $this->subdomain . '.amocrm.ru';

            $this->client_id = '';
            $this->client_secret = '';
            $this->redirect_uri = '';

            $this->token_file = AMOFIX_FOLDER . 'helpers/token_info_amocrm.json';
            $this->users_file = AMOFIX_FOLDER . 'helpers/users_info_amocrm.json';

            $this->iv = '2565428542332002';

            $this->getTokenFromFile();

            if (time() > $this->token_expires_at) {
                $this->refreshToken();
            }

        }

        function __destruct() {
            $this->log->lclose();
        }

        private function refreshToken() {
            $data = [
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
                'grant_type' => 'refresh_token',
                'refresh_token' => $this->refresh_token,
                'redirect_uri' => $this->redirect_uri,
            ];

            $headers = ['Content-Type:application/json'];

            $url = '/oauth2/access_token';

            $result = $this->request($url, $headers, $data);
            //$result['response'] = json_decode(json_encode($result['response']), true);
            
            if ($result['success'] && $this->saveTokenToFile(json_decode($result['response'], true))) {
                $this->log->lwrite("Успешно обновили и сохранили токен в файл $this->token_file");
                return true;
            }

            $this->log->lwrite("Не удалось обновить токен и сохранить его в файл $this->token_file");

            return false;
        
        }

        private function saveTokenToFile($accessToken) {

            //echo '<pre>savetofile '; print_r($accessToken); echo '</pre>';

            if (
                isset($accessToken)
                && isset($accessToken['token_type'])
                && isset($accessToken['expires_in'])
                && isset($accessToken['access_token'])
                && isset($accessToken['refresh_token'])
            ) {
                $data = [
                    'token_type' => $accessToken['token_type'],
                    'expires_at' => intval(time()) + intval($accessToken['expires_in']),
                    'access_token' => $accessToken['access_token'],
                    'refresh_token' => $accessToken['refresh_token'],
                ];
        
                if (file_put_contents($this->token_file, json_encode($data))) {
                    $this->token_type = $data['token_type'];
                    $this->token_expires_at = $data['expires_at'];
                    $this->access_token = $data['access_token'];
                    $this->refresh_token = $data['refresh_token'];

                    return true;
                }
            }

            $this->clearToken();

            return false;

        }

        private function getTokenFromFile() {
            if (!file_exists($this->token_file)) {
                $this->clearToken();
                return false;
            }
        
            $accessToken = json_decode(file_get_contents($this->token_file), true);
        
            if (
                isset($accessToken)
                && isset($accessToken['token_type'])
                && isset($accessToken['expires_at'])
                && isset($accessToken['access_token'])
                && isset($accessToken['refresh_token'])
            ) {

                // echo '<pre>'; print_r($accessToken); echo '<pre>'; 

                $this->token_type = $accessToken['token_type'];
                $this->token_expires_at = $accessToken['expires_at'];
                $this->access_token = $accessToken['access_token'];
                $this->refresh_token = $accessToken['refresh_token'];
                
                return true;
            }

            $this->clearToken();

            return false;
        }

        private function clearToken() {
            $this->token_type = false;
            $this->token_expires_at = false;
            $this->access_token = false;
            $this->refresh_token = false;
        }

        public function getUser($id = 0) {
            if (!file_exists($this->users_file) || intval($id) === 0) {
                return false;
            }

            $users_list = json_decode(file_get_contents($this->users_file), true);

            if (isset($users_list[$id]) && !empty($users_list[$id])) {
                return openssl_decrypt($users_list[$id], "AES-128-CTR", $this->client_secret, 0, $this->iv);
            }

            $user = $this->request("/api/v4/users/$id");
            if ($user['success']) {
                $user_info = json_decode($user['response'], true);
                $users_list[$id] = openssl_encrypt($user_info['name'], "AES-128-CTR", $this->client_secret, 0, $this->iv);
                file_put_contents($this->users_file, json_encode($users_list));

                return $user_info['name'];
            }

            return false;
        }

        public function request($url, $headers = false, $data = false) {
            $this->log->lwrite("Вызвана функция request() с параметрами url, headers, data");
            $this->log->lwrite($this->link . $url);
            if (!$headers) {
                $this->log->lwrite('параметр headers пуст, ставим свои заголовки');
                $headers = ['Authorization: Bearer ' . $this->access_token];
            }
            $this->log->lwrite(json_encode($headers, true));
            if ($data) {
                $this->log->lwrite(json_encode($data, true));
            } else {
                $this->log->lwrite('параметр data пуст');
            }

            /**
             * Нам необходимо инициировать запрос к серверу.
             * Воспользуемся библиотекой cURL (поставляется в составе PHP).
             * Вы также можете использовать и кроссплатформенную программу cURL, если вы не программируете на PHP.
             */
            $curl = curl_init(); //Сохраняем дескриптор сеанса cURL
            /** Устанавливаем необходимые опции для сеанса cURL  */
            curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-oAuth-client/1.0');
            curl_setopt($curl,CURLOPT_URL, $this->link . $url);
            curl_setopt($curl,CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl,CURLOPT_HEADER, false);
            if ($data) {
                curl_setopt($curl,CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode($data));
            }
            curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 2);
            $out = curl_exec($curl); //Инициируем запрос к API и сохраняем ответ в переменную
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            /** Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом. */
            $code = (int)$code;
            $errors = [
                400 => 'Bad request',
                401 => 'Unauthorized',
                403 => 'Forbidden',
                404 => 'Not found',
                500 => 'Internal server error',
                502 => 'Bad gateway',
                503 => 'Service unavailable',
            ];

            $this->log->lwrite("Функция request() завершила работу с кодом ответа $code");
            $this->log->lwrite(json_encode($out));

            if(curl_errno($curl)){   
                $this->log->lwrite('Curl error: ' . curl_error($curl));
            }

            /** Если код ответа не успешный - возвращаем сообщение об ошибке  */
            if ($code < 200 || $code > 204) {
                return ['success' => false, 'errors' => true, 'response' => $out];
            }
            
            return ['success' => true, 'errors' => false, 'response' => $out];
        }
    }
