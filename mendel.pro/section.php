<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;

$this->setFrameMode(true);

$watermark_blank = [
	[
		'name' => 'watermark',
		'position' => 'center',
		'size' => 'real',
		'fill' => 'exact',
		'file' => $_SERVER['DOCUMENT_ROOT'] . '/upload/watermarks/watermark_blank.png',
	]
];


if ( !$_SESSION["PROJECT"]["SECTION_TEMPLATE"] )
	$_SESSION["PROJECT"]["SECTION_TEMPLATE"] = 'tile';

if ( $_GET["view"] && in_array($_GET["view"], array("list", "tile")) )
	$_SESSION["PROJECT"]["SECTION_TEMPLATE"] = $_GET["view"];
?>


<?
if (!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '')
    $arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');

$isVerticalFilter = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");
$isSidebar = ($arParams["SIDEBAR_SECTION_SHOW"] == "Y" && isset($arParams["SIDEBAR_PATH"]) && !empty($arParams["SIDEBAR_PATH"]));
$isFilter = ($arParams['USE_FILTER'] == 'Y');

if ($isFilter)
{
    $arFilter = array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ACTIVE" => "Y",
        "GLOBAL_ACTIVE" => "Y",
    );
    if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
        $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
    elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
        $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

    $obCache = new CPHPCache();
    if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
    {
        $arCurSection = $obCache->GetVars();
    }
    elseif ($obCache->StartDataCache())
    {
        $arCurSection = array();
        if ( Loader::includeModule("iblock") )
        {
            $dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

            if(defined("BX_COMP_MANAGED_CACHE"))
            {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache("/iblock/catalog");

                if ($arCurSection = $dbRes->Fetch())
                    $CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);

                $CACHE_MANAGER->EndTagCache();
            }
            else
            {
                if(!$arCurSection = $dbRes->Fetch())
                    $arCurSection = array();
            }
        }
        $obCache->EndDataCache($arCurSection);
    }
    if (!isset($arCurSection))
        $arCurSection = array();
}

if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
    $basketAction = isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '';
else
    $basketAction = isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '';
?>

<?
$arSectData = \PDV\Tools::getSectionData($arParams['IBLOCK_ID'], $arResult['VARIABLES']['SECTION_CODE']);

$arproductType = [];
$rsElem = \CIBlockElement::GetList(
    ['sort' => 'asc', 'id' => 'desc'],
    ['IBLOCK_ID' => 15, '=CODE' => $arResult["VARIABLES"]["SECTION_CODE"]],
    false,
    false,
    ['ID', 'NAME', 'PREVIEW_TEXT']
);
if ( $arElem = $rsElem->GetNext() ) {
    $arproductType = $arElem;
    $isFilter = false;
}

if ( $arSectData['DEPTH_LEVEL'] == 1 ):
    $APPLICATION->SetPageProperty("SECTION_ATTR_ID", "category");
?>
    <div class="container">
        <?$APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "",
            array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
                "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
                "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
				"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
                "SECTION_USER_FIELDS" => array("UF_PIC")
            ),
            $component,
            array("HIDE_ICONS" => "Y")
        );
        ?>
        <div class="text-content"><?=$arSectData['DESCRIPTION']?></div>
    </div>
<?
else:
    $arSeoFilterData = \PDV\Tools::getSmartFilterSeo($arSectData['ID'], $arParams["IBLOCK_ID"]);
    $APPLICATION->SetPageProperty("SECTION_ATTR_ID", "catalog");

    $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
    $getSort = htmlspecialchars($request->get('sort'));
    $getCount = htmlspecialchars($request->get('count'));

    $arrSort = [
        'price' => [
            'name' => 'Цене',
            'code' => 'PROPERTY_PRICE',
            'items' => [
                'desc' => 'По убыванию',
                'asc' => 'По возрастанию'
            ]
        ],
        'sort' => [
            'name' => 'Популярности',
            'code' => 'sort',
            'items' => [
                'desc' => 'По убыванию',
                'asc' => 'По возрастанию'
            ]
        ]
    ];

    $showAllParam = '';
    foreach ( $request->getQueryList() as $code => $value ) {
        if ( stripos($code, 'SHOWALL_') !== false )
            $showAllParam = $code;
    }

    global $USER;
    if ($USER->IsAdmin()) {
        $arrCount = [
            20 => $APPLICATION->GetCurPageParam('count=20', array('count',$showAllParam)),
            30 => $APPLICATION->GetCurPageParam('count=30', array('count',$showAllParam)),
            60 => $APPLICATION->GetCurPageParam('count=60', array('count',$showAllParam)),
        ];
    } else {
        $arrCount = [
            30 => $APPLICATION->GetCurPageParam('count=30', array('count',$showAllParam)),
            60 => $APPLICATION->GetCurPageParam('count=60', array('count',$showAllParam)),
            'Все' => ''
        ];
    }

    if ( !empty($getSort) ) {
        if (in_array($getSort, ['sale', 'hit'])) {
            $arParams["ELEMENT_SORT_FIELD"] = "propertysort_WOBBLERS";
            $arParams["ELEMENT_SORT_ORDER"] = "DESC";

            /*$res = (CIBlockProperty::GetByID(93, 16));
            $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>16, "CODE"=>"WOBBLERS"));
            while($enum_fields = $property_enums->GetNext())
            {
            echo $enum_fields["ID"]." - ".$enum_fields["VALUE"]."<br>";
            }
            print_r(CIBlockProperty::GetByID(93, 16)->GetNext());*/
        } else {
            foreach ( $arrSort as $sort => $item ) {
                if ( stripos($getSort, $sort) !== false ) {
                    $arParams["ELEMENT_SORT_FIELD"] = $item['code'];
                    foreach ( $item['items'] as $order => $name ) {
                        if ( stripos($getSort, '_'.$order) !== false )
                            $arParams["ELEMENT_SORT_ORDER"] = strtoupper($order);
                    }
                }
            }
        }
    }

    if ( !empty($getCount) ) {
        foreach ( $arrCount as $value => $url ) {
            if ( $getCount == $value )
                $arParams["PAGE_ELEMENT_COUNT"] = $value;
        }
    }
    ?>

    <?if ( !empty($arrSort) ):?>
        <div class="popup innerPopupCustom" id="sortPopup">
            <div class="popupContainer">
                <div class="popupContainerTitle">СОРТИРОВАТЬ</div>
                <div class="popupContainerClose"></div>
                <div class="popupContainerBody">
                    <ul class="sort">
                        <?foreach ( $arrSort as $sort => $item ){?>
                            <li><b><?=$item['name']?></b></li>
                            <?foreach ( $item['items'] as $order => $name ){?>
                                <li><a href="<?=$APPLICATION->GetCurPageParam('sort='.$sort.'_'.$order, array('sort'))?>"><?=$name?></a></li>
                            <? } ?>
                        <? } ?>
                        <?if ($USER->IsAdmin()): ?>
                            <li><b>Прочее</b></li>
                            <li<? if ($getSort == 'sale') { echo ' class="active"'; } ?>>
                                <a href="<?= $APPLICATION->GetCurPageParam('sort=sale', array('sort')); ?>">Распродажа</a>
                            </li>
                            <li<? if ($getSort == 'hit') { echo ' class="active"'; } ?>>
                                <a href="<?= $APPLICATION->GetCurPageParam('sort=hit', array('sort')); ?>">Хит</a>
                            </li>
                        <? endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    <?endif;?>

    <div class="container">

        <h1><?php $APPLICATION->ShowTitle(false)?></h1>

        <?if ( !empty($arSectData['DETAIL_PICTURE']) ):?>
            <?if(!empty($arSectData['UF_BANNER_LINK'])){?><a href="<?=$arSectData['UF_BANNER_LINK']?>"><?}?>

			<?
			$resizeImg = CFile::ResizeImageGet($arSectData['DETAIL_PICTURE'], array('width'=>1600, 'height'=>1600),
				BX_RESIZE_IMAGE_PROPORTIONAL, true, $watermark_blank);
			$sectionImgSrc = $resizeImg['src'];
			?>

                <img src="/images/placeholder.gif" data-src="<?=$sectionImgSrc?>" alt="<?=$arSectData['NAME']?>" class="d-none d-lg-block lzld" />
            <?if(!empty($arSectData['UF_BANNER_LINK'])){?></a><?}?>
        <?endif;?>

        <?if ( !empty($arproductType['PREVIEW_TEXT']) ):?>
            <div><?=$arproductType['PREVIEW_TEXT']?></div>
        <?endif;?>

        <?
        if ( $arSectData['ELEMENT_CNT'] == 0 && empty($arproductType) ) {
            $APPLICATION->SetTitle($arSectData['NAME']);
        ?>
            <div class="row"><div class="col-lg-12" style="margin-top: 30px">В данном разделе товаров нет</div></div>
        <?
        }
        else {
            ?>

            <?
            include('section_top.php') ?>

            <div class="row">
                <?
                if ($isFilter) : ?>
                    <div class="catalogFilters col-lg-3 d-xl-block">
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:catalog.smart.filter",
                            "",
                            array(
                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "SECTION_ID" => $arCurSection['ID'],
                                "FILTER_NAME" => $arParams["FILTER_NAME"],
                                "PRICE_CODE" => $arParams["FILTER_PRICE_CODE"],
                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                                "SAVE_IN_SESSION" => "N",
                                "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                                "XML_EXPORT" => "N",
                                "SECTION_TITLE" => "NAME",
                                "SECTION_DESCRIPTION" => "DESCRIPTION",
                                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                                "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                                "SEF_MODE" => $arParams["SEF_MODE"],
                                "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
                                "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                                "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
                                "POPUP_POSITION" => "right"
                            ),
                            $component,
                            array('HIDE_ICONS' => 'Y')
                        ); ?>

                        <?
                        if (!empty($arSectData['UF_BANNER_LEFT'])): ?>
                            <?
                            if (!empty($arSectData['UF_BANNER_LEFT_LINK'])) { ?><a href="<?= $arSectData['UF_BANNER_LEFT_LINK'] ?>"><?
                            } ?>
                            <img src="/images/placeholder.gif" data-src="<?= CFile::GetPath($arSectData['UF_BANNER_LEFT']) ?>" alt="<?= $arSectData['NAME'] ?>" class="d-none d-xl-block lzld"/>
                            <?
                            if (!empty($arSectData['UF_BANNER_LEFT_LINK'])) { ?></a><?
                            } ?>
                        <?endif; ?>
                    </div>
                <?endif; ?>
                <div class="col-lg-<?= $isFilter ? '9' : '12' ?>">

                    <?
                    if (!empty($arproductType)) {
                        $GLOBALS[$arParams["FILTER_NAME"]]['PROPERTY_PAGES'] = $arproductType['ID'];
                        $arResult["VARIABLES"]["SECTION_ID"] = false;
                        $arResult["VARIABLES"]["SECTION_CODE"] = false;
                    }
                    ?>
                    <?
                    $intSectionID = $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        $_SESSION["PROJECT"]["SECTION_TEMPLATE"],
                        array(
                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                            "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                            "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                            "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                            "PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
                            "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                            "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                            "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                            "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                            "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
                            "BASKET_URL" => $arParams["BASKET_URL"],
                            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                            "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                            "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                            "FILTER_NAME" => $arParams["FILTER_NAME"],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "SET_TITLE" => $arParams["SET_TITLE"],
                            "MESSAGE_404" => $arParams["~MESSAGE_404"],
                            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                            "SHOW_404" => $arParams["SHOW_404"],
                            "FILE_404" => $arParams["FILE_404"],
                            "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                            "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
                            "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                            "PRICE_CODE" => $arParams["~PRICE_CODE"],
                            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                            "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                            "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                            "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                            "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

                            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                            "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                            "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                            "LAZY_LOAD" => $arParams["LAZY_LOAD"],
                            "MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
                            "LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

                            "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                            "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                            "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                            "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

                            "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                            "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                            "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                            "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                            "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                            'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                            'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

                            'LABEL_PROP' => $arParams['LABEL_PROP'],
                            'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                            'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                            'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                            'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
                            'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
                            'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
                            'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
                            'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
                            'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
                            'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
                            'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

                            'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                            'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                            'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                            'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
                            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                            'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                            'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
                            'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
                            'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
                            'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
                            'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
                            'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
                            'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
                            'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
                            'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
                            'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

                            'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
                            'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
                            'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

                            'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
							"ADD_SECTIONS_CHAIN" => $arParams['ADD_SECTIONS_CHAIN'],
                            'ADD_TO_BASKET_ACTION' => $basketAction,
                            'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                            'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
                            'COMPARE_NAME' => $arParams['COMPARE_NAME'],
                            'USE_COMPARE_LIST' => 'Y',
                            'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
                            'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
                            'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                            "SHOW_ALL_WO_SECTION" => "Y"
                        ),
                        $component
                    ); ?>
                </div>
            </div>

            <?
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.products.viewed",
                "catalog",
                Array(
                    "ACTION_VARIABLE" => "action_cpv",
                    "ADDITIONAL_PICT_PROP_16" => "-",
                    "ADDITIONAL_PICT_PROP_17" => "-",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "ADD_TO_BASKET_ACTION" => "ADD",
                    "BASKET_URL" => "/personal/basket.php",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "3600",
                    "CACHE_TYPE" => "A",
                    "CART_PROPERTIES_16" => array("", ""),
                    "CART_PROPERTIES_17" => array("", ""),
                    "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
                    "COMPARE_PATH" => "",
                    "CONVERT_CURRENCY" => "N",
                    "DEPTH" => "2",
                    "DISPLAY_COMPARE" => "Y",
                    "ENLARGE_PRODUCT" => "STRICT",
                    "HIDE_NOT_AVAILABLE" => "L",
                    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                    "IBLOCK_ID" => "16",
                    "IBLOCK_MODE" => "single",
                    "IBLOCK_TYPE" => "1c_catalog",
                    "LABEL_PROP_16" => array(),
                    "LABEL_PROP_POSITION" => "top-left",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_COMPARE" => "Сравнить",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                    "OFFER_TREE_PROPS_17" => array(),
                    "PAGE_ELEMENT_COUNT" => "9",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRICE_CODE" => $arParams['PRICE_CODE'],
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                    "PRODUCT_SUBSCRIPTION" => "Y",
                    "PROPERTY_CODE_16" => array("", ""),
                    "PROPERTY_CODE_17" => array("", ""),
                    "PROPERTY_CODE_MOBILE_16" => array(),
                    "SECTION_CODE" => "",
                    "SECTION_ELEMENT_CODE" => "",
                    "SECTION_ELEMENT_ID" => "",
                    "SECTION_ID" => "",
                    "SHOW_CLOSE_POPUP" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_FROM_SECTION" => "N",
                    "SHOW_MAX_QUANTITY" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "SHOW_SLIDER" => "N",
                    "SLIDER_INTERVAL" => "3000",
                    "SLIDER_PROGRESS" => "N",
                    "TEMPLATE_THEME" => "blue",
                    "USE_ENHANCED_ECOMMERCE" => "N",
                    "USE_PRICE_COUNT" => "N",
                    "USE_PRODUCT_QUANTITY" => "N"
                )
            ); ?>

            <?
        }

        if ( !empty($arSeoFilterData['PREVIEW_TEXT']) )
            $arSectData['DESCRIPTION'] = $arSeoFilterData['PREVIEW_TEXT'];
        ?>
        <?if ( !empty($arSectData['DESCRIPTION']) ):?>
            <div class="text-content catalogTextContent"><?=$arSectData['DESCRIPTION']?></div>
        <?endif;?>
    </div>
    <?
    if ( !empty($arproductType['NAME'])) {
        $APPLICATION->SetTitle($arproductType['NAME']);
        $APPLICATION->SetPageProperty('title', $arproductType['NAME']);
    }
    else{
        $arFilter = array('IBLOCK_ID' => 18,'CODE'=>$arResult["VARIABLES"]["SECTION_CODE"]);
        $rsSections = CIBlockSection::GetList(array(), $arFilter);
        while ($arSection = $rsSections->Fetch())
        {
            $arproductType = $arSection;
        }
        if ( !empty($arproductType['NAME'])) {
            $APPLICATION->SetTitle($arproductType['NAME']);
            $APPLICATION->SetPageProperty('title', $arproductType['NAME']);
        }
        
        if ( !empty($arproductType['DESCRIPTION'])) {
            echo '<div class="container"><div class="text-content catalogTextContent">' . $arproductType['DESCRIPTION'] . '</div></div>';
        }
    }
    if ( !empty($arSeoFilterData['PROPERTY_H1_VALUE']) ) {
        $APPLICATION->SetTitle($arSeoFilterData['PROPERTY_H1_VALUE']);
		$APPLICATION->AddChainItem($arSeoFilterData['PROPERTY_H1_VALUE']);
    }
    if ( !empty($arSeoFilterData['PROPERTY_TITLE_VALUE']) )
        $APPLICATION->SetPageProperty('title', $arSeoFilterData['PROPERTY_TITLE_VALUE']);
    if ( !empty($arSeoFilterData['PROPERTY_KEYWORDS_VALUE']) )
        $APPLICATION->SetPageProperty('keywords', $arSeoFilterData['PROPERTY_KEYWORDS_VALUE']);
    if ( !empty($arSeoFilterData['PROPERTY_DESCRIPTION_VALUE']) )
        $APPLICATION->SetPageProperty('description', $arSeoFilterData['PROPERTY_DESCRIPTION_VALUE']);
if($arSectData['SECTION_PAGE_URL']!='')
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://'. SITE_SERVER_NAME . $arSectData['SECTION_PAGE_URL'] . '" />');

    ?>
<?endif;?>
