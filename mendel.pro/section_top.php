<?

    global $USER;
    if ($USER->IsAdmin()):

        $items_series = [];
        $items_tags = [];

        $arSelectItems = Array("ID", "IBLOCK_ID", "CATALOG_QUANTITY"); //IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilterItems = Array("IBLOCK_ID" => $arSectData['IBLOCK_ID'], "SECTION_ID" => $arSectData['ID'], "INCLUDE_SUBSECTIONS" => "Y", "ACTIVE"=>"Y");
        $resItems = CIBlockElement::GetList(Array(), $arFilterItems, false, Array(), $arSelectItems);
        while($obItems = $resItems->GetNextElement()) { 
            $arFields = $obItems->GetFields();  
			// echo '<pre>'; print_r($arFields); echo '</pre>';

            $arProps = $obItems->GetProperties();

			if (empty($arProps['SERIES']['VALUE_XML_ID'])) {
				continue;
			}

			// echo '<p>'; echo(strlen($arProps['SERIES']['VALUE_XML_ID'])); echo ': '.$arProps['SERIES']['VALUE_XML_ID']; echo '</p>';

            if (isset($items_series[$arProps['SERIES']['VALUE_XML_ID']]["COUNT"])) {
                $items_series[$arProps['SERIES']['VALUE_XML_ID']]["COUNT"]++;
            } else {
                $items_series[$arProps['SERIES']['VALUE_XML_ID']]["COUNT"] = 1;
            }

            
            if ($arProps['PRICE']['VALUE'] > 0) {
                $item_price = $arProps['PRICE']['VALUE'];
            } else {
                $item_price = 0;
            }

            if (
                !isset($items_series[$arProps['SERIES']['VALUE_XML_ID']]["MINIMUM_PRICE"]) ||
                $items_series[$arProps['SERIES']['VALUE_XML_ID']]["MINIMUM_PRICE"] > $item_price
            ) {
                $items_series[$arProps['SERIES']['VALUE_XML_ID']]["MINIMUM_PRICE"] = $item_price;
            }

            if (!isset($items_series[$arProps['SERIES']['VALUE_XML_ID']]["VALUE"])) {
                $items_series[$arProps['SERIES']['VALUE_XML_ID']]["VALUE"] = $arProps['SERIES']['VALUE'];
            }

			// echo '<pre>'; print_r($items_series[$arProps['SERIES']['VALUE_XML_ID']]["VALUE"]); echo '</pre>';

            foreach ($arProps['TAGS']['VALUE'] as $tag) {
                if (!in_array($tag, $items_tags)) {
                    $items_tags[] = $tag;
                }
            }

            
            // echo '<pre>'; print_r($arProps); echo '</pre>';
        }

        $bool_items_categories_allowed = true;

        if (!empty($items_tags)) {
            $arSelect = Array("ID", "NAME");
            $arFilter = Array("IBLOCK_ID" => 36, "ACTIVE" => "Y", "ID" => $items_tags);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
            $items_tags = [];
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();
                if (urldecode(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH)) !== $arSectData['SECTION_PAGE_URL'] . 'filter/tags-is-' . $arFields['NAME'] . '/apply/') {
                    $items_tags[] = strtolower($arFields['NAME']);
                } else {
                    $bool_items_categories_allowed = false;
                }
            }   
        }

        if (!empty($items_series)) {
            $items_series_pictures = [];
            $arSelect = Array("ID", "NAME", "CODE", "PREVIEW_PICTURE");
            $arFilter = Array("IBLOCK_ID" => 37, "ACTIVE" => "Y");
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();
                $items_series_pictures[$arFields["CODE"]] = $arFields["PREVIEW_PICTURE"];
            }   
        }

        // echo '<pre>'; print_r($arSectData); echo '</pre>';
?>

    <? if ($bool_items_categories_allowed && $arSectData['DEPTH_LEVEL'] > 1): ?>
        <div class="row">
            <div class="col-lg-12" id="category" style="margin-top: 25px;">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.section.list",
                    "section",
                    array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "SECTION_ID" => $arSectData['ID'],
                        "SECTION_CODE" => $arSectData["CODE"],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                        "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
                        "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
                        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                        "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
                        "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
                        "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
                        "ADD_SECTIONS_CHAIN" => 'N',
                        "SECTION_USER_FIELDS" => array("UF_PIC")
                    ),
                    $component,
                    array("HIDE_ICONS" => "Y")
                );
                ?>  
            </div>
        </div>

    <? endif; ?>

        <? if (!empty($items_tags)): ?>
            <h2>Популярные категории товаров:</h2>

            <? if (count($items_tags) > 15): ?>
                <div class="catalogTopTags">
                    <? for ($i = 0; $i < 15; $i++): ?>
                        <a href="<?= $arSectData['SECTION_PAGE_URL']; ?>filter/tags-is-<?= $items_tags[$i]; ?>/apply/">
                            <span><?= $items_tags[$i]; ?></span>
                        </a>
                    <? endfor; ?>
                    <a class="catalogTopTagsShowMore" href="javascript:void(0);"><span>показать еще</span></a>
                    <? for ($i = 15; $i < count($items_tags); $i++): ?>
                        <a class="catalogTopTagsMore" href="<?= $arSectData['SECTION_PAGE_URL']; ?>filter/tags-is-<?= $items_tags[$i]; ?>/apply/">
                            <span><?= $items_tags[$i]; ?></span>
                        </a>
                    <? endfor; ?>
                    <a class="catalogTopTagsHideMore" href="javascript:void(0);"><span>скрыть</span></a>
                </div>
            <? else: ?>

                <div class="catalogTopTags">
                    <? foreach ($items_tags as $tag): ?>
                        <a href="<?= $arSectData['SECTION_PAGE_URL']; ?>filter/tags-is-<?= $tag; ?>/apply/">
                            <span><?= $tag; ?></span>
                        </a>
                    <? endforeach; ?>
                </div>

            <? endif; ?>

        <? endif; ?>

        <? if (!empty($items_series)): ?>

            <h2>Серии товаров</h2>

            <ul class="catalogTopSeries">
                <? foreach ($items_series as $key => $seria): ?>
                    <? 
                        $seria_name = $arSectData['NAME'] . ' ' . $seria['VALUE']; 
                        $seria_url = $arSectData['SECTION_PAGE_URL'] . 'filter/series-is-' . $key . '/apply/';
                        $seria_picture = (!empty($items_series_pictures[$key])) ? CFile::GetPath($items_series_pictures[$key]) : '';
                           
                    ?>
                    <li>
                        <div class="catalogTopSeries__image">
                        <a href="<?=$seria_url; ?>">
                                <img src="/images/placeholder.gif" data-src="<?= $seria_picture; ?>" alt="<?= $seria_name; ?>" class="lzld">
                            </a>
                        </div>
                        <div class="catalogTopSeries__info">
                            <a class="catalogTopSeries__info-link" href="<?=$seria_url; ?>">
                                <p class="catalogTopSeries__description"><?= $seria_name; ?></p>
                            </a>
                            <div class="catalogTopSeries__parametr">
                                <a href="<?=$seria_url; ?>" class="catalogTopSeries__modif">
                                    <?= $seria['COUNT']; ?> моделей
                                </a>
                                <? if ($seria['MINIMUM_PRICE'] > 0): ?>
                                    <span>от</span>
                                    <div class="catalogTopSeries__price">
                                        <?= \PDV\Tools::formatPrice($seria['MINIMUM_PRICE']); ?><span class="catalogTopSeries__price-rub">руб.</span>
                                    </div>
                                <? endif; ?>
                            </div>
                        </div>
                    </li>
                <? endforeach; ?>
            </ul>

        <? endif; ?>

<?
        
        $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        $getSort = htmlspecialchars($request->get('sort'));
        $getCount = htmlspecialchars($request->get('count'));

        if (in_array($getSort, ['hit', 'sale'])) {
            $property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>16, "CODE"=>"WOBBLERS"));
            $sort = 0;
            while($enum_fields = $property_enums->GetNext())
            {
                if ($getSort == $enum_fields['VALUE']) {
                    CIBlockPropertyEnum::Update( $enum_fields['ID'], ['SORT' => 500] );
                } else {
                    $sort = $sort + 10;
                    CIBlockPropertyEnum::Update( $enum_fields['ID'], ['SORT' => $sort] );
                }
                BXClearCache(false, $arSectData['SECTION_PAGE_URL']);
            }
        }

?>

<? endif; ?>

<div class="catalogTop js-catalog_top">

    <div class="row">
        <div class="col-12 col-sm-7 d-xl-none ">
            <button class="filtrBtn openPopup" data-id="filterPopup">Фильтр</button>
            <button class="sortBtn openPopup" data-id="sortPopup">Сортировать</button>
        </div>

        <div class="catalogTopSort d-none d-xl-block col-lg-3">&nbsp;</div>

        <div class="catalogTopSort d-none d-xl-block <? if ($USER->IsAdmin()): ?>col-lg-5<?else:?>col-lg-4<?endif;?>">

            <ul>
                <li>Сортировать по:</li>
                <?foreach ( $arrSort as $sort => $item ){
                    $order = 'asc';
                    if ( $arParams["ELEMENT_SORT_FIELD"] == $item['code'] && $arParams["ELEMENT_SORT_ORDER"] == 'ASC' )
                        $order = 'desc';
                    ?>
                    <li<?if($arParams["ELEMENT_SORT_FIELD"] == $item['code']){?> class="active <?=strtolower($arParams["ELEMENT_SORT_ORDER"])?>"<?}?>>
                        <a href="<?=$APPLICATION->GetCurPageParam('sort='.$sort.'_'.$order, array('sort'))?>"><?=$item['name']?> <svg width="11px" height="10px" viewBox="0 0 11 10"><use xlink:href="/images/style/icon-sprite.svg#sort-icon" /></svg></a>
                    </li>    
                <? } ?>
                <?if ($USER->IsAdmin()): ?>
                    <li class="catalogTopSort2<? if ($getSort == 'sale') { echo ' active'; } ?>">
                        <a href="<?= $APPLICATION->GetCurPageParam('sort=sale', array('sort')); ?>">Распродажа</a>
                    </li>
                    <li class="catalogTopSort2<? if ($getSort == 'hit') { echo ' active'; } ?>">
                        <a href="<?= $APPLICATION->GetCurPageParam('sort=hit', array('sort')); ?>">Хит</a>
                    </li>
                <? endif; ?>
            </ul>
        </div>

        <?if ( !empty($arrCount) ):?>
        <div class="catalogTopLimit <? if ($USER->IsAdmin()): ?>col-lg-2<?else:?>col-lg-3<?endif;?> d-none d-sm-block">
                <? if ($USER->IsAdmin()): ?>
                    <span>Выводить по:</span> 
                    <select id="catalogTopCount">
                        <?foreach ( $arrCount as $name => $url): ?>
                            <option 
                                value="<?= $name; ?>" 
                                data-href="<?= $url; ?>"
                                <? if ($getCount == $name) {echo ' selected="selected"'; } ?>
                            >
                                <?= $name; ?>
                            </option>
                        <? endforeach; ?>
                    </select>
                <? else: ?>
                    <ul>
                        <li>Выводить по:</li>
                        <?foreach ( $arrCount as $name => $url){?>
                            <li<?if($getCount == $name || ($name == 'Все' && $showAllParam) || ($name == '30' && empty($getCount) && empty($showAllParam))){?> class="active"<?}?>><a href="<?=$url?>"><?=$name?></a></li>
                        <? } ?>
                    </ul>
                <? endif; ?>
            </div>
        <?endif;?>

        <div class="catalogTopStyle col-lg-2 d-none d-xl-block">
            <ul>
                <li>Вид товаров:</li>
                <li <?if ($_SESSION["PROJECT"]["SECTION_TEMPLATE"]=="tile"):?>class="active"<?endif?> data-id="tile"><a href="<?=$APPLICATION->GetCurPageParam('view=tile',['view'])?>#catalog"><svg width="19px" height="15px" viewBox="0 0 19 15"><use xlink:href="/images/style/icon-sprite.svg#tile" /></svg></a></li>
                <li <?if ($_SESSION["PROJECT"]["SECTION_TEMPLATE"]=="list"):?>class="active"<?endif?> data-id="list"><a href="<?=$APPLICATION->GetCurPageParam('view=list',['view'])?>#catalog"><svg width="19px" height="15px" viewBox="0 0 19 15"><use xlink:href="/images/style/icon-sprite.svg#list" /></svg></a></li>
            </ul>
        </div>
    </div>
</div>
