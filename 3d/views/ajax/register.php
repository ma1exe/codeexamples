<?

    $errors = [];

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (!isset($_SESSION['USER_ID']) || !isset($_POST['confirmation_email']) || !empty($_POST['confirmation_email'])) {

            if (empty($_POST['email'])) {
                $errors[] = 'E-mail не может быть пустым.';
            }
            if (empty($_POST['password'])) {
                $errors[] = 'Пароль не может быть пустым.';
            }
            if (empty($_POST['confirmation_password'])) {
                $errors[] = 'Подтвеждение пароля не может быть пустым.';
            }
            if (empty($_POST['first_name'])) {
                $errors[] = 'Имя не может быть пустым.';
            }
            /*if (empty($_POST['last_name'])) {
                $errors[] = 'Фамилия не может быть пустой.';
            }*/

            if ($_POST['password'] !== $_POST['confirmation_password']) {
                $errors[] = 'Пароли не совпадают.';
            }

            if (strlen($_POST['password']) < MIN_LENGTH_USER_PASSWORD) {
                $errors[] = 'Длина пароля не может быть меньше ' . MIN_LENGTH_USER_PASSWORD . ' символов.';
            }
            if (strlen($_POST['password']) > MAX_LENGTH_USER_PASSWORD) {
                $errors[] = 'Длина пароля не может быть больше ' . MAX_LENGTH_USER_PASSWORD . ' символов.';
            }

            $user = $mysqli->query("SELECT * FROM " . DB_TABLE_USERS . " WHERE `email` = '" . htmlspecialchars($_POST['email']) . "'");
            if ($product = $user->fetch_assoc()) {
                $errors[] = 'Пользователь с таким e-mail уже зарегистрирован.';
            }

            if ($errors) {
                echo json_encode(['success' => false, 'errors' => $errors]);
            } else {
                $email = htmlspecialchars($_POST['email']);
                $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
                $first_name = htmlspecialchars($_POST['first_name']);
                $last_name = ''; //htmlspecialchars($_POST['last_name']);
                $user_hash = generateHashString();
                $sql = "INSERT INTO " . DB_TABLE_USERS . " (`email`, `password`, `first_name`, `last_name`, `hash`, `hash_password_reset`) VALUES ('" . $email . "', '" . $password .  "', '" . $first_name . "', '" . $last_name . "', '" . $user_hash . "', '" . generateHashString() . "')";
                if (!$mysqli->query($sql)) {
                    $errors[] = 'При создании пользователя произошла ошибка. Попробуйте позже.';
                    // $errors[] = $sql;
                    echo json_encode(['success' => false, 'errors' => $errors]);
                } else {
                    setcookie('user_id', $mysqli->insert_id, time()+60*60*24*30, '/');
                    setcookie('user_hash', $user_hash, time()+60*60*24*30, '/', null, null, true); // httponly !!! , null, null, true

                    // Письмо тому, кто зарегистрировался

                    // To send HTML mail, the Content-type header must be set
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                    
                    // Create email headers
                    $headers .= 'From: ' . SITE_INFO_EMAIL . "\r\n".
                        'Reply-To: ' . $_POST['email'] . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();

                    $message = "<html><body><p>Вы успешно зарегистрировались на сайте " . SITE_EMAIL_URL . ".</p>";
                    $message .= '<p>Ваши данные для входа: </p><p>Логин - ' . $_POST['email'] . '</p><p>Пароль - ' . $_POST['password'] . '</p></body></html>';
                    mail($_POST['email'], 'Регистрация на сайте ' . SITE_EMAIL_URL, $message, $headers);

                    // Письмо на адрес сайта

                    // To send HTML mail, the Content-type header must be set
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

                    // Create email headers
                    $headers .= 'From: ' . SITE_INFO_EMAIL . "\r\n".
                        'Reply-To: ' . SITE_INFO_EMAIL . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();

                    $message = "<html><body><p>На сайте " . SITE_EMAIL_URL . " зарегистрировался новый пользователь.</p>";
                    $message .= '<p>Имя - ' . $first_name . '</p><p>Почта - ' . $_POST['email'] . '</p></body></html>';
                    mail(SITE_INFO_EMAIL, 'Новый пользователь на сайте ' . SITE_EMAIL_URL, $message, $headers);
                    
                    echo json_encode(['success' => true, 'errors' => $errors]);
                }
            }

        } else {
            echo json_encode(['success' => false, 'errors' => $errors]);
        }
    } else {
        echo json_encode(['success' => false, 'errors' => $errors]);
    }