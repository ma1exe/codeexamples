<?

    class Planfix {
        private $log;

        private $api_server;
        private $api_key;
        private $api_token;

        public $account;

        function __construct() {
            $this->log = new Logger();
            $this->log->lfile(PLANFIX_LOGS_FOLDER . @date('d_m_Y') . '_api.txt');

            $this->api_server = 'https://apiru.planfix.ru/xml/';
            $this->api_key = '';
            $this->api_token = '';

            $this->account = '';            
        }

        function __destruct() {
            $this->log->lclose();
        }

        public function request($requestXml) {
            $this->log->lwrite("Вызвана функция request()");
            $this->log->lwrite($requestXml->asXML());

            $ch = curl_init($this->api_server);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // не выводи ответ на stdout
            curl_setopt($ch, CURLOPT_HEADER, 1); // получаем заголовки
            // не проверять SSL сертификат
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            // не проверять Host SSL сертификата
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $this->api_key . ':' . $this->api_token);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml->asXML());

            $response = curl_exec($ch);
            $error = curl_error($ch);
            //var_dump($error);

            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $responseBody = substr($response, $header_size);

            curl_close($ch);

            $this->log->lwrite("Функция request() завершила запрос");
            $this->log->lwrite($responseBody);

            return $responseBody; 
        }

    }
