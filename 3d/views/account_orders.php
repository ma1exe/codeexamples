<? 
    $result = $mysqli->query('
        SELECT
            *
        FROM
            ' . DB_TABLE_ORDERS . '
        WHERE
            user_id = ' . $user['id']
    );
?>

<h1>Мои заказы</h1>

<? if ($result && $result->num_rows): ?>
    <? 
        $i = 0;
        while ($order = $result->fetch_assoc()): 
            $i++;
    ?>
        <? if ($i > 1): ?><hr><? endif; ?>
        <div>
            <h2>Заказ №<?= $order['id']; ?></h2>
            <h4>Сумма: <?= generatePriceString($order['total_sum'], $current_rate, $current_symbol); ?></h4>
            <h4>Дата: <?= $order['date']; ?></h4>
            <p><a href="/account_order_view<?= SITE_URLS_SUFFIX; ?>?id=<?= $order['id']; ?>">Просмотр</a></p>
        </div>
    <? endwhile; ?>
<? else: ?>
    <p>Заказов нет.</p>
<? endif; ?>