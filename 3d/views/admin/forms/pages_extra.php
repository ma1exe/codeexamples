<form action="" method="POST">
    <div class="form-group">
        <label for="blocks_1"><?= $blocks_1_title; ?></label>
        <textarea name="blocks_1" class="form-control tinymce" id="blocks_1" rows="3"><?= ($page_extra) ? $page_extra['blocks_1'] : ''; ?></textarea>
    </div>
    <div class="form-group">
        <label for="blocks_2"><?= $blocks_2_title; ?></label>
        <textarea name="blocks_2" class="form-control tinymce" id="blocks_2" rows="3"><?= ($page_extra) ? $page_extra['blocks_2'] : ''; ?></textarea>
    </div>
    <button name="submit" type="submit" class="btn btn-primary">Обновить</button>
</form>
