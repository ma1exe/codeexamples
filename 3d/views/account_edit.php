<h1>Изменить данные</h1>

<form class="form_account_edit" action="/account_edit<?= SITE_URLS_SUFFIX; ?>" method="POST" data-type="personal">

    <div class="page">
        <div class="pagecontent">
            <fieldset class="form">
                <span class="Requirement"> поля, отмеченные *, обязательны для заполнения</span><br><br>
                <legend>Персональные данные</legend>
                <p><label for="first_name">Имя:</label> <input type="text" name="first_name" id="first_name" value="<?= $user['first_name']; ?>" required>&nbsp;<span class="Requirement">*</span></p>
                <!--
                    <p>
                        <label for="last_name">Фамилия:</label> 
                        <input type="text" name="last_name" id="last_name" value="<?= $user['last_name']; ?>" required>
                        &nbsp;<span class="Requirement">*</span>
                    </p>
                -->
                <p><label for="password">Введите пароль:</label> <input type="password" name="password" id="password" required>&nbsp;<span class="Requirement">*</span></p>
            </fieldset>
        </div>
    </div>
    
    <div class="pagecontentfooter">
        <span class="button">
            <button type="submit">
                <img src="/web/images/icons/submit.png" alt="Обновить" title=" Обновить " width="12" height="12">
                &nbsp;Обновить
            </button>
        </span>
    </div>

</form>

<br>

<form class="form_account_edit" action="/account_edit<?= SITE_URLS_SUFFIX; ?>" method="POST" data-type="password">

    <div class="page">
        <div class="pagecontent">
            <fieldset class="form">
                <span class="Requirement"> поля, отмеченные *, обязательны для заполнения</span><br>
                <span class="Requirement"> после смены пароля потребуется заново войти в профиль</span><br><br>
                <legend>Изменить пароль</legend>
                <p>
                    <label for="password">Текущий пароль:</label> 
                    <input type="password" name="password" id="password" minlength="<?= MIN_LENGTH_USER_PASSWORD; ?>" maxlength="<?= MAX_LENGTH_USER_PASSWORD; ?>" required>&nbsp;<span class="Requirement">*</span>
                </p>
                <p>
                    <label for="new_password">Новый пароль:</label> 
                    <input type="password" name="new_password" id="new_password" minlength="<?= MIN_LENGTH_USER_PASSWORD; ?>" maxlength="<?= MAX_LENGTH_USER_PASSWORD; ?>" required>&nbsp;<span class="Requirement">*</span>
                </p>
                <p>
                    <label for="confirmation_new_password">Подтвердите пароль:</label> 
                    <input type="password" name="confirmation_new_password" id="confirmation_new_password"  minlength="<?= MIN_LENGTH_USER_PASSWORD; ?>" maxlength="<?= MAX_LENGTH_USER_PASSWORD; ?>" required>&nbsp;<span class="Requirement">*</span>
                </p>
            </fieldset>
        </div>
    </div>
    
    <div class="pagecontentfooter">
        <span class="button">
            <button type="submit">
                <img src="/web/images/icons/submit.png" alt="Обновить" title=" Обновить " width="12" height="12">
                &nbsp;Обновить
            </button>
        </span>
    </div>


</form>