<?
    $errors = [];
    $success = false;
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $email = htmlspecialchars($_POST['email']);
        
        $sql = "SELECT * FROM " . DB_TABLE_USERS . " WHERE email = '" . $email . "'";
        $result = $mysqli->query($sql);
        if ($user = $result->fetch_assoc()) {
            if (password_verify($_POST['password'], $user['password'])) {
                $user_hash = generateHashString();
                $sql_hash = "UPDATE " . DB_TABLE_USERS . " SET hash = '" . $user_hash . "' WHERE id = " . $user['id'];
                if ($mysqli->query($sql_hash)) {
                    setcookie('user_id', $user['id'], time()+60*60*24*30, '/');
                    setcookie('user_hash', $user_hash, time()+60*60*24*30, '/', null, null, true); // httponly !!! , null, null, true
                    $success = true;
                } else {
                    $errors[] = 'Неудачная попытка авторизации. Попробуйте позже.';
                }
            } else {
                $errors[] = 'Неверный пароль.';
            }
        } else {
            $errors[] = 'Пользователь с таким email не найден.';
        }
    }

    echo json_encode(['success' => $success, 'errors' => $errors]);