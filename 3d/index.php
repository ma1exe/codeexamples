<?
	if (FALSE) {
		ini_set('display_errors', '1');
		ini_set('display_startup_errors', '1');
		error_reporting(E_ALL);
	}

	session_start();

	require('./config/database.php');
	require('./config/functions.php');
	require('./config/site.php');

	if (isset($_COOKIE['user_id']) && isset($_COOKIE['user_hash'])) {
		$user_id = intval($_COOKIE['user_id']);
		$user_hash = htmlspecialchars($_COOKIE['user_hash']);
		
		$sql = "SELECT * FROM " . DB_TABLE_USERS . " WHERE id = " . $user_id . " AND hash = '" . $user_hash . "'";
		$result = $mysqli->query($sql);
		if ($user_db = $result->fetch_assoc()) {
			$user['id'] = $user_db['id'];
			$user['first_name'] = $user_db['first_name'];
			$user['last_name'] = $user_db['last_name'];
			$user['is_admin'] = ($user_db['is_admin']) ? true : false;
		} else {
			$user['id'] = false;
			$user['is_admin'] = false;
		}
		unset($user_db, $user_id, $user_hash);
	} else {
		$user['id'] = false;
		$user['is_admin'] = false;
	}

	$currencies = [];
	$bool_currencies_need_update = false;

	function curlRequest($url) {
		$myCurl = curl_init();

		curl_setopt_array($myCurl, array(
			CURLOPT_URL => $url,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_RETURNTRANSFER => true,
		));
		$response = curl_exec($myCurl);
		curl_close($myCurl);

		return $response;
	}

	$sql = "SELECT * FROM " . DB_TABLE_CURRENCIES;
	$result = $mysqli->query($sql);
	while ($currency = $result->fetch_assoc()) {
		$currencies[$currency['id']] = $currency;
	}

	if (strtotime('+ 1 day', strtotime($currencies[1]['updated'])) < strtotime(date('Y-m-d H:i:s'))) {
		$bool_currencies_need_update = true;
	
		$currencies_new = json_decode(curlRequest('https://www.cbr-xml-daily.ru/daily_json.js'), true);
	}

	foreach ($currencies as $currency) {
		if ($bool_currencies_need_update && isset($currencies_new['Valute'][$currency['code']])) {
			$new_value = $currencies_new['Valute'][$currency['code']]['Nominal'] * $currencies_new['Valute'][$currency['code']]['Value'];
			$sql = "UPDATE " . DB_TABLE_CURRENCIES . " SET rate = " . $new_value . " WHERE `code` = '" . $currency['code'] . "'";
			$mysqli->query($sql);
		}
	}

	$current_rate = 1;
	$current_symbol = 'руб.';

	if (isset($_COOKIE['currency']) && intval($_COOKIE['currency']) > 0) {
		$current_rate = $currencies[intval($_COOKIE['currency'])]['rate'];
		$current_symbol = $currencies[intval($_COOKIE['currency'])]['symbol'];
	}

	unset($currencies);

	$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

	$path = explode('/', $path);

	$restricted_paths = [
		'guests' => ['account_orders.php', 'account_edit.php', 'account_order_view.php'],
		'users' => [
			'register.php', 'login.php', 'reset_password.php'
		]
	];

	if (
		($user['id'] && in_array($path[1], $restricted_paths['users'])) ||
		(!$user['id'] && in_array($path[1], $restricted_paths['guests'])) ||
		($path[1] == 'admin' && !$user['is_admin']) || 
		($path[1] == 'admin.php' && !$user['is_admin'])
	) {
		header('Location: /');
	}

	$page_templates = [];
	$is_404 = false;
	$is_reload_required = false;

	$meta_title = '3d модели для ЧПУ в STL формате | STL модели для ЧПУ';
	$meta_description = '3d модели для ЧПУ | STL3d модели для ЧПУ, 3d модели stl, stl 3d модели для фрезерных станков с ЧПУ и 3d принтеров';
	$meta_keywords = '3d модели для ЧПУ, stl модели3d модели для ЧПУ | 3d  stl | 3d ЧПУ';

	if ($path[1] == 'ajax' && file_exists('./views/ajax/' . $path[2])) {
		$page_templates[] = './views/ajax/' . $path[2];
	} else if ($path[1] == 'admin.php') {
		$page_templates[] = './views/admin/blocks/header.php';
		
		$page_templates[] = './views/admin/index.php';

		$page_templates[] = './views/admin/blocks/footer.php';
	} else if ($path[1] == 'admin') {
		$page_templates[] = './views/admin/blocks/header.php';
		
		if (file_exists('./views/admin/' . $path[2])) {
			$page_templates[] = './views/admin/' . $path[2];
		} else {
			$is_404 = true;
			$page_templates[] = './views/admin/404.php';
		}

		$page_templates[] = './views/admin/blocks/footer.php';
	} else {
		$page_templates[] = './views/blocks/header.php';

		if (count($path) > 2 && !empty($path[2])) {
			$is_404 = true;
			$page_templates[] = './views/404.php';
		} else if (empty($path[1])) {
			$page_templates[] = './views/index.php';
		} else if (file_exists('./views/' . $path[1])) {
			$page_templates[] = './views/' . $path[1];
		} else if ($result = $mysqli->query("SELECT * FROM " . DB_TABLE_ALIASES . " WHERE `alias` = '" . $path[1] . "'")) {
			if ($page = $result->fetch_assoc()) {

				if ($page['meta_title']) {
					$meta_title = $page['meta_title'];
				}

				if ($page['meta_description']) {
					$meta_description = $page['meta_description'];
				}

				if ($page['meta_keywords']) {
					$meta_keywords = $page['meta_keywords'];
				}

				$page_templates[] = './views/pages/' . $page['type'] . '.php';
			} else {
				$is_404 = true;
				$page_templates[] = './views/admin/404.php';
			}
		} else {
			$is_404 = true;
			$page_templates[] = './views/404.php';
		}

		$page_templates[] = './views/blocks/footer.php';
	}

	ob_start();

	if ($is_404) {
		$meta_title = 'Страница не найдена';
	}

	foreach ($page_templates as $template) {
		require($template);
	}

	if ($is_404) {
		header("HTTP/1.0 404 Not Found");
	} else if ($is_reload_required) {
		header('Location: ' . $_SERVER['REQUEST_URI']);
	}

	ob_end_flush();

	if (SITE_DEBUG_ENABLED && $user['is_admin']) {
		echo '<pre>'; print_r($_SESSION); echo '</pre>';
		echo '<pre>'; print_r($_COOKIE); echo '</pre>';
		echo '<pre>'; print_r($user); echo '</pre>';
		echo '<pre>'; print_r($cart); echo '</pre>';
	}

?>