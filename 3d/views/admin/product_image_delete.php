<?

    $sql = "SELECT * FROM " . DB_TABLE_PRODUCTS . " WHERE id = " . $_GET['id'];

    if ($result = $mysqli->query($sql)) {
        if ($result_product = $result->fetch_assoc()) {

            $image_path = $_SERVER['DOCUMENT_ROOT'] . $result_product['image'];

            if (unlink($image_path)) {
                echo '<p>Изображение продукта успешно удалено с диска.</p>';
            } else {
                echo '<p>При удалении изображения продукта с диска произошла ошибка.</p>';
            }

            $sql = "UPDATE " . DB_TABLE_PRODUCTS . " SET image = '' WHERE id = " . $_GET['id'];

            if ($result_update = $mysqli->query($sql)) {
                echo '<p>Изображение продукта успешно удалено из базы данных.</p>';
            } else {
                echo '<p>При удалении изображения продукта из базы данных произошла ошибка.</p>';
            }

        } else {
            echo '<p>Ошибка при работе с запросом на получение товара.</p>';
        }
    } else {
        echo '<p>Товар не найден.</p>';
    }
  
?>

<p><a href="<?= $_SERVER['HTTP_REFERER']; ?>">Вернуться назад</a>.</p>