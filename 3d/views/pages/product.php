<?
    $result = $mysqli->query("SELECT * FROM " . DB_TABLE_PRODUCTS . " WHERE `alias_id` = '" . $page['id'] . "'");
    if (!$product = $result->fetch_assoc()):
        echo '<h1>Продукт не найден.</h1>';
	else:
		$related_products = [];
		if ($result_related_products = $mysqli->query("
			SELECT 
				p.id,
				p.title, 
				p.price,
				p.image,
				a.alias
			FROM " . DB_TABLE_PRODUCTS_RELATED . " pr 
			LEFT OUTER JOIN " . DB_TABLE_PRODUCTS . " p 
				ON pr.related_id = p.id
			LEFT OUTER JOIN " . DB_TABLE_ALIASES . " a 
				ON a.id = p.alias_id
			WHERE pr.product_id = " . $product['id']
		)) {
			while ($related_product = $result_related_products->fetch_assoc()) {
				$related_products[] = $related_product;
			}
		}

		$images_extra = false;

		if ($product['images_extra']) {
			$images_extra = explode(',', $product['images_extra']);
		}
?>

	<div itemscope="" itemtype="http://schema.org/Product">
		<h1 itemprop="name"><?= $product['title']; ?></h1>
		<div class="row-fluid">
			<div class="span6 product-images">
				<div class="thumbnail big text-center">
                    <a href="<?= checkImageExists($product['image']); ?>" title="<?= $product['title']; ?>" class="lightbox cboxElement">
                        <img itemprop="image" id="img" src="<?= checkImageExists($product['image']); ?>" alt="<?= $product['title']; ?>">
                        <span class="frame-overlay"></span>
                        <span class="zoom">
                            <i class="fa fa-search-plus"></i>
                        </span>
                    </a>
				</div>
				<? if ($images_extra): ?>
					<div class="row-fluid small">
						<? foreach ($images_extra as $img): ?>
							<div class="span4 thumbnail text-center">
								<a href="<?= checkImageExists($img); ?>" title=""<?= $product['title']; ?>" class="lightbox cboxElement">
									<img src="<?= checkImageExists($img); ?>" itemprop="image" alt=""<?= $product['title']; ?>">
									<span class="frame-overlay"></span>
									<span class="zoom"><i class="fa fa-search-plus"></i></span>
								</a>
							</div>
						<? endforeach; ?>
					</div>
				<? endif; ?>
			</div>
			<div class="span6 product-info">
				<div class="description inner" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
					<span class="price"><?= generatePriceString($product['price'], $current_rate, $current_symbol); ?></span>
				</div>
				<div class="inner nobottom product-cart">
					<p class="CartContentLeft"></p>
					&nbsp;&nbsp;<span class="button">
						<?
							$button_cart_title = (isset($cart['items'][$product['id']])) ? 'Убрать из корзины' : 'Добавить в корзину';
							$button_cart_class = (isset($cart['items'][$product['id']])) ? 'btn-remove-from-cart-js' : 'btn-add-to-cart-js';
						?>
						<button type="button" class="<?= $button_cart_class; ?>" data-id="<?= $product['id']; ?>">
							<img src="/web/images/icons/buy.png" alt="<?= $button_cart_title; ?>" title=" <?= $button_cart_title; ?> " width="12" height="12">
							&nbsp;<?= $button_cart_title; ?>
						</button>
					</span>
					<p></p>
				</div>
			</div>
		</div>

	<div class="row-fluid">
		<div class="row-fluid product-tabs">
			<div class="widget">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#description" data-toggle="tab" aria-expanded="true"><i class="fa fa-thumbs-up"></i> Описание</a></li>
					<li class=""><a href="#shipping" data-toggle="tab" aria-expanded="false"><i class="fa fa-truck"></i> Доставка</a></li>
					<li><a href="#payment" data-toggle="tab"><i class="fa fa-credit-card"></i> Оплата</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane inner fade notop active in" id="description">
						<div itemprop="description">
                            <?= htmlspecialchars_decode($product['description']); ?>
						</div>
					</div>
					<div class="tab-pane inner fade notop" id="shipping">
						<p>Условия доставки.</p>
						<p>3d модель, в STL формате, отправляем после оплаты в течение часа на емейл указанный при регистрации.</p>
						<p>&nbsp;</p>
					</div>
					<div class="tab-pane inner fade in notop" id="payment">
						<? require($_SERVER['DOCUMENT_ROOT'] . '/views/blocks/payment_methods.php'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<? if ($related_products): ?>
		<? $i = 0; ?>

		<h3>С данным товаром также покупают:</h3>

		<div class="page">
		<div class="pageItem">
			<div class="row-fluid shop-products">
				<ul class="thumbnails">
					<? foreach ($related_products as $product): ?>
						<? $i++; ?>
						<li class="item span3<?= ($i % 5 == 0 || $i == 1) ? ' first' : ''; ?>">
							<div class="thumbnail text-center" style="height: 249px;">
								<a href="/<?= $product['alias']; ?>" class="image">
								<img src="<?= checkImageExists($product['image']); ?>" alt="<?= $product['title']; ?>" style="height: 189px;">
								<span class="frame-overlay"></span><span class="price"> <?= generatePriceString($product['price'], $current_rate, $current_symbol); ?></span></a>
								<div class="inner notop nobottom text-center">
									<h4 class="title"><a href="/<?= $product['alias']; ?>"><?= $product['title']; ?></a></h4>
								</div>
							</div>
							<div class="inner darken notop text-center">
								<?
									$button_cart_title = (isset($cart['items'][$product['id']])) ? 'Убрать' : 'Добавить';
									$button_cart_class = (isset($cart['items'][$product['id']])) ? 'btn-remove-from-cart-js' : 'btn-add-to-cart-js';
								?>
								<button type="button" class="btn btn-add-to-cart <?= $button_cart_class; ?>" data-id="<?= $product['id']; ?>">
								<i class="fa fa-shopping-cart"></i> <?= $button_cart_title; ?>
								</button>
							</div>
						</li>
					<? endforeach; ?>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<? endif; ?>
</div>

<? endif; ?>