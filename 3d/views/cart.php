<? if ($cart['count']): ?>
	<h1>В корзине:</h1>
	<form id="checkout" action="/checkout<?= SITE_URLS_SUFFIX; ?>" method="post">
		<div class="page">
			<div class="pagecontent">
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
					<tbody>
						<tr>
							<td>&nbsp;</td>
							<td align="left"><strong>Товар</strong></td>
							<td align="left"><strong>Цена</strong></td>
							<td align="left"><strong>Количество</strong></td>
							<td align="left"><strong>Удалить</strong></td>
						</tr>
						<tr>
							<td colspan="6"></td>
						</tr>
						<? foreach ($cart['items'] as $id => $item): ?>
							<tr>
								<td><img src="<?= checkImageExists($item['image']); ?>" width="100" alt="<?= $item['title']; ?>"></td>
								<td valign="middle" align="left">
									<strong><a href="/<?= $item['alias']; ?>"><?= $item['title']; ?></a></strong><br>
								</td>
								<td valign="middle" align="left"> <?= generatePriceString($item['price'], $current_rate, $current_symbol); ?></td>
								<td valign="middle" align="left">1</td>
								<td width="10" align="left" valign="middle">
									<button class="button btn btn-secondary btn-remove-from-cart-js" type="button" data-id="<?= $id; ?>">
										<span>X</span>
									</button>
								</td>
							</tr>
							<tr>
								<td colspan="6"></td>
							</tr>
						<? endforeach; ?>
						<tr>
							<td colspan="6" align="right"><strong>Итого: <?= generatePriceString($cart['total_sum'], $current_rate, $current_symbol); ?><br></strong></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="clear"></div>
		<div class="navigation"><span class="right">
			<a class="button" href="javascript:void(0);" onclick="$('#checkout').submit();">
				<span>
					<img src="/web/images/icons/checkout.png" alt="Оформить заказ" title=" Оформить заказ " width="12" height="12">
					&nbsp;Оформить заказ
				</span>
			</a>
		</span></div>
		<div class="clear"></div>
	</form>
<? else: ?>
	<h1>Ваша корзина пуста.</h1>
<? endif; ?>