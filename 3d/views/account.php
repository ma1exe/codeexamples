
<h1>Ваши персональные данные</h1>
<div class="page">
	<div class="pagecontent">
		<p>Добро пожаловать!</p>
		<p>Это Ваша персональная страница, где Вы можете просмотреть выполненные заказы, а также список просмотренных Вами товаров.</p>
	</div>
</div>
<div class="page">
	<div class="pagecontent">

		<? if ($user['id']): ?>

			<p>Персональная информация</p>
			<ul class="accountLinks">
				<li class="accountLinks"><a href="/account_orders<?= SITE_URLS_SUFFIX; ?>">Мои заказы</a></li>
				<li class="accountLinks"><a href="/account_edit<?= SITE_URLS_SUFFIX; ?>">Посмотреть или изменить Ваши данные</a></li>
				<? if ($user['is_admin']): ?>
					<li class="accountLinks"><a href="/admin<?= SITE_URLS_SUFFIX; ?>">Перейти в панель администратора</a></li>
				<? endif; ?>
				<li class="accountLinks"><a href="/ajax/logout<?= SITE_URLS_SUFFIX; ?>">Выйти из аккаунта</a></li>
			</ul>

		<? else: ?>

			<a class="btn btn-inverse" href="/login<?= SITE_URLS_SUFFIX; ?>"><i class="fa fa-user"></i> Войдите или зарегистрируйтесь</a>

		<? endif; ?>

	</div>
</div>