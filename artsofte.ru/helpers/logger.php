<?
    class Logger {
        // declare log file and file pointer as private properties
        private $log_file, $fp;
        // set log file (path and name)
        public function lfile($path) {
            $this->log_file = $path;
        }
        // write message to the log file
        public function lwrite($message) {
            // if file pointer doesn't exist, then open log file
            if (!is_resource($this->fp)) {
                $this->lopen();
            }
            // define script name
            $script_name = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
            // define current time and suppress E_WARNING if using the system TZ settings
            // (don't forget to set the INI setting date.timezone)
            $time = @date('[d/m/Y H:i:s]');
            // write current time, script name and message to the log file
            fwrite($this->fp, "$time $message" . PHP_EOL);
        }
        // close log file (it's always a good idea to close a file when you're done with it)
        public function lclose() {
            fclose($this->fp);
        }
        // open log file (private method)
        private function lopen() {
            $log_file_default = $_SERVER['DOCUMENT_ROOT'] . 'amofix/logs/' . @date('d_m_Y') . '.txt';
            // define log file from lfile method or use previously set default
            $lfile = $this->log_file ? $this->log_file : $log_file_default;
            // open log file for writing only and place file pointer at the end of the file
            // (if the file does not exist, try to create it)
            $this->fp = fopen($lfile, 'a') or exit("Can't open $lfile!");
        }
    }