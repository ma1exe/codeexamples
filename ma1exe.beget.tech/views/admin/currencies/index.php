<?php

    use yii\helpers\Url;
    use yii\widgets\LinkPager;
?>

<div class="row">
    <div class="col-xs-12">
        <h2>Валюты <a href="<?= Url::toRoute('admin/currencies/update'); ?>">ОБНОВИТЬ</a></a></h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Название</th>
                    <th>Код</th>
                    <th>Символ</th>
                    <th>Курс</th>
                    <th>Обновлено</th>
                </tr>
            </thead>
            <?php foreach ($currencies as $currency): ?>
                <tr>
                    <td><?= $currency['name']; ?></td>
                    <td><?= $currency['code']; ?></td>
                    <td><?= $currency['symbol']; ?></td>
                    <td><?= $currency['rate']; ?></td>
                    <td><?= $currency['updated']; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?= LinkPager::widget([
    'pagination' => $pages,
]); ?>