<? 
	$sql = '
		SELECT 
			c.id,
			c.title,
			a.alias 
		FROM ' . DB_TABLE_CATEGORIES . ' c 
		LEFT OUTER JOIN ' . DB_TABLE_ALIASES . ' a 
		ON 
			a.id = c.alias_id
		ORDER BY `title` ASC
	';
	$result = $mysqli->query($sql);
	$categories = [];

	while ($category = $result->fetch_assoc()) {
		$categories[] = $category;
	}

	$cart = (isset($_SESSION['cart'])) ? $_SESSION['cart'] : [];
	$cart['count'] = 0;
	$cart['total_sum'] = 0;
	if ($cart && isset($cart['items'])) {
		$cart_items = [];
		foreach ($cart['items'] as $id) {
			// echo '<pre>'; print_r($cart); echo '</pre>'; 
			$sql = "SELECT * FROM " . DB_TABLE_PRODUCTS . " p LEFT OUTER JOIN " . DB_TABLE_ALIASES . " a ON a.id = p.alias_id WHERE p.id = " . $id;
			// echo $sql.'<br>';
			$result = $mysqli->query($sql);
			// echo $result->num_rows;
			if ($product = $result->fetch_assoc()) {
				$cart_items[$id]['title'] = $product['title'];
				$cart_items[$id]['price'] = $product['price'];
				$cart_items[$id]['image'] = $product['image'];
				$cart_items[$id]['alias'] = $product['alias'];
				$cart['count']++;
				$cart['total_sum'] += $product['price'];
				// echo '<pre>'; print_r($item); echo '</pre>'; 
			}
		}
		$cart['items'] = $cart_items;
		unset($cart_items);
	}

	$cart_allowed_paths = ['checkout'];

	if (in_array($path[1] . SITE_URLS_SUFFIX, $cart_allowed_paths) && $cart['count'] == 0) {
		header('Location: /');
	}

	// echo '<pre>'; print_r($cart); echo '</pre>'; 

	$menu = [
		'cart' => 'Корзина',
		'contact_us' => 'Обратная связь',
		'account' => 'Мой профиль',
		'sposoby_oplaty' => 'Способы оплаты'
	];
?>

<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0, width=device-width" />
<meta name="theme-color" content="#dd2c00"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<meta name="robots" content="index,follow" />
<meta name="twitter:domain" content="<?= SITE_URL; ?>" />
<meta property="og:site_name" content="localhost" />
<meta name="twitter:card" content="summary" />
<title><?= $meta_title; ?></title>
<meta name="description" content="<?= $meta_description; ?>" />
<meta name="keywords" content="<?= $meta_keywords; ?>" />
<link rel="canonical" href="<?= SITE_URL; ?>" />
<meta property="og:title" content="<?= $meta_title; ?>" />
<meta property="og:description" content="<?= $meta_description; ?>" />
<meta name="twitter:title" content="<?= $meta_title; ?>" />
<meta name="twitter:description" content="<?= $meta_description; ?>" />
<base href="<?= SITE_URL; ?>" />
</head>
<body>
<div class="topbar navbar">
	<div class="container">
		<ul class="nav nav-pills pull-right">
			<li><a href="/ajax/set_currency<?= SITE_URLS_SUFFIX; ?>?id=0">RUB</a></li>
			<li><a href="/ajax/set_currency<?= SITE_URLS_SUFFIX; ?>?id=1">USD</a></li>
			<li><a href="/ajax/set_currency<?= SITE_URLS_SUFFIX; ?>?id=2">EUR</a></li>
			<li id="google_translate_element">
		
				<script>
					function googleTranslateElementInit() {
						new google.translate.TranslateElement({pageLanguage: 'ru', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
					}
				</script>
				<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
			</li>
		</ul>
	</div>
</div>
<div id="header">
	<div class="container">
		<div class="row-fluid">
			<div class="span7 logo">
				<a href="<?= SITE_URL; ?>">
					<img src="/web/images/logo.png" alt="Логотип" title="Логотип" />
				</a>
			</div>
			<div class="span6 mobile logo-mobile">
				<a href="<?= SITE_URL; ?>">
					<img src="/web/images/logo_mobile.png" alt="Логотип" title="Логотип" />
				</a>
			</div>
			<div class="span5 text-center text-wrapper" itemscope itemtype="http://schema.org/Organization">
				<div class="text">
					<span itemprop="name" style="font-size: 25px;">3d-model-stl.com</span>
					<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"> 3d модели для фрезерных станков с ЧПУ</div>
					<h4>
						<a href="tel:+79586461255 ">
							<i class="fa fa-phone"></i> 
							<span itemprop="telephone">+7 958 646 12 55</span>
						</a>
					</h4>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="navigation" class="default">
	<div class="container-fluid">
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar navbar-toggle toggle-menu menu-left" data-toggle="collapse" data-target=".navbar-responsive-collapse">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="index.php"><i class="fa fa-home"></i></a>
					<div class="nav-collapse collapse navbar-responsive-collapse cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left">
						<ul class="nav">
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle" href="">Каталог <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<? foreach ($categories as $category): ?>
										<li><a href="/<?= $category['alias']; ?>"><?= $category['title']; ?></a></li>
									<? endforeach; ?>
								</ul>
							</li>
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle" href="">Информация <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<? foreach ($menu as $alias => $name): ?>
										<li><a href="/<?= $alias; ?><?= SITE_URLS_SUFFIX; ?>"><?= $name; ?></a></li>
									<? endforeach; ?>
								</ul>
							</li>
						</ul>
						<div class="search">
							<form class="navbar-search pull-left input-append" id="search" action="/advanced_search_result<?= SITE_URLS_SUFFIX; ?>" method="get" autocomplete="off">
								<input class="search-query span2 placeholder" id="quick_find_keyword" autocomplete="off" name="keywords" type="text" placeholder="Поиск">
								<input class="btn search-bt" type="submit" value="">
								<div class="ajaxQuickFind" id="ajaxQuickFind"></div>
							</form>
						</div>
						<ul class="nav pull-right">
							<li><a href="/contact_us<?= SITE_URLS_SUFFIX; ?>"> Обратная связь</a></li>
							<? if (!$user['id']): ?>
								<li><a href="/login<?= SITE_URLS_SUFFIX; ?>"><i class="fa fa-user"></i> Регистрация и вход</a></li>
							<? else: ?>
								<li><a href="/account<?= SITE_URLS_SUFFIX; ?>"><i class="fa fa-user"></i> Мой профиль</a></li>
							<? endif; ?>
							<li id="divShoppingCart">
								<a data-toggle="dropdown" class="dropdown-toggle cart" data-target="#" href="/cart<?= SITE_URLS_SUFFIX; ?>" title="Корзина"> 
									<i class="fa fa-shopping-cart"></i> Корзина 
									<sup><span title="" class="badge badge-important"><?= $cart['count']; ?></span></sup> 
									<b class="caret"></b>
								</a>
								<div class="dropdown-menu cart">
									<div class="widget inner shopping-cart-widget">
										<div class="cart-dropdown">
											<div class="content">
												<? if ($cart['count']) : ?>
													<div class="products">
														<? foreach ($cart['items'] as $id => $item): ?>
															<? // echo '<pre>'; print_r($item); echo '</pre>'; ?>

															<div class="media">
																<a class="pull-right" href="/<?= $item['alias']; ?>">
																	<img class="media-object" src="<?= checkImageExists($item['image']); ?>" alt="<?= $item['alias']; ?>" title="<?= $item['alias']; ?>" width="40" height="40"/>
																</a>
																<div class="media-body">
																	<span class="media-heading"><a href="/<?= $item['alias']; ?>"><?= $item['title']; ?></a></span>  
																		<button class="btn btn-link btn-remove-from-cart-js btn-sm label label-important" data-id="<?= $id; ?>"><i class="fa fa-times"></i></button>
																	1 x <?= generatePriceString($item['price'], $current_rate, $current_symbol); ?>
																</div>
															</div>

														<? endforeach; ?>
													</div>
													<p class="subtotal"><strong>Всего</strong><span class="amount"> <?= generatePriceString($cart['total_sum'], $current_rate, $current_symbol); ?></span></p>
													<p class="buttons">
														<a class="btn btn-inverse viewcart" href="/cart<?= SITE_URLS_SUFFIX; ?>"><i class="fa fa-shopping-cart"></i> Корзина</a>
														<a class="btn btn-inverse checkout" href="/checkout<?= SITE_URLS_SUFFIX; ?>"><i class="fa fa-check"></i> Оформить заказ →</a>
													</p>
												<? else: ?>
													<p>Ваша корзина пуста.</p>
												<? endif; ?>
											</div>
										</div>
									</div>
								</div>
							</li>
							<li class="mobile" id="closeMenuMobile"><a href="javascript:void(0);">Закрыть меню</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="container">
	<div class="container">
		<div class="row-fluid">
			<div class="span9 page-sidebar pull-right">