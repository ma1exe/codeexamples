<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title); ?> | Полет 102</title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<header>
    <!-- TOP HEADER -->
    <div id="top-header">
        <div class="container">
            <ul class="header-links pull-left">
                <li><a href="tel:+79603818912"><i class="fa fa-phone"></i> +7-960-381-89-12</a></li>
                <li><a href="mailto:info@polet102.ru"><i class="fa fa-envelope-o"></i> info@polet102.ru</a></li>
            </ul>
        </div>
    </div>
    <!-- /TOP HEADER -->

    <!-- MAIN HEADER -->
    <div id="header">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- LOGO -->
                <div class="col-md-2">
                    <div class="header-logo">
                        <a href="<?= Url::toRoute(Url::home()); ?>" class="logo">
                            Полёт102
                        </a>
                    </div>
                </div>
                <!-- /LOGO -->

                <!-- SEARCH BAR -->
                <div class="col-md-8">
                    <div class="header-search">
                        <form action="<?= Url::toRoute('search/index'); ?>">
                            <input class="input" name="text" required minlength="3">
                            <button class="search-btn">Поиск</button>
                        </form>
                    </div>
                </div>
                <!-- /SEARCH BAR -->

                <!-- ACCOUNT -->
                <div class="col-md-2 clearfix">
                    <div class="header-ctn">

                        <!-- Cart -->
                        <div class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                <i class="fa fa-shopping-cart"></i>
                                <span>Корзина</span>
                                <?php 
                                    $cart_products_counter = 0;
                                    $cart_price_total = 0;
                                    foreach ($this->context->cart_products as $cart_product) {
                                        $cart_products_counter += $cart_product['count'];
                                        $cart_price_total += $cart_product['count'] * $cart_product['price'];
                                    }
                                ?>
                                <div class="qty"><?= $cart_products_counter; ?></div>
                            </a>
                            <?php if ($this->context->cart_products): ?>
                                <div class="cart-dropdown">
                                    <div class="cart-list">
                                        <?php foreach ($this->context->cart_products as $cart_product): ?>
                                            <div class="product-widget">
                                                <div class="product-img">
                                                    <img src="<?= $this->context->isImageExists($cart_product['image']); ?>" alt="<?= $cart_product['name']; ?>">
                                                </div>
                                                <div class="product-body">
                                                    <h3 class="product-name"><a href="<?= $cart_product['link']; ?>"><?= $cart_product['name']; ?></a></h3>
                                                    <h4 class="product-price">
                                                        <span class="qty"><?= $cart_product['count']; ?>x</span>
                                                        <?= $this->context->formatPriceToRUB($cart_product['price']); ?> руб.
                                                    </h4>
                                                </div>
                                                <button class="delete"><i class="fa fa-close"></i></button>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="cart-summary">
                                        <h5>ИТОГО: <?= $this->context->formatPriceToRUB($cart_price_total); ?> руб.</h5>
                                    </div>
                                    <div class="cart-btns">
                                        <a href="<?= Url::toRoute('cart/index'); ?>">К корзине</a>
                                        <a href="<?= Url::toRoute('cart/checkout'); ?>">Оформить  <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                        <!-- /Cart -->

                        <!-- Menu Toogle -->
                        <div class="menu-toggle">
                            <a href="#">
                                <i class="fa fa-bars"></i>
                                <span>Menu</span>
                            </a>
                        </div>
                        <!-- /Menu Toogle -->
                    </div>
                </div>
                <!-- /ACCOUNT -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </div>
    <!-- /MAIN HEADER -->
</header>

<div class="wrap">
    <?php
    NavBar::begin([
        /*'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,*/
        'options' => [
            'id' => 'navigation',
            'class' => 'navbar',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'main-nav nav navbar-nav'],
        'items' => [
            [
                'label' => 'Главная', 
                'url' => Url::toRoute('site/index'),
                'active' => (Yii::$app->request->url == Url::toRoute('site/index')),
            ],
            [
                'label' => 'Двигатели', 
                'url' => Url::toRoute('catalog/engines'), 
                'active' => (Yii::$app->request->url == Url::toRoute('catalog/engines')),
            ],
            [
                'label' => 'Запчасти', 
                'url' => Url::toRoute('catalog/spares'),
                'active' => (Yii::$app->request->url == Url::toRoute('catalog/spares')),
            ],
            [
                'label' => 'Парапланы', 
                'url' => Url::toRoute('catalog/paragliders'),
                'active' => (Yii::$app->request->url == Url::toRoute('catalog/paragliders')),
            ],
            [
                'label' => 'Парамоторы', 
                'url' => Url::toRoute('catalog/paramotors'),
                'active' => (Yii::$app->request->url == Url::toRoute('catalog/paramotors')),
            ],
            ['label' => 'Клуб', 'url' => Url::toRoute('club/index')],
        ],
    ]);
    NavBar::end();
    ?>

    <?php if (Url::toRoute(Url::home()) != Url::toRoute(Yii::$app->controller->getRoute())): ?>

        <div id="breadcrumb">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?= Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            'options' => [
                                'class' => 'breadcrumb-tree'
                            ],
                            'homeLink' => [
                                'label' => 'Главная',
                                'url' => Url::toRoute(Url::home()),
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>

    <?php endif; ?>

    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<!-- FOOTER -->
<footer id="footer">
    <!-- top footer -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer">
                        <h3 class="footer-title">Информация</h3>
                        <ul class="footer-links">
                            <li><a href="<?= Url::toRoute('site/contacts'); ?>">Контакты</a></li>
                            <li><a href="<?= Url::toRoute('site/delivery'); ?>">Доставка и оплата</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /top footer -->

    <!-- bottom footer -->
    <div id="bottom-footer" class="section">
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-xs-12">
                    <span class="copyright">
                        Данный интернет-сайт носит исключительно информационный характер и ни при каких условиях не является публичной офертой, 
                        определяемой положениями Статьи 437 (2) ГК РФ. Для получения подробной информации о наличии, стоимости и характеристиках указанных товаров и 
                        (или) услуг, обращайтесь по телефону <a href="tel:+79603818912">+7-960-381-89-12</a>.
                    </span>
                    <span class="copyright">
                        ИП Алексеев Ю. В., ИНН 025604668709, ОГРН 405025618200041<br>
                        Сайт разработан и поддерживается <a href="mailto:ma1exe@yandex.com" target="_blank">ma1exe@yandex.com</a><br>
                        Полет 102 © 2019 – <?= date('Y') ?>
                    </span>
                </div>
            </div>
                <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /bottom footer -->
</footer>
<!-- /FOOTER -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
