<?
    require($_SERVER['DOCUMENT_ROOT'] . 'amofix/helpers/init.php');
    
    $log = new Logger();
    
    $log->lfile(AMOCRM_LOGS_FOLDER . @date('d_m_Y') . '_redirect_uri.txt');

    $log->lwrite("Поступил $_SERVER[REQUEST_METHOD]-запрос с IP $_SERVER[REMOTE_ADDR]");

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $log->lwrite(json_encode($_POST));
    }

    $log->lclose();