<?php

namespace app\assets;

use yii\web\AssetBundle;

class ClubAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700',
        'css/club.css',
    ];
    public $js = [
    ];
    public $depends = [
    ];
}
