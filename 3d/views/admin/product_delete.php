<?

    //$sql = "DELETE FROM " . DB_TABLE_PRODUCTS . " WHERE id = " . $_GET['id'];
    $sql = "SELECT `alias_id` FROM " . DB_TABLE_PRODUCTS . " WHERE id = " . $_GET['id'];

    if ($result = $mysqli->query($sql)): 
        if ($product = $result->fetch_assoc()):
            $sql_product = "DELETE FROM " . DB_TABLE_PRODUCTS . " WHERE id = " . $_GET['id'];
            if ($mysqli->query($sql_product)): 
    ?>
                <p>Продукт успешно удален.</p>
                <? 
                    $sql_alias = "DELETE FROM " . DB_TABLE_ALIASES . " WHERE id = " . $product['alias_id'];
                    if ($mysqli->query($sql_alias)):
                ?>
                        <p>Алиас успешно удален.</p>
                    <? else: ?>
                        <p>При удалении алиаса произошла ошибка.</p>
                    <? endif; ?>
                <? 
                    $sql_related = "DELETE FROM " . DB_TABLE_PRODUCTS_RELATED . " WHERE product_id = " . $_GET['id'];
                    if ($mysqli->query($sql_related)):
                ?>
                        <p>Похожие продукты успешно очищены.</p>
                    <? else: ?>
                        <p>При удалении похожих продуктов произошла ошибка.</p>
                    <? endif; ?>
            <? else: ?>
                <p>При удалении продукта произошла ошибка.</p>
            <? endif; ?>
        <? else: ?>
            <p>Продукт не найден.</p>
        <? endif; ?>
    <? else: ?>
        <p>Ошибка при удалении: некорректный запрос к базе данных.</p>
    <? endif; ?>
<p><a href="/admin/products<?= SITE_URLS_SUFFIX; ?>">Вернуться назад</a>.</p>