<?

    $menu = [ 
        'categories' => 'Категории',
        'products' => 'Товары',
        'orders' => 'Заказы',
        'users' => 'Пользователи',
        'currencies' => 'Валюты',
        'extra' => 'Дополнительно'
    ];

?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <title>Панель администратора</title>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <span class="navbar-brand" href="/">Админка</span>
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Меню</div>
                            <a class="nav-link" href="/">К сайту</a>
                            <? foreach ($menu as $url => $title): ?>
                                <a class="nav-link" href="/admin/<?= $url; ?><?= SITE_URLS_SUFFIX; ?>"><?= $title; ?></a>
                            <? endforeach; ?>
                        </div>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">