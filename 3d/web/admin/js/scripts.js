/*ClassicEditor
        .create( document.querySelector( '#description' ), { language: 'ru' } )
        .catch( error => {
            console.error( error );
        } );*/

$(document).ready(function() {
    tinymce.init({
        selector: '.tinymce',
        plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
        language: 'ru',
        paste_data_images: false,
        convert_urls: false,
        valid_elements : '*[*]',
        // without images_upload_url set, Upload tab won't show up
        images_upload_url: 'upload.php',
        
        // override default upload handler to simulate successful upload
        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
          
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', '/ajax/admin_upload_image.php');
          
            xhr.onload = function() {
                var json;
            
                if (xhr.status != 200) {
                    failure('Ошибка HTTP: ' + xhr.status);
                    return;
                }
            
                json = JSON.parse(xhr.responseText);
            
                if (!json || typeof json.location != 'string') {
                    failure('Неправильный JSON: ' + xhr.responseText);
                    return;
                }
            
                success(json.location);
            };
          
            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
          
            xhr.send(formData);
        }
    });

    $("#image").change(function (e){
        var fileName = e.target.files[0].name;
        $("#image_label").html(fileName);
    });

    $("#images_extra").change(function (e){
        var fileCount = e.target.files.length;
        $("#images_extra_label").html('Выбрано файлов: ' + fileCount.toString());
    });

    $('#sidebarToggle').on('click', function() {
        if ($('#layoutSidenav_nav').css('transform') == 'none') {
            $('#layoutSidenav_nav').removeAttr('style');
        } else {
            $('#layoutSidenav_nav').css('transform', 'unset');
        }
    });
});