<?php 
    use yii\helpers\Url;
    use yii\widgets\LinkPager; 

    $this->title = $category['name'];
    $this->params['breadcrumbs'][] = [
        'label' => 'Каталог',
        'url' => Url::toRoute('catalog/index')
    ];
    $this->params['breadcrumbs'][] = $this->title;

    $products_chunks = array_chunk($products, 3, TRUE);
?>

<div class="row">
    <div class="col-md-3">
        <!-- ASIDE -->
        <div id="aside" class="col-md-3">
            <!-- aside Widget -->
            <div class="aside">
                <h3 class="aside-title">Категория</h3>
                <div>
                    <?php foreach ($categories as $category): ?>
                    
                        <p><a href="<?= Url::toRoute('catalog/'.$category['link']); ?>"><?= $category['name']; ?></a></p>

                    <?php endforeach; ?>
                </div>
            </div>
            <!-- /aside Widget -->

            <?= $this->render('blocks/filter', compact('manufacturers')); ?>
        </div>
        <!-- /ASIDE -->
    </div>

    <div class="col-md-9">

        <div class="row">
            <div class="col-xs-12">
                <h1><?= $this->title; ?></h1>
            </div>
        </div>

        <?php foreach($products_chunks as $products): ?>
            <div class="row">
                <?php foreach($products as $product): ?>
                    <div class="col-xs-6 col-md-4">
                        <?php 
                            $product_category = $category;
                            $product_currency = $currencies[$product['currency_id']];
                        ?>
                        <?= $this->render('blocks/product', compact('product', 'product_category', 'product_currency')); ?>
                    </div>
                <? endforeach; ?>
            </div>
        <? endforeach; ?>

        <?= LinkPager::widget([
            'pagination' => $pages,
        ]); ?>

        </div>
    
    </div>

</div>