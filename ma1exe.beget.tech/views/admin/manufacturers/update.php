<?php 
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
    use app\models\CatalogCategories;
    use app\models\CatalogManufacturers;
    use app\models\CatalogCurrencies;
?>

<?php $form = ActiveForm::begin([
    'id' => 'catalog-manufacturer',
]); ?>

    <?php if(Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= Yii::$app->session->getFlash('success'); ?>
        </div>
    <?php endif;?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'name'); ?>

    <?= $form->field($model, 'updated')->textInput(['readonly'=> true])?>

    <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>