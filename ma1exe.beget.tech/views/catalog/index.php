<?php 
    use yii\helpers\Url;
    use yii\widgets\LinkPager; 

    $this->title = 'Каталог';

    if ($current_category) {
        $this->params['breadcrumbs'][] = [
            'label' => $this->title,
            'url' => Url::toRoute('catalog/index')
        ];
        $this->title = $current_category['name'];
    }

    $this->params['breadcrumbs'][] = $this->title;

    $categories_chunks = array_chunk($categories, 4, TRUE);
    $products_chunks = array_chunk($products, 3, TRUE);
?>

<div class="row">
    <div class="col-md-3">
        <!-- ASIDE -->
        <div id="aside">
            <?= $this->render('blocks/filter', compact('manufacturers', 'popular_products')); ?>
        </div>
        <!-- /ASIDE -->
    </div>

    <div class="col-md-9">

        <div class="row">
            <div class="col-xs-12">
                <h1><?= $this->title; ?></h1>
            </div>
        </div>

        <?php foreach($products_chunks as $products): ?>
            <div class="row">
                <?php foreach($products as $product): ?>
                    <div class="col-xs-6 col-md-4">
                        <?php 
                            $product_category = $categories[$product['category_id']];
                            $product_currency = $currencies[$product['currency_id']];
                        ?>
                        <?= $this->render('//blocks/product', compact('product', 'product_category', 'product_currency')); ?>
                    </div>
                <? endforeach; ?>
            </div>
        <? endforeach; ?>

        <?= LinkPager::widget([
            'pagination' => $pages,
        ]); ?>

        </div>

    </div>
</div>