<?php
/* Данный код обрабатывает торговые предложения товаров после их выгрузки из 1С */

$full_debug = false;


/* Поиск элементов, у которых свойства не совпадают с выгрузкой */

/* $offers_options_sql = "SELECT offers.data_id, ops.option, ops.value, prop.target, product_options.value FROM Nbu44_jFtc446Cdm_msync_offers offers, Nbu44_jFtc446Cdm_msync_offer_options ops, Nbu44_jFtc446Cdm_msync_product_property prop, Nbu44_jFtc446Cdm_ms2_product_options product_options WHERE offers.id = ops.offer_id AND ops.value != '' AND ops.option != 'images' AND prop.active = 1 AND product_options.product_id = offers.data_id AND product_options.key = prop.target AND product_options.value != ops.value ";
$offers_options_str = $modx->query($offers_options_sql);
$offers_options = $offers_options_str->fetchAll(PDO::FETCH_ASSOC);

foreach ($offers_options as $option) {
    echo '<p>';
    echo $option['data_id'] . ' | ' . mb_convert_encoding($option['value'], "windows-1251", "utf-8");
    echo '</p>';
}

die(); */

$log_file = MODX_CORE_PATH . 'components/msync/logs/import_' . date("d-m-y_His") . '.txt';
$log_text = file_get_contents($log_file);

function logFormat($new_text) {
    return date("d.m.y H:i:s") . ' ' . $new_text . PHP_EOL;
}

$sql = "SELECT COUNT(*) FROM  Nbu44_jFtc446Cdm_msync_offers offer, Nbu44_jFtc446Cdm_site_content products
WHERE products.id = offer.data_id and offer.uuid_1c LIKE '%#%'";
$offers_str = $modx->query($sql);
$offers__count = $offers_str->fetchAll(PDO::FETCH_ASSOC);

$log_text .= logFormat('Количество offers: '.$offers__count[0]['COUNT(*)'].' шт.');

$counter = 0;

$modifications = [];

$sql = "SELECT offer.data_id,offer.id,offer.price,offer.name,offer.uuid_1c,offer.count FROM Nbu44_jFtc446Cdm_msync_offers offer, Nbu44_jFtc446Cdm_site_content products WHERE products.id = offer.data_id and offer.uuid_1c LIKE '%#%'";
$offers_str = $modx->query($sql);

$log_text .= logFormat('Запрос: '.$sql);

$offers = $offers_str->fetchAll(PDO::FETCH_ASSOC);


if ($full_debug) 
{
    echo "<br/>Все offers:<pre>";
    print_r($offers);
    echo "</pre>";
}

function massI__to_massK($mass)
{
	$new = [];
	$massive__name = ['brake', 'type__product', 'vendor__p', 'color', 'size__ram', 'size', 'material_rami', 'speeds'];
	foreach ($mass as $element)
	{
		if (in_array($element['option'], $massive__name))
		{
			$new[$element['option']] = $element['value'];
		}
	}
	return $new;
}

function no__element($n)
{
    global $full_debug;

	if ($full_debug) { echo 'no__element: ' . substr($n, 0, 7) . '<br/>'; }

	if (substr($n, 0, 3) == '"u0' || substr($n, 0, 7) == '"x16 u0' || substr($n, 0, 7) == '"x18 u0') return '';
	else return $n;
}

if ($full_debug) { echo 'Проходим по каждому offer:'; }

foreach ($offers as $offer)
{

    $counter++;

	if ($full_debug) { echo '<br/><br/>offer__id ' . $offer['id'] . '<br/>'; }

	$id = $offer['id'];
	$data_id = $offer['data_id'];

	$sql = "SELECT `option`, `value` FROM  Nbu44_jFtc446Cdm_msync_offer_options WHERE offer_id = {$id}";
	$options__sql = $modx->query($sql);
	$optionsYes = $options__sql->fetchAll(PDO::FETCH_ASSOC);

	$options = massI__to_massK($optionsYes);

	$options__key = array_keys($options);
	
	$options__key__str = implode($options__key, ',');

	$image__id = '';

	$sql = "SELECT `value` FROM `Nbu44_jFtc446Cdm_msync_offer_options` WHERE offer_id = {$id} AND `option` LIKE 'images' AND `value` != ''";
	if ($full_debug) { echo 'sql images: '.$sql; }
	$images__sql = $modx->query($sql);

	if ($images__sql)
	{
		$images = $images__sql->fetchAll(PDO::FETCH_ASSOC);

        if ($full_debug) 
        { 
            echo '<br/>images[0][value]: ' . $images[0]['value'] . '<br/>'; 
            echo 'data_id: ' . $data_id;
        }

		$resource = $modx->getObject('modResource', $data_id);

		if ($resource && isset($images[0]['value']) && $images[0]['value'] && file_exists(MODX_BASE_PATH . 'assets/components/msync/1c_temp/' . $images[0]['value']))
		{

			$resource->set('source', 2);
			$resource->save();

			$response = $modx->runProcessor('gallery/upload', array(
				'file' => MODX_BASE_PATH . 'assets/components/msync/1c_temp/' . $images[0]['value'],
				'id' => $data_id,
				'source' => 2
			) , array(
				'processors_path' => MODX_CORE_PATH . 'components/minishop2/processors/mgr/'
            ));
            
            if ($response->isError()) { $log_text .= logFormat('Картинка для торгового предложения "' . $name . '" (' . $data_id . ') не загружена. Ошибка: ' . $response->getMessage()); }
            else { $log_text .= logFormat('Картинка для торгового предложения "' . $name . '" (' . $data_id . ') успешно загружена.'); }

			$resource->save();
		}

		$images__name_1 = explode('/', $images[0]['value']);
		$images__name_2 = explode('.', $images__name_1[count($images__name_1) - 1]);

		if ($images__name_2)
		{
			$sql = "SELECT `id` FROM `Nbu44_jFtc446Cdm_ms2_product_files` WHERE product_id = {$data_id} AND `name` LIKE '" . $images__name_2[0] . "' AND parent = 0";
			$file__sql = $modx->query($sql);

			if ($full_debug) { echo '<br/>file__sql: ' . $sql . '<br/>'; }

			if ($file__sql)
			{
				$file__images = $file__sql->fetchAll(PDO::FETCH_ASSOC);

				$image__id = $file__images[0]['id'];
			}
		}
	}
	if ($full_debug) { echo '<br/>|image__id' . $image__id . '|<br/>'; }

	$msoptionsprice = $modx->getService('msoptionsprice');
	$msoptionsprice->initialize('web');

	$name = str_replace('\"', "", $offer['name']);

	$modifications[$data_id][] = 
		array(
			'name' => $name,
			'price' => $offer['price'],
			'image' => $image__id,
			'count' => intval($offer['count']),
			'options' => $options

		);
    
    if ($full_debug) {
        echo '<pre>'; print_r($offer); echo '</pre>';
        echo '<pre>'; print_r($options); echo '</pre>';
    }

	$products__options__new = [];
    $product__sql__str = "";
    $zap = "";
    
    $sql = "SELECT color,size__ram,size,vendor__p,type__product,brake,material_rami,speeds FROM  Nbu44_jFtc446Cdm_ms2_products WHERE id = {$data_id}";
	$products__sql = $modx->query($sql);
	if ($full_debug) { echo '<br/>products__sql: ' . $sql . '<br/>'; }
	$products__options = $products__sql->fetchAll(PDO::FETCH_ASSOC);

	foreach ($products__options[0] as $key => $product)
	{

		if ($product__sql__str != '')
		{
			$zap = ",";
        }

		if ($product == '')
		{

			$products__options__new[$key] = $options[$key];
			$product__sql__str .= $zap . '`' . $key . '` = \'"' . ($options[$key]) . '"\'';

		}
		else
		{

			$product__str = explode(',', (str_replace(array(
				'[',
				']'
			) , '', $product)));

			$product__strA = $product__str;

			$key__value = '"' . ($options[$key]) . '"';

			$flag = true;

			$product__strA = array_unique($product__strA);

            if ($full_debug) 
            {
                echo "<br/>product__strA: <pre>";
                print_r($product__strA);
                echo "</pre><br/>";
            }

			$product__strA = array_map('no__element', $product__strA);

			$products__options__new[$key] = '[' . implode($product__strA, ',') . ']';

			$product__sql__str .= $zap . '`' . $key . '` = \'[' . implode($product__strA, ',') . ']\'';

		}

	}

	$sql__str = "UPDATE `Nbu44_jFtc446Cdm_ms2_products` SET {$product__sql__str} WHERE id = {$data_id}";
    if ($full_debug) { echo '<br/><br/>sql__str: ' . $sql__str . '<br/><br/>'; } 
    else 
    { 
        /*echo '<br/>У продукта "' . $name . '" обновлены свойства.<pre>'; 
        print_r(json_decode($sql__str)); 
        echo '</pre>';*/
    }
	
}

$log_text .= logFormat('Обработано offers: ' . $counter . '/' . $offers__count[0]['COUNT(*)'] . ' шт.');

$sql = "SELECT offer.data_id,offer.count FROM Nbu44_jFtc446Cdm_msync_offers offer, Nbu44_jFtc446Cdm_site_content products WHERE products.id = offer.data_id and offer.uuid_1c NOT LIKE '%#%'";
$offers_products_str = $modx->query($sql);

$log_text .= logFormat('Запрос: ' . $sql);

$offers_products = $offers_products_str->fetchAll(PDO::FETCH_KEY_PAIR);

// echo '<br/><pre>'; print_r($offers_products); echo '</pre>';

foreach ($modifications as $rid => $modification) {
	if ($modx->call('msopModification', 'saveProductModification', array(&$modx,
		$rid,
		$modification
	))) {
        // echo '<br/>Для товара с id = ' . $rid . ' сохранено модификаций: ' . count($modification). '. ';
    } else {
        // echo '<br/>Неудачная попытка сохранить модификации (' . count($modification) . ') для товара с id = ' . $rid . '. ';
	}
	$product_count = $offers_products[$rid];
	foreach ($modification as $mod) {
		$product_count += intval($mod['count']);
	}
	$product_page = $modx->getObject('modResource', $rid);
	if (!$product_page->setTVValue('count', $product_count)) {
		$log_text .= logFormat('Неудачная попытка обновить кол-во товара с id = ' . $rid . ' до значения ' . $product_count);
	} else {
		$log_text .= logFormat('Кол-во товара с id = ' . $rid . ' обновлено до значения: ' . $product_count);
	}
    // echo '<br/>Кол-во: ' . $product_count . '<br/><pre>'; print_r($modification); echo '</pre>';
}

// $modx->getObject('modResource', 123);

$modx
	->cacheManager
    ->refresh();

$log_text .= logFormat('Кэш очищен.');
file_put_contents($log_file, $log_text);

echo '<p>Обработка завершена.</p>';