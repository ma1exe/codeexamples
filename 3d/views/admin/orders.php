<?

    $sql = "SELECT * from " . DB_TABLE_ORDERS;

    if ($result = $mysqli->query($sql)): 
?>

<table class="table">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Дата</th>
            <th scope="col">Ссылка</th>
        </tr>
    </thead>
    <tbody>
        <? while ($order = $result->fetch_assoc()): ?>
            <tr>
                <th scope="row"><?= $order['id']; ?></th>
                <td><?= $order['date']; ?></td>
                <td><a href="/admin/order_view<?= SITE_URLS_SUFFIX; ?>?id=<?= $order['id']; ?>">просмотр</a></td>
            </tr>
    <? endwhile; ?>
        </tbody>
    </table>
<? endif; ?>