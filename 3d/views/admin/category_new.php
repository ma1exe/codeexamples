<?

    $result_category = false;

    if (isset($_POST['submit'])) {
        // Если нет ошибок
        if (!empty($_POST['title']) && intval($_POST['price']) >= 0 && !empty($_POST['alias'])) {

            $form = $_POST;

            if (!empty($form['meta_title'])) { 
                $meta_title = htmlspecialchars($form['meta_title']);
            } else {
                $meta_title = NULL;
            }

            if (!empty($form['meta_description'])) { 
                $meta_description = htmlspecialchars($form['meta_description']);
            } else {
                $meta_description = NULL;
            }

            if (!empty($form['meta_keywords'])) { 
                $meta_keywords = htmlspecialchars($form['meta_keywords']);
            } else {
                $meta_keywords = NULL;
            }

            unset($form['submit'], $form['meta_title'], $form['meta_description'], $form['meta_keywords']);

            $form['alias'] = makeAlias($form['alias']);

            $sql = "INSERT INTO " . DB_TABLE_ALIASES . " (`alias`, `type`, `meta_title`, `meta_description`, `meta_keywords`) VALUES ('" . $form['alias'] . "', 'category', '" . $meta_title . "', '" . $meta_description . "', '" . $meta_keywords . "')";

            if ($mysqli->query($sql)) {

                unset($form['alias']);
                $form['alias_id'] = $mysqli->insert_id;

                $sql = makeSqlInsert(DB_TABLE_CATEGORIES, $form);
            
                if ($result = $mysqli->query($sql)) {
                    addFlash('<p>Категория добавлена!</p>'); 
                } else {
                    addFlash('<p>Категория не добавлена: произошла ошибка при работе с базой данных.</p>');
                    addFlash('<p>' . $mysqli->error . '</p>');
                }
                
                // обновление страницы
                //header("Location: " . $_SERVER['REQUEST_URI']);
            } else {
                addFlash('<p>Категория не добавлена: ошибка при добавлении алиаса.</p>');
            }

        } else {
            addFlash('<p>При добавлении категории произошли следующие ошибки:<ul>');

            if (empty($_POST['title'])) {
                addFlash('<li>Название категории отсутствует.</li>');
            }

            if (empty($_POST['alias'])) {
                addFlash('<li>Алиас не может быть пустым.</li>');
            }

            addFlash('</ul></p>');
        }
    }
    if (getFlash()) {
        echo getFlash();
        clearFlash();
    }

    require('./views/admin/forms/category.php');
?>
