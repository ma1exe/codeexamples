
<div class="content-block content-block-header">
    <h1>Авиационно-спортивный клуб &laquo;Полет102&raquo;</h1>
</div>
<div class="content-block content-block-info">
    <p>Ознакомительные полёты на мотопараплане<br>
        Обучение самостоятельным полётам на мотопараплане<br>
        Помощь в подборе авиа-мото техники, оборудования</p>
    <p>Респ. Башкортостан, г. Белорецк</p>
    <p><a class="content-block-link" href="tel:+79603818912">+7 960 381 89 12</a></p>
    <p><a class="content-block-link" href="mailto:info@polet102.ru">info@polet102.ru</a></p>
</div>
<div class="content-block">
    <h2>Чем мы занимаемся</h2>
    <div class="content-block-services">
        <div class="content-block-service content-block-service-training">
            <h2>Обучение полетам на мотопараплане</h2>
            <p style="display: none;"></p>
            <p style="display: none;"><a class="content-block-link" href="javascript:void();">Записаться</a></p>
        </div>
        <div class="content-block-service content-block-service-flights">
            <h2>Полеты над Белой</h2>
            <p style="display: none;"></p>
            <p style="display: none;"><a class="content-block-link" href="javascript:void();">Записаться</a></p>
        </div>
    </div>
</div>
<div class="content-block">
    <p>Наш клуб не является коммерческой организацией.<br/>
    Обучение и приобретение спортивного оборудования проводится за счет собственных средств, энтузиастов и любителей неба.<br/>
    Приглашаем спонсоров, инвесторов и рекламодателей, а также собираем пожертвования на развитие клуба.</p>
</div>
<div class="content-block content-block-map">
    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A4f9f223001e532b659dfc69c11c10a51977a774254e3b9e12706ffec79b65e6a&amp;width=100%25&amp;height=750&amp;lang=ru_RU&amp;scroll=true"></script>
</div>
<div class="content-block">
    <p><a href="/">Вернуться на сайт</a>.</p>
</div>