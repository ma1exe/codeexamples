<?

	$sql = 'SELECT * FROM ' . DB_TABLE_PAGES_EXTRA . ' WHERE id = 2';
	$result_footer = $mysqli->query($sql);
	$footer_extra = $result_footer->fetch_assoc();

?>

</div>
<aside class="span3 sidebar pull-left">
				<section class="widget inner categories-widget">
					<h3 class="widget-title">Каталог</h3>
					<ul class="icons clearfix" id="CatNavi">
						<? foreach ($categories as $category): ?>
							<li class="CatLevel0"><a href="/<?= $category['alias']; ?>"><?= $category['title']; ?></a></li>
						<? endforeach; ?>
					</ul>
				</section>
				<section class="widget inner categories-widget">
					<?= htmlspecialchars_decode($footer_extra['blocks_1']); ?>
				</section>
			</aside>
        </div>
	</div>
</div>

<footer id="footer">
	<?= htmlspecialchars_decode($footer_extra['blocks_2']); ?>
</footer>

<link href="/web/css/colorbox.css" rel="stylesheet" type="text/css"/>
<link href="/web/css/artline-packed.css" rel="stylesheet" type="text/css"/>
<link href="/web/css/styles.css" rel="stylesheet" type="text/css"/>

<script src="/web/js/jquery.js"></script>
<script src="/web/js/jquery.colorbox-min.js"></script>
<script src="/web/js/jquery.colorbox-ru.js"></script>
<script src="/web/js/artline-packed.js"></script>
<script src="/web/js/scripts.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-22323372-1', 'auto');
  ga('require', 'displayfeatures');  
  ga('send', 'pageview');

</script>
<!-- Yandex.Metrika counter -->
<script>
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(11442823, "init", {
        id:11442823,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        trackHash:true,
        ecommerce:"dataLayer"
   });
</script>
<!-- /Yandex.Metrika counter -->
</body>
</html>