<?

    function addFlash($text) {
        if (!isset($_SESSION['flash'])) {
            $_SESSION['flash'] = $text;
        } else {
            $_SESSION['flash'] .= $text;
        }
    }


    function getFlash() {
        if (isset($_SESSION['flash'])) {
            return $_SESSION['flash'];
        }
        return false;
    }


    function clearFlash() {
        if (isset($_SESSION['flash'])) {
            unset($_SESSION['flash']);
        }
    }


    function makeSqlInsert($table, $arr) {
        $keys = [];
        $values = [];

        foreach ($arr as $key => $value) {
            $keys[] = '`' . $key . '`';
            $values[] = "'" . htmlspecialchars($value) . "'";
        }

        $sql = "INSERT INTO " . $table . " (" . implode(', ', $keys) . ") VALUES (" . implode(', ', $values) . ")";

        return $sql;
    }

    function makeSqlUpdate($table, $arr, $id) {
        $update_values = [];

        foreach ($arr as $key => $value) {
            $val = is_numeric($value) ? $value : "'" . htmlspecialchars($value) . "'";
            $update_values[] = '`' . $key . '` = ' . $val;
        }

        $sql = "UPDATE " . $table . " SET " . implode(', ', $update_values) . " WHERE id = " . $id;

        return $sql;
    }


    function makeAlias($alias) {
        $alias = trim($alias);
        $alias = str_replace(' ', '-', $alias); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-\_\.]/', '', $alias); // Removes special chars.
    }


    function checkImageExists($image) {
        return ($image) ? $image : '/web/images/no_photo.png';
    }


    function generateHashString($length = 16) {
        return bin2hex(random_bytes($length));
    }

    function generatePriceString($price = 0, $rate = 1, $symbol = 'руб.') {
        return ceil($price / $rate * 100) / 100 . ' ' . $symbol;
    }