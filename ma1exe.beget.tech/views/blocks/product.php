<?php 
    use yii\helpers\Url;

    $image_url = $this->context->isImageExists($product['image']);
    $price_converted = $this->context->convertPriceWithRate($product['price'], $product->currency->rate);
?>

<div class="product">
    <div class="product-img">
        <img class="img-responsive" src="<?= $image_url; ?>" alt="<?= $product['name']; ?>">
    </div>
    <div class="product-body">
        <p class="product-category"><?= $product->category->name; ?></p>
        <h3 class="product-name">
            <a href="<?= Url::toRoute('catalog/'.$product->category->link.'/'.$product['id']); ?>">
                <?= $product['name']; ?>
            </a>
        </h3>
        <h4 class="product-price"><?= $this->context->formatPriceToRUB($price_converted); ?> руб.</h4>
    </div>
    <div class="add-to-cart">
        <button class="add-to-cart-btn" data-product-id="<?= $product['id']; ?>"><i class="fa fa-shopping-cart"></i> в корзину</button>
    </div>
</div>