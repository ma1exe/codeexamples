<?php 
    use yii\helpers\Url;
    use yii\widgets\LinkPager; 

    $this->title = 'Поиск';
    $this->params['breadcrumbs'][] = $this->title;

    $products_chunks = array_chunk($products, 4, TRUE);
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php if ($products): ?>
                <?php foreach($products_chunks as $products): ?>
                    <div class="row">
                        <?php foreach($products as $product): ?>
                            <div class="col-xs-6 col-md-3">
                                <?php 
                                    $product_category = $categories[$product['category_id']];
                                    $product_currency = $currencies[$product['currency_id']];
                                ?>
                                <?= $this->render('//blocks/product', compact('product', 'product_category', 'product_currency')); ?>
                            </div>
                        <? endforeach; ?>
                    </div>
                <? endforeach; ?>
            <?php else: ?>
                <p>Ничего не найдено, либо введенный поисковый запрос короче 3 символов.</p>
            <?php endif; ?>
        </div>
    </div>
</div>