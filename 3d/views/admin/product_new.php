<?

    $result_product = false;

    $allowed_image_extensions = ['jpg', 'jpeg', 'png', 'gif'];
    $allowed_image_extensions_str = [];

    foreach ($allowed_image_extensions as $ext) {
        $allowed_image_extensions_str[] = '.' . $ext;
    }

    $allowed_image_extensions_str = implode(', ', $allowed_image_extensions_str);

    if (isset($_POST['submit'])) {
        // Если нет ошибок
        if (!empty($_POST['title']) && intval($_POST['price']) >= 0 && !empty($_POST['alias'])) {

            $form = $_POST;

            if (isset($_FILES['image']['size']) && intval($_FILES['image']['size'])) {
                $file_name = basename($_FILES['image']['name']);
                $file_info = pathinfo($file_name);

                if (in_array(strtolower($file_info['extension']), $allowed_image_extensions)) {
                    $file_new_name = $file_info['filename'];

                    $file_new_name .= '.' . $file_info['extension'];

                    $file_path = $_SERVER['DOCUMENT_ROOT'] . '/web/images/products/' . $file_new_name;

                    if (move_uploaded_file($_FILES['image']['tmp_name'], $file_path)) {
                        $form['image'] =  '/web/images/products/' . $file_new_name;
                    } else {
                        unset($form['image']);
                    }
                } else {
                    addFlash('<p>Допустимые расширения изображения: ' . implode(', ', $allowed_image_extensions_str) . '</p>'); 
                    unset($form['image']);
                }
            }

            $images_extra = [];

            if (isset($_FILES['images_extra'])) {
                $images_extra_total = count($_FILES['images_extra']['name']);
                $images_extra_errors = false;

                for( $i=0 ; $i < $images_extra_total ; $i++ ) {
                    $file_name = basename($_FILES['images_extra']['name'][$i]);
                    $file_info = pathinfo($file_name);

                    if (in_array(strtolower($file_info['extension']), $allowed_image_extensions)) {

                        $file_new_name .= $file_info['filename'] . '.' . $file_info['extension'];
        
                        $file_path = $_SERVER['DOCUMENT_ROOT'] . '/web/images/products/' . $file_new_name;
    
                        if (move_uploaded_file($_FILES['images_extra']['tmp_name'][$i], $file_path)) {
                            $images_extra[] = '/web/images/products/' . $file_new_name;
                        } else {
                            $images_extra_errors = false;
                        }
                    } else {
                        $images_extra_errors = false;
                    }
                }

                if ($images_extra_errors) {
                    addFlash('<p>При загрузке доп. изображений произошла одна или несколько ошибок.</p>'); 
                }
            }

            if ($images_extra) {
                $form['images_extra'] = implode(',', $images_extra);
            } else {
                unset($form['images_extra']);
            }

            if (!empty($form['meta_title'])) { 
                $meta_title = htmlspecialchars($form['meta_title']);
            } else {
                $meta_title = NULL;
            }

            if (!empty($form['meta_description'])) { 
                $meta_description = htmlspecialchars($form['meta_description']);
            } else {
                $meta_description = NULL;
            }

            if (!empty($form['meta_keywords'])) { 
                $meta_keywords = htmlspecialchars($form['meta_keywords']);
            } else {
                $meta_keywords = NULL;
            }

            // Рекомендуемые
            if (!empty($form['products_related'])) {
                $products_related = $form['products_related'];
            } else {
                $products_related = false;
            }

            unset($form['submit'], $form['image_name'], $form['image_name_update'], $form['meta_title'], $form['meta_description'], $form['meta_keywords'], $form['products_related']);

            $form['alias'] = makeAlias($form['alias']);

            $sql = "INSERT INTO " . DB_TABLE_ALIASES . " (`alias`, `type`, `meta_title`, `meta_description`, `meta_keywords`) VALUES ('" . $form['alias'] . "', 'product', '" . $meta_title . "', '" . $meta_description . "', '" . $meta_keywords . "')";

            if ($mysqli->query($sql)) {
                unset($form['alias'], $meta_title, $meta_description, $meta_keywords);
                $form['alias_id'] = $mysqli->insert_id;

                $sql = makeSqlInsert(DB_TABLE_PRODUCTS, $form);
            
                if ($result = $mysqli->query($sql)) {
                    addFlash('<p>Товар добавлен!</p>');
                    
                    if ($products_related) {
                        $rel_products_str = [];
                        foreach ($products_related as $rel_product) {
                            $rel_products_str[] = '(' . intval($mysqli->insert_id) . ', ' . intval($rel_product) . ')';
                        }
                        $sql = "INSERT INTO " . DB_TABLE_PRODUCTS_RELATED . " VALUES " . implode(', ', $rel_products_str);
                        if (!$mysqli->query($sql)) {
                            addFlash('<p>Рекомендуемые продукты не добавлены: произошла ошибка при записи данных.</p>'); 
                        }
                    }
                    
                    $is_reload_required = true;
                } else {
                    addFlash('<p>Товар не добавлен: произошла ошибка при работе с базой данных.</p>');
                    addFlash('<p>' . $mysqli->error . '</p>');
                }
            } else {
                addFlash('<p>Товар не добавлен: ошибка при добавлении алиаса и meta-тегов.</p>');
            }
            
        } else {
            addFlash('<p>При добавлении товара произошли следующие ошибки:<ul>');

            if (empty($_POST['title'])) {
                addFlash('<li>Название товара отсутствует.</li>');
            }
            
            if (intval($_POST['price']) < 0) {
                addFlash('<li>Цена товара не может быть отрицательным числом.</li>');
            }

            if (empty($_POST['alias'])) {
                addFlash('<li>Алиас не может быть пустым.</li>');
            }

            addFlash('</ul></p>');
        }
    }

    if (getFlash() && !$is_reload_required) {
        echo getFlash();
        clearFlash();
    }

    require('./views/admin/forms/product.php');
?>
