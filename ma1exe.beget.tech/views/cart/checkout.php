<?php 
    use yii\helpers\Url;
    use yii\widgets\LinkPager; 

    $this->params['breadcrumbs'][] = [
        'label' => 'Корзина',
        'url' => Url::toRoute('cart/index')
    ];

    $this->title = 'Оформить заказ';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="col-md-12">

        <div class="row">
            <div class="col-xs-12">
                <h1><?= $this->title; ?></h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <!-- Billing Details -->
                <div class="billing-details">
                    <div class="section-title">
                        <h3 class="title">Billing address</h3>
                    </div>
                    <div class="form-group">
                        <input class="input" type="text" name="first-name" placeholder="First Name">
                    </div>
                    <div class="form-group">
                        <input class="input" type="text" name="last-name" placeholder="Last Name">
                    </div>
                    <div class="form-group">
                        <input class="input" type="email" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input class="input" type="text" name="address" placeholder="Address">
                    </div>
                    <div class="form-group">
                        <input class="input" type="text" name="city" placeholder="City">
                    </div>
                    <div class="form-group">
                        <input class="input" type="text" name="country" placeholder="Country">
                    </div>
                    <div class="form-group">
                        <input class="input" type="text" name="zip-code" placeholder="ZIP Code">
                    </div>
                    <div class="form-group">
                        <input class="input" type="tel" name="tel" placeholder="Telephone">
                    </div>
                </div>
                <!-- /Billing Details -->

                <!-- Order notes -->
                <div class="order-notes">
                    <textarea class="input" placeholder="Order Notes"></textarea>
                </div>
                <!-- /Order notes -->
            </div>

            <!-- Order Details -->
            <div class="col-md-5 order-details">
                <div class="section-title text-center">
                    <h3 class="title">Ваш заказ</h3>
                </div>
                <div class="order-summary">
                    <div class="order-col">
                        <div><strong>ТОВАР</strong></div>
                        <div><strong>СУММА</strong></div>
                    </div>
                    <div class="order-products">
                        <?php foreach ($this->context->cart_products as $cart_product): ?>
                            <div class="order-col">
                                <div><?= $cart_product['count']; ?>x <?= $cart_product['name']; ?></div>
                                <div><?= $this->context->formatPriceToRUB($cart_product['price']); ?> руб.</div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="order-col">
                        <div>Доставка</div>
                        <div><strong>уточняется после заказа</strong></div>
                    </div>
                    <div class="order-col">
                        <div><strong>Сумма</strong></div>
                        <div><strong class="order-total">0 руб.</strong></div>
                    </div>
                </div>
                <div class="payment-method">
                    <div class="input-radio">
                        <input type="radio" name="payment" id="payment-1">
                        <label for="payment-1">
                            <span></span>
                            PayPal
                        </label>
                        <div class="caption">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                <div class="input-checkbox">
                    <input type="checkbox" id="terms">
                    <label for="terms">
                        <span></span>
                        I've read and accept the <a href="#">terms & conditions</a>
                    </label>
                </div>
                <a href="#" class="primary-btn order-submit">Подтвердить</a>
            </div>
            <!-- /Order Details -->
        </div>
    </div>
</div>