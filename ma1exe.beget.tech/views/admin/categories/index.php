<?php

    use yii\helpers\Url;
    use yii\widgets\LinkPager;

?>

<div class="row">
    <div class="col-xs-12">
        <h2>Категории</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Название</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <?php foreach ($categories as $category): ?>
                <tr>
                    <td><?= $category['name']; ?></td>
                    <td><a href="<?= Url::toRoute('admin/categories/update/'.$category['id']); ?>">Редактировать</a></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?= LinkPager::widget([
    'pagination' => $pages,
]); ?>