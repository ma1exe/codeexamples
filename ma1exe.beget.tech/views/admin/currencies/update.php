<?php 
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
    use app\models\CatalogCategories;
    use app\models\CatalogManufacturers;
    use app\models\CatalogCurrencies;
?>

<?php $form = ActiveForm::begin([
    'id' => 'catalog-product',
]); ?>

    <?php if(Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= Yii::$app->session->getFlash('success'); ?>
        </div>
    <?php endif;?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'name'); ?>

    <?= $form->field($model, 'sku'); ?>

    <?= $form->field($model, 'description')->textarea(); ?>

    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(CatalogCategories::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'manufacturer_id')->dropDownList(ArrayHelper::map(CatalogManufacturers::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'price'); ?>

    <?php if ($model->currency_id > 1): ?>

        <p><?= $this->context->formatPriceToRUB($this->context->convertPriceWithRate($model->price, $model->currency->rate));  ?> руб.</p>

    <?php endif; ?>

    <?= $form->field($model, 'currency_id')->dropDownList(ArrayHelper::map(CatalogCurrencies::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'updated')->textInput(['readonly'=> true])?>

    <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>