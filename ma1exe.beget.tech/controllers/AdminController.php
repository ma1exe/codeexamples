<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\models\LoginForm;
use app\models\CatalogCurrencies;
use app\models\CatalogProducts;
use app\models\CatalogCategories;
use app\models\CatalogManufacturers;
use yii\data\Pagination;

class AdminController extends GlobalController
{
    public $layout = 'admin';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() 
    {
        $currencies = CatalogCurrencies::find()->where(['>', 'id', 1])->all();

        $products_query = CatalogProducts::find();
        $pages = new Pagination(['totalCount' => $products_query->count()]);
        $products = $products_query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $categories = CatalogCategories::find()->all();

        return $this->render('index', compact('currencies', 'categories', 'pages'));
    }


    public function actionProducts() {
        $products_query = CatalogProducts::find();
        $pages = new Pagination(['totalCount' => $products_query->count()]);
        $products = $products_query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('products/index', compact('products', 'pages'));
    }


    public function actionProductsUpdate($id = 0) {        
        if ($id && intval($id)) {
            $model = CatalogProducts::findOne($id);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success', 'Товар обновлен.');
                $model = CatalogProducts::findOne($id);
            }
            return $this->render('products/update', compact('model'));    
        }

        $this->redirect(['admin/products']);
    }


    public function actionCategories() {
        $categories_query = CatalogCategories::find();
        $pages = new Pagination(['totalCount' => $categories_query->count()]);
        $categories = $categories_query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('categories/index', compact('categories', 'pages'));
    }


    public function actionCategoriesUpdate($id = 0) {
        if ($id && intval($id)) {
            $model = CatalogCategories::findOne($id);
            $category = CatalogCategories::find($id)->asArray()->one();
            return $this->render('categories/update', compact('category', 'model'));    
        }

        $this->redirect(['admin/categories']);
    }


    public function actionCurrencies() {
        $currencies_query = CatalogCurrencies::find();
        $pages = new Pagination(['totalCount' => $currencies_query->count()]);
        $currencies = $currencies_query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('currencies/index', compact('currencies', 'pages'));
    }


    public function actionCurrenciesUpdate() {
        $currencies = CatalogCurrencies::find()->where(['>', 'id', 1])->asArray()->all();
        
        $json = file_get_contents('https://www.cbr-xml-daily.ru/daily_json.js');
        $obj = json_decode($json);
        $currencies_rates = [];

        foreach ($obj->Valute as $currency) {
            $currencies_rates[$currency->CharCode] = $currency->Value;
        }
        
        foreach ($currencies as $currency) {
            if (array_key_exists($currency['code'], $currencies_rates)) {
                $currency_new = CatalogCurrencies::findOne($currency['id']);
                $currency_new->rate = ceil($currencies_rates[$currency['code']] * 100) / 100;
                $currency_new->save();
            }
        }

        $this->redirect(['admin/currencies']);
    }


    public function actionManufacturers() {
        $manufacturers_query = CatalogManufacturers::find();
        $pages = new Pagination(['totalCount' => $manufacturers_query->count()]);
        $manufacturers = $manufacturers_query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('manufacturers/index', compact('manufacturers', 'pages'));
    }


    public function actionManufacturersUpdate($id = 0) {        
        if ($id && intval($id)) {
            $model = CatalogManufacturers::findOne($id);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success', 'Товар обновлен.');
                $model = CatalogManufacturers::findOne($id);
            }
            return $this->render('manufacturers/update', compact('model'));    
        }

        $this->redirect(['admin/manufacturers']);
    }


    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}