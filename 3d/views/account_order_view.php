<? if (isset($_GET['id']) && intval($_GET['id']) > 0): ?>
    <? 
        $result = $mysqli->query('
            SELECT
                *
            FROM
                ' . DB_TABLE_ORDERS . '
            WHERE
               id = ' . intval($_GET['id']) . ' AND user_id = ' . $user['id']
        );
    ?>

    <? if ($result && $result->num_rows): ?>
        <? if ($order = $result->fetch_assoc()): ?>
            <div>
                <h1>Заказ №<?= $order['id']; ?></h1>
                <h4>Товары: </h4>
                <? 
                    $result_order_products = $mysqli->query('
                        SELECT
                            op.product_id,
                            p.title,
                            a.alias
                        FROM
                            ' . DB_TABLE_ORDERS_PRODUCTS . ' op
                        LEFT OUTER JOIN ' . DB_TABLE_PRODUCTS . ' p ON
                            op.product_id = p.id
                        LEFT OUTER JOIN ' . DB_TABLE_ALIASES . ' a ON
                            p.alias_id = a.id
                        WHERE
                            order_id = ' . $order['id']
                    );

                    if ($result_order_products && $result_order_products->num_rows):
                ?>
                    <? while ($order_product = $result_order_products->fetch_assoc()): ?>
                        <p><a href="/<?= $order_product['alias']; ?>">1 x <?= $order_product['title']; ?></a></p>
                    <? endwhile; ?>
                <? endif; ?>

                <hr>

                <? if ($order['comment']): ?>
                    <h4>Комментарий: <?= $order['comment']; ?></h4>
                    <hr>
                <? endif; ?>
                
                <h4>Сумма: <?= generatePriceString($order['total_sum'], $current_rate, $current_symbol); ?></h4>
                <hr>

                <h4>Дата: <?= $order['date']; ?></h4>

                <p>
                    <a class="button" href="/account_orders<?= SITE_URLS_SUFFIX; ?>">
                        <span>
                            <img src="/web/images/icons/back.png" alt="К списку заказов" title=" К списку заказов " width="12" height="12">&nbsp;К списку заказов
                        </span>
                    </a>
                </p>
            </div>
        <? endif; ?>

    <? else: ?>

        <h1>Заказ не найден</h1>

    <? endif; ?>

<? else: ?>

    <h1>Заказ не найден</h1>

<? endif; ?>