<? 
    $sql = "SELECT * from " . DB_TABLE_ORDERS_PRODUCTS . " op LEFT OUTER JOIN " . DB_TABLE_PRODUCTS . " p ON p.id = op.product_id WHERE op.order_id = " . intval($_GET['id']);

    $result_products = $mysqli->query($sql);
?>

<h1>Заказ №<?= $_GET['id']; ?></h1>

<div class="row">
    <div class="col-12">
        <p>Пользователь №<?= $order['user_id']; ?>, <?= $order['last_name']; ?> <?= $order['first_name']; ?>.</p>
    </div>
</div>

<? if ($order['comment']): ?>

    <div class="row">
        <div class="col-12">
            <p>Комментарий к заказу: <?= htmlspecialchars_decode($order['comment']); ?></p>
        </div>
    </div>

<? endif; ?>

<div class="row">
    <div class="col-12">
        <h2>Товары</h2>
    </div>
</div>

<br>

<table class="table">
    <thead>
        <tr>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
        </tr>
    </thead>
    <tbody>
        <? while ($product = $result_products->fetch_assoc()): ?>
            <tr>
                <th scope="row"><?= $product['title']; ?></th>
                <td><?= $product['price']; ?></td>
            </tr>
        <? endwhile; ?>
    </tbody>
</table>

<div class="row">
    <div class="col-12">
        <p>Общая стоимость: <?= $order['total_sum']; ?> руб.</p>
    </div>
</div>