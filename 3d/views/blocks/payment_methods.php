<p style="text-align:center;"><span style="/*color:#1a2841;font-family:arial,helvetica,sans-serif;font-size:small;*/">Оплатить 3d модель или заказ Вы можете без оформления на сайте. Выбирите товар и оплатите любым способом общую сумму. После оплаты сообщите нам за какую 3d модель Вы оплатили, укажите сумму и способ оплаты, после чего вам придет ссылка для скачивания архива с 3d моделями.</span></p>
<p style="text-align:center;"><span style="/*font-size:small;*/">e-mail: <strong>3d-model-stl@mail.ru</strong>&nbsp; &nbsp;тел.:<strong>+7-958-646-12-55</strong><!--&nbsp; &nbsp;&nbsp;Skype: <strong></strong>--></span></p>
<p style="text-align:center;"><span style="color:#1a2841;font-family:arial,helvetica,sans-serif;font-size:large;"><strong>Вы можете оплатить 3d модель любым способом:</strong></span></p>
<table style="width:auto;margin-left:auto;margin-right:auto;background-color:#fff;" border="0">
    <tbody>
        <tr>
            <td style="width:289.875px;" align="center">
                <p><strong>Перевод на карту</strong></p>
                <p><a href="http://www.sbrf.ru/" target="_blank" rel="noopener"><img src="http://artline3d.ru/templates/moi-shablon/img/logo-sberbank.gif" alt="СБЕРБАНК" width="160" height="57" longdesc="http://www.alfabank.ru/"><span class="стиль3" style="font-size:xx-small;">www.sbrf.ru</span></a></p>
            </td>
            <td style="width:975.875px;">
                <p><strong>Номер карты Сбербанка 4276 0800 1003 7464</strong></p>
                <p>карта на имя;&nbsp;<span class="стиль1">Николай Ильич Б.</span><br>(карта привязана&nbsp; к номеру телефона;  +7 958 646 12 55)<br>(можно перевести через сбербанк Онлайн, банкомат <br>или отделение сбербанка на этот номер карты)</p>
                <p><strong>&nbsp;</strong></p>
            </td>
        </tr>
        <tr>
            <td style="background-color:#fff;width:289.875px;">
                <p><a title="yandex-money" href="https://money.yandex.ru/" target="_blank" rel="noopener"><img src="http://artline3d.ru/templates/moi-shablon/img/logo_yandex.gif" alt="yandex-money" width="170" height="65"></a></p>
                <p><span style="font-size:small;"><strong>Оплата банковской картой</strong></span></p>
                <p><strong>&nbsp;</strong></p>
                <p>&nbsp;</p>
            </td>
            <td style="background-color:#fff;width:975.875px;">
                <p><iframe src="https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=3d-model-stl.com&targets-hint=Укажите название модели, величину платежа, Ваш e-mail&default-sum=&button-text=12&payment-type-choice=on&mobile-payment-type-choice=on&comment=on&mail=on&hint=Укажите название модели сумму платежа ваш e mail&successURL=3d-model-stl.com&quickpay=shop&account=410012512757071" width="100%" height="316" frameborder="0" allowtransparency="true" scrolling="no"></iframe></p>
            </td>
        </tr>
        <tr>
            <td style="width:289.875px;" align="center" height="79"><a title="qiwi" href="http://www.qiwi.ru/" target="_blank" rel="noopener"><img style="display:block;margin-left:auto;margin-right:auto;" src="http://artline3d.ru/templates/moi-shablon/img/qiwi_logo.gif" alt="qiwi" width="150" height="72"></a><span style="font-size:xx-small;"><a class="стиль3" href="http://qiwi.com/n/GOODE671">www.qiwi.ru</a></span></td>
            <td style="width:975.875px;">
                <p>Никнейм QIWI Кошелька: <strong>GOODE671</strong></p>
                <p>QIWI честно показывает, сколько вам придется доплатить при оплате каждым из способов.</p>
                <p>Точную сумму к оплате вы увидите на кнопке «Оплатить»</p>
                <p>Самые выгодные способы оплаты — с кошелька или с терминала, при этом мы не берем никаких дополнительных комиссий.</p>
                <p>Обычно с карты вам придется доплатить 1% комиссии, а при оплате с баланса телефона от 1% в зависимости от услуги и условий оператора.</p>
            </td>
        </tr>
        <tr>
            <td style="width:289.875px;" align="center" height="79">
                <p><span style="font-size:small;"><strong>Переводы с карты на карту</strong></span></p>
                <p><span style="font-size:small;"><strong>Visa, MasterCard.</strong></span></p>
                <p><a title="Переводы с карты на карту" href="https://www.alfaportal.ru/card2card/ptpl/alfaportal/initial.html" target="_blank" rel="noopener"><img src="http://alfabank.ru/f/1/global/logo-common.gif" alt="" width="160" height="57" longdesc="http://www.alfabank.ru/"></a><span style="font-size:xx-small;"><a class="стиль3" href="https://www.alfaportal.ru/card2card/ptpl/alfaportal/initial.html" target="_blank" rel="noopener">www.alfaportal.ru</a></span></p>
            </td>
            <td style="width:975.875px;">
                <p><strong>Переводы с карты на карту</strong><br>Вы можете осуществить перевод, используя карты Visa, MasterCard, Maestro<br><span style="color:#e03e2d;"><strong><a style="color:#e03e2d;" title="Переводы с карты на карту" href="https://www.alfaportal.ru/card2card/ptpl/alfaportal/initial.html" target="_blank" rel="noopener">по этой ссылке</a></strong></span><br>Номер карты получателя<br><strong>4276 0800 1003 7464</strong></p>
            </td>
        </tr>
        <tr>
            <td style="width:289.875px;" align="center" valign="top" height="21">
                <p><strong>&nbsp;</strong><a title="PayPal" href="https://www.paypal.com" target="_blank" rel="noopener">
                <img title="PayPal" src="http://artline3d.ru/templates/moi-shablon/img/logo_paypal.gif" alt="PayPal" width="200" height="50">
                </a><span style="font-size:xx-small;">
                <a class="стиль3" href="https://www.paypal.com/" target="_blank" rel="noopener">www.paypal.com</a>
                </span>
                </p>
            </td>
            <td style="width:975.875px;">
                <p>&nbsp;<span style="font-size:small;">Счет PayPal:&nbsp;<strong>nbrilev@gmail.com</strong> &nbsp;</span> &nbsp;</p>
                <p><span style="color:#000;">(оплата на счет PayPal)</span></p>
            </td>
        </tr>
        <tr>
            <td style="width:289.875px;">
                <p>&nbsp;</p>
            </td>
            <td style="width:975.875px;">&nbsp;</td>
        </tr>
        <tr>
            <td style="width:289.875px;" align="center" height="79">&nbsp;</td>
            <td style="width:975.875px;">&nbsp;</td>
        </tr>
    </tbody>
</table>