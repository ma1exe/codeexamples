<?

    $success = false;

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $action = htmlspecialchars($_POST['action']); 

        switch ($action) {
            case 'add':
                $id = intval($_POST['id']);
                if (!in_array($id, $_SESSION['cart']['items'])) {
                    $_SESSION['cart']['items'][] = $id;
                    $success = true;
                }
                break;

            case 'remove':
                $id = intval(htmlspecialchars($_POST['id'])); 
                if (($key = array_search($id, $_SESSION['cart']['items'])) !== false) {
                    unset($_SESSION['cart']['items'][$key]);
                    $success = true;
                }
                break;

            /*case 'add':
                $id = intval($_POST['id']); 
                $quantity = intval($_POST['quantity']); 
                if (isset($_SESSION['cart']['items'][$id])) {
                    $_SESSION['cart']['items'][$id] += $quantity;
                } else {
                    $_SESSION['cart']['items'][$id] = $quantity;
                }
                echo json_encode(['success' => true]);
                break;*/
            
            default:
                break;
        }   
    }

    echo json_encode(['success' => $success]);