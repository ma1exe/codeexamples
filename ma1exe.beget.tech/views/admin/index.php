<?php

    use yii\helpers\Url;
    use yii\widgets\LinkPager;

?>

<div class="row">
    <div class="col-xs-12">
        <h1>Админка</h1>
        <?= ceil(21.0100000000001*100)/100; ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h2>Валюты</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Название</th>
                    <th>Символ</th>
                    <th>Курс ЦБ РФ</th>
                    <th>Обновлено</th>
                </tr>
            </thead>
            <?php foreach ($currencies as $currency): ?>
                <tr>
                    <td><?= $currency['name']; ?></td>
                    <td><?= $currency['symbol']; ?></td>
                    <td><?= $currency['rate']; ?></td>
                    <td><?= $currency['updated']; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h2>Категории</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Название</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <?php foreach ($categories as $category): ?>
                <tr>
                    <td><?= $category['name']; ?></td>
                    <td>Редактировать</td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

