<?php

    use yii\helpers\Url;

    $this->title = $product['name'];
    $this->params['breadcrumbs'][] = [
        'label' => 'Каталог',
        'url' => Url::toRoute('catalog/index')
    ];
    $this->params['breadcrumbs'][] = [
        'label' => $category['name'],
        'url' => Url::toRoute('catalog/'.$category['link'])
    ];
    $this->params['breadcrumbs'][] = $this->title;

    $price_converted = $this->context->convertPriceWithRate($product['price'], $product->currency->rate); 

?>

<h1><?= $this->title; ?></h1>
<img class="img-responsive" src="<?= $this->context->isImageExists($product['image']); ?>" alt="<?= $product['name']; ?>">
<p>Цена: <?= $this->context->formatPriceToRUB($price_converted); ?> руб.</p>