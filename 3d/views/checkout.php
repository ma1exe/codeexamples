<div class="page">
    <h1>Оформление заказа</h1>
    <div class="pageContent">
        <div id="load_status"></div>
        <div id="box">
            <div id="checkout">
                <form id="form_checkout" action="/ajax/order_confirm<?= SITE_URLS_SUFFIX; ?>" method="post">
                    <? if (!$user['id']): ?>
                        <p>Если Вы наш постоянный клиент, 
                            <b>
                                <a href="/login<?= SITE_URLS_SUFFIX; ?>">
                                    <u> введите Ваши персональные данные</u>
                                </a>
                            </b> для входа. Либо Вы можете оформить заказ прямо сейчас, заполнив форму ниже.
                        </p>
                        <div id="contact_box" class="sm_layout_box">
                            <fieldset class="form">
                                <h2>Контактная информация<input type="hidden" name="guest" value="guest"></h2>
                                <p><label for="firstname">Имя:</label> <input type="text" name="first_name" value="" id="firstname" class="valid" required>&nbsp;<span class="Requirement">*</span></p>
                                <!--<p><label for="lastname">Фамилия:</label> <input type="text" name="last_name" value="" id="lastname" required>&nbsp;<span class="Requirement">*</span></p>-->
                                <p><label for="email">Ваш E-Mail:</label> <input type="text" name="email" value="" id="email" required>&nbsp;<span class="Requirement">*</span></p>
                                <p>Ваш пароль для входа будет сгенерирован автоматически и отправлен на указанный email.</p>
                            </fieldset>
                        </div>
                    <? endif; ?>
                    <div id="shipping_modules_box" class="sm_layout_box">
                        <h2>Способ доставки</h2>
                        <div id="shipping_options">
                            <label class="shipping sogl selected" for="sogl">
                                <div class="shipping-method itemOdd">
                                    <p>&nbsp; Доставка ссылки для скачивания на почту указанную в платеже или при регистрации.<input type="hidden" name="shipping" value="sogl_sogl"></p>
                                </div>
                            </label>
                        </div>
                    </div>
                    <!--<div id="payment_options" class="sm_layout_box">
                        <h2>Способ оплаты</h2>
                        <label class="payment sberbank selected" for="sberbank">
                            <div class="payment-method itemEven">
                                <p>
                                    <input type="radio" name="payment" value="sberbank" checked="checked" id="sberbank" style="border: 1px inset rgb(0, 123, 255);"> 
                                    <span class="bold">Сбербанк</span>
                                </p>
                            </div>
                        </label>
                    </div>-->
                    <div id="comment_box" class="sm_layout_box">
                        <h2>Комментарии к Вашему заказу</h2>
                        <textarea name="comment" id="comment" cols="60" rows="5"></textarea>
                    </div>
                    <div class="sm_layout_box">
                        <h2>Товары</h2>
                        <? //print_r($cart); ?>
                        <? foreach ($cart['items'] as $id => $item): ?>
                            <input type="hidden" name="items[<?= $id; ?>]" value="1">
                            <p>1 x <?= $item['title']; ?>, <?= generatePriceString($item['price'], $current_rate, $current_symbol); ?></p>
                        <? endforeach; ?>
                    </div>
                    <div id="order_total_modules" class="sm_layout_box">
                        <h2>Сумма</h2>
                        <div class="contentText">
                            <div>
                                <table border="0" cellspacing="0" cellpadding="2">
                                    <tbody>
                                        <tr>
                                            <td class="main"><b>Общая стоимость</b>:</td>
                                            <td class="main"><b><?= generatePriceString($cart['total_sum'], $current_rate, $current_symbol); ?></b></td>
                                            <input type="hidden" name="total_sum" value="<?= $cart['total_sum']; ?>">
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                    </div>
                    <div class="form-anti-bot"><strong>Leave this field empty</strong> <span class="required">*</span><input type="text" name="anti-bot-e-email-url" id="anti-bot-e-email-url" size="30" value=""></div>
                    <br>
                    <div class="clear"></div>
                    <div class="pageContentFooter"><span class="button"><button type="submit"><img src="/web/images/icons/submit.png" alt="Подтвердить заказ" title="Подтвердить заказ" width="12" height="12">&nbsp;Подтвердить заказ</button></span></div>
                    <br>
                    <div> Нажимая кнопку, я даю согласие на обработку своих персональных данных. <a href="/privacy<?= SITE_URLS_SUFFIX; ?>">Подробнее о защите персональной информации.</a></div>
                </form>
            </div>
        </div>
    </div>
</div>