<?php    
    namespace app\controllers;
    use Yii;
    use yii\web\Controller;
    use yii\helpers\Url;
    use app\models\CatalogCategories;
    use app\models\CatalogProducts;
    use app\models\CatalogCurrencies;

    class GlobalController extends Controller
    {
        public $cart_products = [];
        public $cart_hash = false;
        private $cart_hash_key = 'cart.hash';


        public function init(){
            $this->setCartProducts();
            parent::init();
        }


        public function formatPriceToRUB($price = 0){
            return number_format($price, 0, '.', ' ' );
        }


        public function convertPriceWithRate($price = 0, $rate = 1){
            return ceil($price * $rate);
        }


        public function isImageExists($image = false) {
            return ($image) ? $image : '/images/catalog/no_photo.png';
        }


        private function setCartHash() {
            Yii::$app->session->open();
            $cart = Yii::$app->session->get('cart');
            $this->cart_hash = Yii::$app->getSecurity()->hashData(json_encode($cart), $this->cart_hash_key);
        }


        public function validateCartHash() {
            Yii::$app->session->open();
            $cart = Yii::$app->session->get('cart');
            return Yii::$app->getSecurity()->validateData(json_encode($cart), $this->cart_hash_key);
        }


        public function setCartProducts() {
            Yii::$app->session->open();
            if (!Yii::$app->session->has('cart')) {
                Yii::$app->session->set('cart', []);
            }
    
            $session_products = Yii::$app->session->get('cart')['products'];
            $products_ids = array_keys($session_products);
            if ($products_ids) {
                $categories_query = CatalogCategories::find()->asArray()->all();
                $categories = [];
                foreach ($categories_query as $category) {
                    $categories[$category['id']] = $category['link'];
                }

                $currencies_query = CatalogCurrencies::find()->asArray()->all();
                $currencies = [];
                foreach ($currencies_query as $currency) {
                    $currencies[$currency['id']] = $currency['rate'];
                }

                $cart_products = CatalogProducts::find($products_ids)->where(['in', 'id', $products_ids])->asArray()->all();
                foreach ($cart_products as $product) {
                    $product_id = $product['id'];
                    $this->cart_products[$product_id] = [
                        'count' => $session_products[$product_id],
                        'name' => $product['name'],
                        'image' => $product['image'],
                        'price' => $this->convertPriceWithRate($product['price'], $currencies[$product['currency_id']]),
                        'link' => Url::toRoute('/catalog/'.$categories[$product['category_id']].'/'.$product_id)
                    ];
                }
            }

            $this->setCartHash();
        }
    }