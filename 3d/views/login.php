<script type="text/javascript" src="jscript/modified.js"></script><script type="text/javascript">
    $(document).ready(function(){
    
        $(function () {
            $('#login :input:text:visible:enabled:first').focus();
        })
    
    })
    
</script>
<h1>Зарегистрируйтесь или войдите в магазин</h1>
<form name="login" id="form_login" action="/ajax/login<?= SITE_URLS_SUFFIX; ?>" method="post">
    <div class="page">
        <div class="pagecontent">
            <dl class="Login">
                <dt class="Login"></dt>
                <dd class="Login"><span class="bold">Зарегистрируйтесь в магазине</span></dd>
                <dd> Зарегистрировавшись в нашем магазине, Вы сможете совершать покупки намного быстрее и удобнее, кроме того, Вы сможете следить за выполнением заказов, смотреть историю своих заказов.</dd>
                <dd><a class="button" href="/register<?= SITE_URLS_SUFFIX; ?>"><span><img src="/web/images/icons/submit.png" alt="Зарегистрироваться" title=" Зарегистрироваться " width="12" height="12">&nbsp;Зарегистрироваться</span></a></dd>
            </dl>
            <dl class="Login">
                <dt class="Login"></dt>
                <dd></dd>
                <dd>
                    <fieldset class="form">
                        <legend>Введите свои данные для входа</legend>
                        <p><label>E-Mail:</label> <input type="email" name="email"></p>
                        <p><label>Пароль:</label> <input type="password" name="password" minlength="<?= MIN_LENGTH_USER_PASSWORD; ?>" maxlength="<?= MAX_LENGTH_USER_PASSWORD; ?>"></p>
                    </fieldset>
                    <p><a href="/reset_password<?= SITE_URLS_SUFFIX; ?>">Забыли пароль?</a></p>
                    <p><span class="button"><button type="submit"><img src="/web/images/icons/submit.png" alt="Войти" title=" Войти " width="12" height="12">&nbsp;Войти</button></span></p>
                </dd>
            </dl>
            <div class="clear"></div>
        </div>
    </div>
</form>